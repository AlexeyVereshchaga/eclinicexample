# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/alex/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontpreverify
-repackageclasses ''
-allowaccessmodification
-optimizations !code/simplification/arithmetic
-keepattributes *Annotation*

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.content.Context {
   public void *(android.view.View);
   public void *(android.view.MenuItem);
}

-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# For RoboSpice
#Results classes that only extend a generic should be preserved as they will be pruned by Proguard
#as they are "empty", others are kept
-keep class com.alex.eclinic.model.**

-keep class com.alex.eclinic.network.**

#RoboSpice requests should be preserved in most cases
-keepclassmembers class com.alex.eclinic.network.** {
  public void set*(***);
  public *** get*();
  public *** is*();
}

#Warnings to be removed. Otherwise maven plugin stops, but not dangerous
-ignorewarnings
#-dontwarn android.support.**
#-dontwarn com.sun.xml.internal.**
#-dontwarn com.sun.istack.internal.**
#-dontwarn org.codehaus.jackson.**
#-dontwarn org.springframework.**
#-dontwarn java.awt.**
#-dontwarn javax.security.**
#-dontwarn java.beans.**
#-dontwarn javax.xml.**
#-dontwarn java.util.**
#-dontwarn org.w3c.dom.**
#-dontwarn com.google.common.**
#-dontwarn com.octo.android.robospice.persistence.**

-keepattributes *Annotation*,EnclosingMethod,Signature

-keep public class com.alex.eclinic.** {
  public void set*(***);
  public *** get*();
}

### Jackson SERIALIZER SETTINGS
-keepclassmembers,allowobfuscation class * {
    @org.codehaus.jackson.annotate.* <fields>;
    @org.codehaus.jackson.annotate.* <init>(...);
}

-keep class com.fasterxml.** { *; }
-keep class com.octo.android.robospice.retrofit.** { *; }

-keepnames class com.fasterxml.jackson.** { *; }
 -dontwarn com.fasterxml.jackson.databind.**
 -keep class org.codehaus.** { *; }
 -keepclassmembers public final enum org.codehaus.jackson.annotate.JsonAutoDetect$Visibility {
 public static final org.codehaus.jackson.annotate.JsonAutoDetect$Visibility *; }
  -keepclassmembers public final enum com.fasterxml.jackson.annotate.JsonAutoDetect$Visibility {
  public static final com.fasterxml.jackson.annotation.JsonAutoDetect$Visibility *; }

-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}

-keep class com.squareup.** { *; }
-keepclassmembers class * {
    @org.codehaus.jackson.annotate.* *;
}
-keepclassmembers class * {
    @com.fasterxml.jackson.annotation.* *;
}

#Retrofit
#-keepattributes Signature

-keep class retrofit.** { *; }
-keep class retrofit.http.** { *; }
-keep class retrofit.client.** { *; }
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-keep class com.google.gson.** { *; }
-keep class com.google.inject.* { *; }
-keep class org.apache.http.* { *; }
-keep class org.codehaus.mojo.** { *; }
-keep class org.apache.james.mime4j.* { *; }
-keep class javax.inject.* { *; }
-keep class sun.misc.Unsafe { *; }

-dontwarn javax.xml.stream.events.**
-dontwarn rx.**
-dontwarn org.apache.lang.**
-dontwarn com.squareup.okhttp.**

#GMS
-keep class com.google.android.gms.** { *; }
-keep class com.google.android.gms.common.** { *; }
-dontwarn com.google.android.gms.**
-keep public class com.google.android.gms.* { public *; }
-keep class com.alex.eclinic.gcm.** { *; }
-keep class com.alex.eclinic.controller.** { *; }