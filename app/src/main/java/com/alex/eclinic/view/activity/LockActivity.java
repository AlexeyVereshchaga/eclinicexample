package com.alex.eclinic.view.activity;

import android.app.Fragment;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.transit.LockTransitManager;
import com.alex.eclinic.utils.SharedPreferencesHelper;
import com.alex.eclinic.view.fragment.lock.LockFragment;
import com.alex.eclinic.view.fragment.lock.SplashLockFragment;

import java.util.Date;

public class LockActivity extends BaseActivity {

    @Override
    protected void onResume() {
        super.onResume();
        if (getFragmentManager().findFragmentById(R.id.fragment_container_external) == null
                && SharedPreferencesHelper.getExpiredAt() < new Date().getTime()) {
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container_external, Fragment.instantiate(this, SplashLockFragment.class.getName()), SplashLockFragment.class.getName())
                    .commitAllowingStateLoss();
        }
    }

    @Override
    protected int getActivityLayoutRes() {
        return R.layout.activity_lock;
    }

    @Override
    protected int getMainFragmentContainerRes() {
        return R.id.fragment_container;
    }

    @Override
    protected void createTransitManager() {
        transitManager = new LockTransitManager(this);
    }

    @Override
    protected Class<? extends Fragment> getFragmentForStart() {
        return LockFragment.class;
    }

    @Override
    public void onBackPressed() {
    }
}
