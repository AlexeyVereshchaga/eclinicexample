package com.alex.eclinic.view.fragment.pin;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.transit.FragmentAction;
import com.alex.eclinic.view.fragment.Dialog;

import static com.alex.eclinic.controller.HeaderController.LEFT_BTN_ID;
import static com.alex.eclinic.controller.HeaderController.RIGHT_BTN_ID;

/**
 * Class description
 * 23.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class ChangePinFragment extends PinFragment {

    @Override
    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setTitle(getActivity().getString(R.string.fragm_pin_title_create));
            getHeaderController().setRightButton(this, true, getActivity().getString(R.string.fragm_pin_header_next));
            getHeaderController().setLeftButton(this, true, getActivity().getString(R.string.fragm_header_common_back));
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case RIGHT_BTN_ID:
                if (TextUtils.isEmpty(etPin.getText())) {
                    Dialog dialog = Dialog.newInstance(getActivity().getString(R.string.dialog_no_pin),
                            getActivity().getString(R.string.dialog_no_pin_message));
                    dialog.show(getFragmentManager(), "0");
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString(CreatePinFragment.PIN_KEY, etPin.getText().toString());
                    getTransitManager().switchFragment(this, FragmentAction.CONFIRM_CHANGE_PIN_ACTION, bundle);
                }
                break;
            case LEFT_BTN_ID:
                getTransitManager().back();
        }
    }

}
