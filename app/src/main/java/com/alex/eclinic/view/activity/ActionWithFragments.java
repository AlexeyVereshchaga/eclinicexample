package com.alex.eclinic.view.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.annotation.Nullable;

import com.alex.eclinic.R;
import com.alex.eclinic.view.fragment.CallDetailsFragment;
import com.alex.eclinic.view.fragment.CallbackFragment;
import com.alex.eclinic.view.fragment.CallsFragment;
import com.alex.eclinic.view.fragment.ForwardFragment;
import com.alex.eclinic.view.fragment.PopUpFragment;
import com.alex.eclinic.view.fragment.RecipientFragment;
import com.alex.eclinic.view.fragment.TextFragment;
import com.alex.eclinic.view.fragment.archive.ArchivedCallDetailsFragment;
import com.alex.eclinic.view.fragment.forward.ForwardedCallDetailsFragment;
import com.alex.eclinic.view.fragment.inbox.InboxCallDetailsFragment;
import com.alex.eclinic.view.fragment.inbox.RecordFragment;
import com.alex.eclinic.view.fragment.inbox.TextMessageFragment;
import com.alex.eclinic.view.fragment.listener.OnSentListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 05.11.15.
 *
 * @author Alexey Vereshchaga
 */
public class ActionWithFragments implements OnSentListener, RecipientFragment.OnRecipientChosen, CallDetailsFragment.Callbacks {

    private Activity activity;

    public ActionWithFragments(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onSent() {
        if (activity != null) {
            Class[] classes = {InboxCallDetailsFragment.class, ArchivedCallDetailsFragment.class, ForwardedCallDetailsFragment.class};
            CallDetailsFragment currentFragment = findFragmentByTag(classes);
            if (currentFragment != null) {
                currentFragment.updateEvents();
            }
            CallsFragment callsFragment = (CallsFragment) activity.getFragmentManager().findFragmentByTag(CallsFragment.class.getName());
            if (callsFragment != null) {
                callsFragment.getCalls();
            }
        }
    }

    @Override
    public void onChosen(Integer providerId) {
        if (activity != null) {
            Fragment fragment = activity.getFragmentManager().findFragmentByTag(ForwardFragment.class.getName());
            if (fragment != null) {
                ((ForwardFragment) fragment).updateRecipient(providerId);
            }
        }
    }

    @Nullable
    private <T extends CallDetailsFragment> T findFragmentByTag(Class<T>[] classes) {
        for (Class<T> clazz :
                classes) {
            T fragment = (T) activity.getFragmentManager().findFragmentByTag(clazz.getName());
            if (fragment != null)
                return fragment;
        }
        return null;
    }

    @Override
    public void onCallSwitched(SwitchFragment switchFragment, String sid) {
        FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
        switch (switchFragment) {
            case CALLBACK:
                ft.replace(R.id.fragment_container_external, CallbackFragment.newInstance(sid), CallbackFragment.class.getName());
                ft.addToBackStack(CallbackFragment.class.getName());
                break;
            case MESSAGE:
                ft.replace(R.id.fragment_container_external, TextMessageFragment.newInstance(sid), TextMessageFragment.class.getName());
                ft.addToBackStack(TextMessageFragment.class.getName());
                break;
            case RECORD:
                ft.replace(R.id.fragment_container_external, RecordFragment.newInstance(sid), RecordFragment.class.getName());
                ft.addToBackStack(RecordFragment.class.getName());
                break;
            case TEXT:
                ft.replace(R.id.fragment_container_external, TextFragment.newInstance(sid), TextFragment.class.getName());
                ft.addToBackStack(TextFragment.class.getName());
                break;
            case POP_UP:
                ft.replace(R.id.fragment_container_external, new PopUpFragment(), PopUpFragment.class.getName());
                ft.addToBackStack(PopUpFragment.class.getName());
                break;
        }
        ft.commit();
    }

    @Override
    public void onComplete() {
        if (activity != null) {
            Fragment fragment = activity.getFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment != null && fragment instanceof CallDetailsFragment) {
                ((CallDetailsFragment) fragment).sendCompletedCall();
            }
        }
    }
}
