package com.alex.eclinic.view.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.alex.eclinic.controller.HeaderController;
import com.alex.eclinic.controller.ProfileProvidersController;
import com.alex.eclinic.controller.transit.FragmentAction;
import com.alex.eclinic.view.activity.BaseActivity;
import com.alex.eclinic.view.activity.MainActivity;
import com.fsm.transit.core.ITransitManager;

/**
 * Class description
 * 30.09.15.
 *
 * @author Alexey Vereshchaga
 */
public abstract class AbstractFragment<T extends BaseActivity> extends Fragment {
    private T baseActivity;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getViewLayout(), container, false);
        assignViews(view);
        initView(view);
        return view;
    }

    protected abstract int getViewLayout();

    protected void assignViews(View view) {
    }

    protected void initView(View view) {
    }

    /**
     * special fragment manager, do all switch use this object.
     *
     * @return
     */
    public ITransitManager<FragmentAction> getTransitManager() {
        if (getActivity() != null) {
            return ((BaseActivity) getActivity()).getTransitManager();
        } else {
            return null;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        baseActivity = (T) activity;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setTitle("");
            getHeaderController().setRightButton(null, false, 0, "");
            getHeaderController().setLeftButton(null, false, 0, "");
            getHeaderController().getRightButton().setBackgroundDrawable(null);
            getHeaderController().getLeftButton().setBackgroundDrawable(null);
            getHeaderController().getGeneralHeaderView().setVisibility(View.VISIBLE);
            getHeaderController().getCallsNumberView().setVisibility(View.GONE);
        }
        if (getProvidersController() != null) {
            getProvidersController().getListView().setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity().getCurrentFocus() != null) {
            Object inputMethodManager = getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                ((InputMethodManager) inputMethodManager).hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    public HeaderController getHeaderController() {
        if (getActivity() != null) {
            return ((BaseActivity) getActivity()).getHeaderController();
        } else {
            return null;
        }
    }

    public ProfileProvidersController getProvidersController() {
        //todo think about this controller
        if (getActivity() != null && getActivity() instanceof MainActivity) {
//            try {
            return ((MainActivity) getActivity()).getProfileProvidersController();
//            } catch (ClassCastException e) {
//                Log.e(AbstractFragment.class.getSimpleName(), "", e);
//            }
        }
        return null;
    }

    public T getBaseActivity() {
        return baseActivity;
    }
}
