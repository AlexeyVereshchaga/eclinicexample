package com.alex.eclinic.view.fragment.inbox;

import android.Manifest;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.EventType;
import com.alex.eclinic.modelv3.response.Event;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.utils.audio.AudioRecorder;
import com.alex.eclinic.utils.audio.IRecordingListener;
import com.alex.eclinic.utils.audio.Wave;
import com.alex.eclinic.view.activity.BaseActivity;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.fragment.CallsFragment;
import com.alex.eclinic.view.fragment.listener.OnSentListener;
import com.alex.eclinic.view.fragment.view.VoiceView;
import com.fasterxml.jackson.core.type.TypeReference;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Class description
 * 20.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class RecordFragment extends Fragment implements View.OnClickListener {

    public static final String AUDIO_X_WAV = "audio/x-wav";
    private static final String TAG = RecordFragment.class.getName();
    private static final int MY_AUDIO_PERMISSIONS = 1;
    private static final int SENT_SUCCESS = 1;
    private static final int SENT_FAIL = 2;
    private String sid;
    private TextView tvCancel;
    private GraphView graph;
    private ProgressBar loader;
    private LinearLayout llTimer;
    private TextView timer;
    private LinearLayout llRecordButtons;
    private TextView tvTap;
    private ImageView ivRecord;
    private ImageView ivStop;
    private ImageView ivUndo;
    private ImageView ivCheck;
    private AudioRecorder recorder;
    private MediaPlayer mPlayer;
    private VoiceView voiceView;
    private CountDownTimer t;
    private Handler mHandler;
    private int messageSentStatus;
//    private File recordFile;


    /**
     * @param sid call sid.
     * @return A new instance of fragment RecordFragment.
     */
    public static RecordFragment newInstance(String sid) {
        RecordFragment fragment = new RecordFragment();
        Bundle args = new Bundle();
        args.putString(HelpUtils.CALL_SID_KEY, sid);
        fragment.setArguments(args);
        return fragment;
    }

    public RecordFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sid = getArguments().getString(HelpUtils.CALL_SID_KEY);
        }
        checkPermission();
        mHandler = new Handler(Looper.getMainLooper());
    }

    private void initRecorder() {
        recorder = new AudioRecorder();
        recorder.setRecordingListener(new IRecordingListener() {
            @Override
            public void onStop() {
                startPlaying(recorder.getFilename());
                drawWave(recorder.getFilename());
            }

            @Override
            public void receiveAmplitude(final Double amplitude) {
                if (voiceView != null) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (voiceView != null) {
                                voiceView.animateRadius((float) Math.log10(Math.max(1, amplitude)) * HelpUtils.dpToPx(getActivity(), 18));
                            }
                        }
                    });
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragm_record, container, false);
        tvCancel = (TextView) view.findViewById(R.id.tv_record_cancel);
        graph = (GraphView) view.findViewById(R.id.wave_graph);
        loader = (ProgressBar) view.findViewById(R.id.loader);
        llTimer = (LinearLayout) view.findViewById(R.id.ll_timer);
        timer = (TextView) view.findViewById(R.id.timer);
        llRecordButtons = (LinearLayout) view.findViewById(R.id.ll_record_buttons);
        tvTap = (TextView) view.findViewById(R.id.tv_tap);
        ivRecord = (ImageView) view.findViewById(R.id.iv_record);
        ivStop = (ImageView) view.findViewById(R.id.iv_stop);
        ivUndo = (ImageView) view.findViewById(R.id.iv_undo);
        ivCheck = (ImageView) view.findViewById(R.id.iv_check);
        voiceView = (VoiceView) view.findViewById(R.id.voice_view);
        initView();
        return view;
    }

    private void initView() {
        initPreRecordState();
        tvCancel.setOnClickListener(this);
        ivRecord.setOnClickListener(this);
        ivStop.setOnClickListener(this);
        ivUndo.setOnClickListener(this);
        ivCheck.setOnClickListener(this);

        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.NONE);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);
        graph.setBackgroundColor(0);
    }

    private void checkPermission() {
        // Assume thisActivity is the current activity
        int permissionAudioCheck = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.RECORD_AUDIO);
        int permissionStorageCheck = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionAudioCheck != PackageManager.PERMISSION_GRANTED || permissionStorageCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_AUDIO_PERMISSIONS);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        switch (messageSentStatus) {
            case SENT_SUCCESS:
                ((MainActivity) getActivity()).getTransitManager().back();
                break;
            case SENT_FAIL:
                initPostRecordState();
                break;
        }
        messageSentStatus = 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_record_cancel:
                ((BaseActivity) getActivity()).getTransitManager().back();
                break;
            case R.id.iv_record:
                initRecordState();
                initRecorder();
                try {
                    recorder.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.iv_stop:
                t.cancel();
                initPostRecordState();
                try {
                    recorder.stop();
                } catch (Exception e) {
                    Log.e(TAG, "Error: ", e);
                }
                break;
            case R.id.iv_undo:
                if (mPlayer != null)
                    try {
                        mPlayer.stop();
                    } catch (Exception e) {
                        Log.e("Media Player", "Error: ", e);
                    }
                initRecordState();
                try {
                    recorder.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.iv_check:
                initSendRecordState();
                uploadAudio();
                break;
        }
    }

    private void createTimer() {
        final String timerStr = "%dm%2ds";
        t = new CountDownTimer(Long.MAX_VALUE, 1000) {
            long cnt;

            @Override
            public void onTick(long millisUntilFinished) {
                long seconds;
                cnt++;
                int minutes = (int) (cnt / 60);
                seconds = (int) cnt % 60;
                String time = String.format(timerStr, minutes, seconds);
                final SpannableString ss = new SpannableString(time);
                ss.setSpan(new RelativeSizeSpan(0.5f), time.length() - 4, time.length() - 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new RelativeSizeSpan(0.5f), time.length() - 1, time.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (getActivity() != null) {
                    ss.setSpan(new ForegroundColorSpan(
                                    ContextCompat.getColor(getActivity(), R.color.timer_color)),
                            time.length() - 4, time.length() - 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    ss.setSpan(new ForegroundColorSpan(
                                    ContextCompat.getColor(getActivity(), R.color.timer_color)),
                            time.length() - 1, time.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                }
                timer.setText(ss);
            }

            @Override
            public void onFinish() {
            }
        };
    }

    private void uploadAudio() {
        Event event = new Event();
        ProfileProvider profileProvider = ((MainActivity) getActivity()).getUserAccountManager().getCurrentProfileProvider();
        if (profileProvider != null) {
            event.setProviderId(profileProvider.getId());
            event.setType(EventType.VOICE_RECORDING);
            event.setCreatedAt(new Date());
            File recordFile = new File(recorder.getFilename());
            RequestBody requestBody = RequestBody.create(MediaType.parse(AUDIO_X_WAV), recordFile);
            DataManager.uploadAudioFile(sid, requestBody,
                    new BaseHandler<>(new VoiceMessageCallback(recordFile, event, (MainActivity) getActivity()), new TypeReference<Status>() {
                    }));
        }
    }


    private void startInAnimation(View view) {
        view.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.record_anim_in);
        animation.setDuration(300);
        view.setAnimation(animation);
        view.animate();
        animation.start();
    }

    private void startOutAnimation(View view) {
        Animation animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.record_anim_out);
        animation1.setDuration(300);
        view.setAnimation(animation1);
        view.animate();
        animation1.start();
    }

    private void initPreRecordState() {
        graph.setVisibility(View.GONE);
        loader.setVisibility(View.GONE);
        llTimer.setVisibility(View.GONE);
        llRecordButtons.setVisibility(View.VISIBLE);
        tvTap.setVisibility(View.VISIBLE);
        voiceView.setVisibility(View.INVISIBLE);
        startInAnimation(ivRecord);
        ivStop.setVisibility(View.GONE);
        ivUndo.setVisibility(View.GONE);
        ivCheck.setVisibility(View.GONE);
    }

    private void initRecordState() {
        graph.setVisibility(View.GONE);
        loader.setVisibility(View.GONE);
        llTimer.setVisibility(View.VISIBLE);
        llRecordButtons.setVisibility(View.VISIBLE);
        tvTap.setVisibility(View.INVISIBLE);
        voiceView.setVisibility(View.VISIBLE);
        ivRecord.clearAnimation();
        ivRecord.setVisibility(View.GONE);
        startInAnimation(ivStop);
        startOutAnimation(ivUndo);
        startOutAnimation(ivCheck);
        createTimer();
        t.start();
    }

    private void initPostRecordState() {
        graph.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
        llTimer.setVisibility(View.GONE);
        llRecordButtons.setVisibility(View.VISIBLE);
        tvTap.setVisibility(View.INVISIBLE);
        voiceView.setVisibility(View.INVISIBLE);
        ivRecord.setVisibility(View.GONE);
        ivStop.clearAnimation();
        ivStop.setVisibility(View.GONE);
        startInAnimation(ivUndo);
        startInAnimation(ivCheck);
    }

    private void initSendRecordState() {
        graph.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
        llTimer.setVisibility(View.GONE);
        llRecordButtons.setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        loader.setVisibility(View.GONE);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        releasePlayer();
        if (recorder != null) {
            recorder.releaseRecorder();
        }
        super.onDestroy();
    }

    public void startPlaying(String fileName) {
        releasePlayer();
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(fileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e("Media Player: ", "prepare() failed", e);
        }
    }

    public void releasePlayer() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    private void drawWave(final String filename) {
        if (graph.getSeries() != null) {
            graph.getSeries().clear();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                Wave wave = new Wave(filename);
                short[] amplitudes = wave.getSampleAmplitudes();
                final int length = amplitudes.length;
                double n = length / 500;
                int k = (int) n;
                int counter = 0;
                int counterData = 0;
                DataPoint[] dataPoints = new DataPoint[(int) (2 * length / n) + k];
                for (int i = 0; i < length - 1; i = i + k) {
                    dataPoints[counter] = new DataPoint(counterData, amplitudes[i]);
                    dataPoints[counter + 1] = new DataPoint(counterData, -amplitudes[i]);
                    counter = counter + 2;
                    counterData++;
                }
                List<DataPoint> list = new ArrayList<>();
                for (DataPoint s : dataPoints) {
                    if (s != null) {
                        list.add(s);
                    }
                }
                dataPoints = list.toArray(new DataPoint[list.size()]);
                final LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dataPoints);
                if (mHandler != null) {
                    final DataPoint[] finalDataPoints = dataPoints;
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (graph != null) {
                                series.setColor(Color.WHITE);
                                series.setThickness(3);
                                graph.addSeries(series);
                                graph.getViewport().setXAxisBoundsManual(true);
                                graph.getViewport().setYAxisBoundsManual(true);
                                graph.getViewport().setMaxX(finalDataPoints.length / 2);
                                graph.getViewport().setMaxY(Short.MAX_VALUE);
                                graph.getViewport().setMinY(Short.MIN_VALUE);
                            }
                        }
                    });
                }
            }
        }).start();
    }

    private class VoiceMessageCallback extends BaseRemoteListener<Status> {
        private OnSentListener callback;
        private Event event;
        private File file;

        public VoiceMessageCallback(File file, Event event, MainActivity activity) {
            super(activity);
            this.event = event;
            this.file = file;
            callback = activity.getActionWithFragments();
        }

        @Override
        public void onSuccess(Status result) {
            MainActivity mainActivity = (MainActivity) activity;
            if (mainActivity != null && result.getStatus().equals("OK")) {
                Toast.makeText(activity, R.string.record_sending_success, Toast.LENGTH_SHORT).show();
                callback.onSent();
                file.delete();
                if (activity.getFragmentManager().findFragmentById(R.id.fragment_container_external) != null
                        && activity.getFragmentManager().findFragmentById(R.id.fragment_container_external).getClass().getName()
                        .equals(RecordFragment.class.getName())) {
                    if (!mainActivity.isPaused()) {
                        mainActivity.getTransitManager().back();
                    } else {
                        RecordFragment.this.messageSentStatus = SENT_SUCCESS;
                    }
                }
                CallsFragment callsFragment = (CallsFragment) activity.getFragmentManager().findFragmentByTag(CallsFragment.class.getName());
                if (callsFragment != null) {
                    callsFragment.getCalls();
                }
            }
        }

        @Override
        public void onFailure(Integer errorCode, Status error) {
            super.onFailure(errorCode, error);
            if (activity.getFragmentManager().findFragmentById(R.id.fragment_container_external) != null
                    && activity.getFragmentManager().findFragmentById(R.id.fragment_container_external).getClass().getName()
                    .equals(RecordFragment.class.getName())) {
                if (!((MainActivity) activity).isPaused()) {
                    initPostRecordState();
                } else {
                    RecordFragment.this.messageSentStatus = SENT_FAIL;
                }
            }
        }
    }
}
