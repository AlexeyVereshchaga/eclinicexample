package com.alex.eclinic.view.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.controller.transit.FragmentAction;
import com.alex.eclinic.modelv3.request.Provider;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.User;
import com.alex.eclinic.modelv3.response.UserProfile;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.utils.SharedPreferencesHelper;
import com.alex.eclinic.view.activity.MainActivity;
import com.fasterxml.jackson.core.type.TypeReference;

import static com.alex.eclinic.controller.HeaderController.LEFT_BTN_ID;
import static com.alex.eclinic.controller.HeaderController.RIGHT_BTN_ID;

/**
 * 05.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class ProfileFragment extends AbstractFragment<MainActivity> implements View.OnClickListener {

    private TextView tvName;
    private TextView tvEmail;
    private TextView tvAvailability;
    private CheckBox cbSmsNotification;
    private CheckBox cbVoiceNotification;
    private CheckBox cbEmailNotification;
    private CheckBox cbPushNotification;
    private CheckBox cbPlaySpeaker;
    private LinearLayout llAvailability;
    private LinearLayout llChangePin;
    private Boolean isChanged = false;

    @Override
    protected int getViewLayout() {
        return R.layout.fragm_profile;
    }

    @Override
    protected void assignViews(View view) {
        tvName = (TextView) view.findViewById(R.id.tv_name);
        tvEmail = (TextView) view.findViewById(R.id.tv_email);
        tvAvailability = (TextView) view.findViewById(R.id.tv_availability);
        cbSmsNotification = (CheckBox) view.findViewById(R.id.cb_sms_notifications);
        cbVoiceNotification = (CheckBox) view.findViewById(R.id.cb_voice_notifications);
        cbEmailNotification = (CheckBox) view.findViewById(R.id.cb_email_notifications);
        cbPushNotification = (CheckBox) view.findViewById(R.id.cb_push_notifications);
        cbPlaySpeaker = (CheckBox) view.findViewById(R.id.cb_play_speaker);
        llAvailability = (LinearLayout) view.findViewById(R.id.ll_availability);
        llChangePin = (LinearLayout) view.findViewById(R.id.ll_change_pin);
    }

    @Override
    protected void initView(View view) {
        setViewsValues();
        cbSmsNotification.setOnClickListener(this);
        cbVoiceNotification.setOnClickListener(this);
        cbEmailNotification.setOnClickListener(this);
        cbPushNotification.setOnClickListener(this);
        cbPlaySpeaker.setOnClickListener(this);
        llChangePin.setOnClickListener(this);
        llAvailability.setOnClickListener(this);
    }

    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setTitle(getActivity().getString(R.string.profile_title));
            getHeaderController().setLeftButton(this, true, R.drawable.bar_item_sidebar, null);
            getHeaderController().setRightButton(this, true, getResources().getString(R.string.log_out));
        }
        DataManager.getUserProfile(new BaseHandler<>(new BaseRemoteListener<UserProfile>(getActivity()) {
            @Override
            public void onSuccess(UserProfile result) {
                MainActivity mainActivity = getBaseActivity();
                if (result != null
                        && result.getUser() != null
                        && mainActivity != null
                        && getProvidersController() != null) {
                    mainActivity.getUserAccountManager().setUser(result.getUser());
                    User user = mainActivity.getUserAccountManager().getUser();
                    mainActivity.getUserAccountManager().setProfileProviders(result.getUser().getProviders());
                    mainActivity.getUserAccountManager().updateCurrentProfileProvider();
                    getProvidersController().getProfileProvidersAdapter().clear();
                    getProvidersController().getProfileProvidersAdapter().addAll(result.getUser().getProviders());
                    mainActivity.getSlidingMenu().setName(user.getFirstName() + " " + user.getLastName());
                }
                setViewsValues();
            }
        }, new TypeReference<UserProfile>() {
        }));
    }

    private void setViewsValues() {
        cbPlaySpeaker.setChecked(SharedPreferencesHelper.isSpeakerState());
        if (getBaseActivity() != null && getBaseActivity().getUserAccountManager().getUser() != null) {
            User user = getBaseActivity().getUserAccountManager().getUser();
            tvName.setText(String.format(getBaseActivity().getString(R.string.profile_name_placeholder), user.getFirstName(), user.getLastName()));
            tvEmail.setText(user.getEmail());
            ProfileProvider profileProvider = getBaseActivity().getUserAccountManager().getCurrentProfileProvider();
            if (profileProvider != null
                    && isAdded()) {
                tvAvailability.setText(profileProvider.getPatientAvailable()
                        ? getResources().getString(R.string.availability_yes)
                        : getResources().getString(R.string.availability_no));
                cbSmsNotification.setChecked(profileProvider.getAllowSmsNotifications());
                cbVoiceNotification.setChecked(profileProvider.getAllowVoiceNotifications());
                cbEmailNotification.setChecked(profileProvider.getAllowEmailNotifications());
                cbPushNotification.setChecked(profileProvider.getAllowMobileNotifications());
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case LEFT_BTN_ID:
                getBaseActivity().getSlidingMenu().getSideDrawer().toggleLeftDrawer();
                break;
            case RIGHT_BTN_ID:
                HelpUtils.logout(getBaseActivity());
                break;
            case R.id.cb_play_speaker:
                SharedPreferencesHelper.saveSpeakerState(cbPlaySpeaker.isChecked());
                break;
            case R.id.ll_availability:
                User user = getBaseActivity().getUserAccountManager().getUser();
                if (user != null && getBaseActivity().getUserAccountManager().getCurrentProfileProvider() != null) {
                    ProfileProvider provider = getBaseActivity().getUserAccountManager().getCurrentProfileProvider();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(AvailabilityFragment.AVAILABILITY_KEY, provider.getPatientAvailable());
                    getTransitManager().switchFragment(this, FragmentAction.AVAILABILITY_ACTION, bundle);
                }
                break;
            case R.id.ll_change_pin:
                getTransitManager().switchFragment(this, FragmentAction.CHANGE_PIN_ACTION);
                break;
            case R.id.cb_sms_notifications:
            case R.id.cb_email_notifications:
            case R.id.cb_voice_notifications:
            case R.id.cb_push_notifications:
                isChanged = true;
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        User user = getBaseActivity().getUserAccountManager().getUser();
        if (user != null && isChanged) {
            ProfileProvider provider = getBaseActivity().getUserAccountManager().getCurrentProfileProvider();
            if (provider != null) {
                provider.setAllowSmsNotifications(cbSmsNotification.isChecked());
                provider.setAllowVoiceNotifications(cbVoiceNotification.isChecked());
                provider.setAllowEmailNotifications(cbEmailNotification.isChecked());
                provider.setAllowMobileNotifications(cbPushNotification.isChecked());
                updateProvider(provider);
            }
        }
    }

    private void updateProvider(ProfileProvider profileProvider) {
        if (profileProvider != null) {
            Provider provider = new Provider(profileProvider.getEmail(), profileProvider.getPhoneNumber(),
                    profileProvider.getAllowMobileNotifications(), profileProvider.getAllowEmailNotifications(),
                    profileProvider.getAllowVoiceNotifications(), profileProvider.getAllowSmsNotifications());
            DataManager.updateProfileProvider(profileProvider.getId(), provider,
                    new BaseHandler<>(new BaseRemoteListener<com.alex.eclinic.modelv3.response.Provider>(getActivity()) {
                        @Override
                        public void onSuccess(com.alex.eclinic.modelv3.response.Provider result) {
                            isChanged = false;
                        }
                    }, new TypeReference<com.alex.eclinic.modelv3.response.Provider>() {
                    }));
        }
    }
}
