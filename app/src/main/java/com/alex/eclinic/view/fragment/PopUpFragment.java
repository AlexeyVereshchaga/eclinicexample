package com.alex.eclinic.view.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alex.eclinic.R;
import com.alex.eclinic.TheApplication;
import com.alex.eclinic.utils.SharedPreferencesHelper;
import com.alex.eclinic.view.activity.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PopUpFragment extends Fragment implements View.OnClickListener {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pop_up, container, false);
        view.findViewById(R.id.iv_close).setOnClickListener(this);
        view.findViewById(R.id.fl_pop_up).setOnClickListener(this);
        view.findViewById(R.id.ll_dont_show).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
            case R.id.fl_pop_up:
                ((BaseActivity) getActivity()).getTransitManager().back();
                break;
            case R.id.ll_dont_show:
                SharedPreferencesHelper.saveNeedShowPopUp(false);
                ((BaseActivity) getActivity()).getTransitManager().back();
                break;


        }
    }
}
