package com.alex.eclinic.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.modelv3.response.CallEvent;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.activity.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Class description
 * 15.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class CallDetailsAdapter extends ArrayAdapter<CallEvent> {
    private int resource;
    private SimpleDateFormat sdf = new SimpleDateFormat("h:mm a 'on' MM/dd/yyyy", Locale.US);
    private MainActivity mainActivity;

    public CallDetailsAdapter(MainActivity mainActivity, int resource) {
        super(mainActivity, resource);
        this.resource = resource;
        this.mainActivity = mainActivity;
    }

    static class ViewHolder {
        ImageView ivStatus;
        TextView tvResponse;
        //        TextView tvText;
        TextView tvTime;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(resource, null);
            holder = new ViewHolder();
            holder.tvResponse = (TextView) rowView.findViewById(R.id.tv_response);
            holder.ivStatus = (ImageView) rowView.findViewById(R.id.iv_status);
//            holder.tvText = (TextView) rowView.findViewById(R.id.tv_text_to_patient);
            holder.tvTime = (TextView) rowView.findViewById(R.id.tv_time);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        CallEvent callEvent = getItem(position);
        holder.tvResponse.setText(callEvent.getSummary());
        HelpUtils.setEventStatusImage(callEvent, holder.ivStatus);
//        String text = callEvent.getTextToPatient() == null ? callEvent.getMessage() : callEvent.getTextToPatient();
//        holder.tvText.setText(text);
//        holder.tvText.setVisibility((text == null || text.isEmpty()) ? View.GONE : View.VISIBLE);
        holder.tvTime.setText(callEvent.getCreatedAt() == null ? "Date Unknown" : sdf.format(callEvent.getCreatedAt()));
        return rowView;
    }

}
