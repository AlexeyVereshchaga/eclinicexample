package com.alex.eclinic.view.fragment.forward;

import android.view.View;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.response.Call;
import com.alex.eclinic.modelv3.response.CommonInfo;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.view.fragment.CallsFragment;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;

/**
 * 06.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class ForwardedCallsFragment extends CallsFragment {

    public void onResume() {
        super.onResume();
        if (getBaseActivity() != null
                && (getBaseActivity().getUserAccountManager().getCommonInfoForward() == null || isNeedUpdate())) {
            getCalls();
        }
        setNeedUpdate(false);
        if (getHeaderController() != null) {
            getHeaderController().setTitle(getActivity().getString(R.string.fragm_forwarded_calls_title));
        }
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        setOnItemClickListener();
    }

    @Override
    public void getCalls() {
        ProfileProvider profileProvider = getBaseActivity().getUserAccountManager().getCurrentProfileProvider();
        if (profileProvider != null) {
            DataManager.getForwardedCalls(profileProvider.getId(), new BaseHandler<>(new BaseRemoteListener<CommonInfo>(getActivity()) {
                @Override
                public void onStartTask() {
                    refreshLayoutCalls.setRefreshing(true);
                    setNeedUpdate(true);
                }

                @Override
                public void onSuccess(CommonInfo result) {
                    if (getBaseActivity() != null) {
                        getBaseActivity().getUserAccountManager().setCommonInfoForward(result);
                    }
                    callsAdapter.clear();
                    callsAdapter.addAll(result.getCalls());
                }

                @Override
                public void onFinishTask() {
                    refreshLayoutCalls.setRefreshing(false);
                    setNeedUpdate(false);
                }
            }, new TypeReference<CommonInfo>() {
            }));
        }
    }

    @Override
    protected List<Call> getSavedCalls() {
        List<Call> calls = null;
        if (getBaseActivity().getUserAccountManager().getCommonInfoForward() != null) {
            calls = getBaseActivity().getUserAccountManager().getCommonInfoForward().getCalls();
        }
        return calls;
    }
}
