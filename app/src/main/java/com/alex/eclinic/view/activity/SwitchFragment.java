package com.alex.eclinic.view.activity;

/**
 * Class description
 * 05.11.15.
 *
 * @author Alexey Vereshchaga
 */
public enum SwitchFragment {
    CALLBACK,
    RECORD,
    MESSAGE,
    TEXT,
    POP_UP,
    NOTE,
    FORWARD
}
