package com.alex.eclinic.view.fragment.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.alex.eclinic.R;

/**
 * Class description
 * 02.11.15.
 *
 * @author Alexey Vereshchaga
 */
public class ImageViewClickableState extends ImageView {

    private static final int[] STATE_CLICKABLE = {R.attr.state_clickable};

    public ImageViewClickableState(Context context) {
        super(context);
    }

    public ImageViewClickableState(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ImageViewClickableState(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public int[] onCreateDrawableState(final int extraSpace) {
        if (isClickable()) {
            final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
            mergeDrawableStates(drawableState, STATE_CLICKABLE);
            return drawableState;
        } else {
            return super.onCreateDrawableState(extraSpace);
        }
    }

    @Override
    public void setClickable(final boolean clickable) {
        super.setClickable(clickable);
        refreshDrawableState();
    }

}
