package com.alex.eclinic.view.fragment.listener;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.alex.eclinic.R;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.view.activity.BaseActivity;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.fragment.Dialog;

import retrofit2.Call;

import static com.alex.eclinic.network.ErrorCode.NETWORK_NOT_AVAILABLE;

/**
 * 26.10.15.
 *
 * @author Alexey Vereshchaga
 */
public abstract class BaseActionRemoteListener extends BaseRemoteListener<Status> {
    protected Class fragmentType;
    private OnSentListener callback;

    public BaseActionRemoteListener(MainActivity activity, Class clazz) {
        super(activity);
        callback = activity.getActionWithFragments();
        fragmentType = clazz;
    }

    @Override
    public void onFailure(Integer failureCode, Call<Status> call, BaseHandler<Status> handler) {
        super.onFailure(failureCode, call, handler);
        if (failureCode.equals(NETWORK_NOT_AVAILABLE.getCode())) {
            if (activity != null) {
                Dialog dialog = Dialog.newInstance(activity.getString(R.string.action_network_not_available_title),
                        activity.getString(R.string.action_network_not_available_text));
                dialog.show(activity.getFragmentManager(), "0");
            }
            Log.e(TAG_NETWORK_NOT_AVAILABLE, NETWORK_NOT_AVAILABLE.getMessage());
        }
    }

    @Override
    public void onSuccess(Status result) {
        if (activity != null) {
            BaseActivity baseActivity = (BaseActivity) activity;
            if (result.getStatus().equals("OK")
                    && baseActivity.getTransitManager() != null
                    && baseActivity.getTransitManager().getCurrentFragment().getClass().getName().equals(fragmentType.getName())) {
                baseActivity.getTransitManager().back();
            }
            Toast.makeText(activity, getSuccessStringRes(), Toast.LENGTH_SHORT).show();
            callback.onSent();
        }
    }

    protected abstract int getSuccessStringRes();
}
