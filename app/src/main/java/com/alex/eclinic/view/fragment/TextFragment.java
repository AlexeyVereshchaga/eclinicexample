package com.alex.eclinic.view.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.activity.BaseActivity;

/**
 * 29.01.16.
 *
 * @author Alexey Vereshchaga
 */
public class TextFragment extends Fragment implements View.OnClickListener {
    // the fragment initialization parameter
    private String mSid;
    private Boolean mPremiumClicked = false;

    private FrameLayout flText;
    private TextView tvMessage;
    private Button btnPremium;
    private TextView tvNotNow;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sid call sid.
     * @return A new instance of fragment TextMessageFragment.
     */
    public static TextFragment newInstance(String sid) {
        TextFragment fragment = new TextFragment();
        Bundle args = new Bundle();
        args.putString(HelpUtils.CALL_SID_KEY, sid);
        fragment.setArguments(args);
        return fragment;
    }

    public TextFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSid = getArguments().getString(HelpUtils.CALL_SID_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragm_text, container, false);
        flText = (FrameLayout) view.findViewById(R.id.fl_text);
        tvMessage = (TextView) view.findViewById(R.id.tv_message);
        btnPremium = (Button) view.findViewById(R.id.btn_premium);
        tvNotNow = (TextView) view.findViewById(R.id.tv_not_now);
        initViews();
        return view;
    }

    private void initViews() {
        flText.setOnClickListener(this);
        btnPremium.setOnClickListener(this);
        tvNotNow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_premium:
                tvMessage.setText(R.string.text_fragment_premium_clicked);
                tvNotNow.setText(R.string.text_fragment_premium_clicked_button);
                if (mPremiumClicked) {
                    ((BaseActivity) getActivity()).getTransitManager().back();
                }
                mPremiumClicked = true;
                break;
            case R.id.fl_text:
            case R.id.tv_not_now:
                ((BaseActivity) getActivity()).getTransitManager().back();
                break;
        }
    }
}
