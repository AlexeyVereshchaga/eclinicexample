package com.alex.eclinic.view.activity;

import android.app.Fragment;
import android.os.Bundle;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.transit.StartTransitManager;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.fragment.Dialog;
import com.alex.eclinic.view.fragment.LoginFragment;

public class StartActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean(HelpUtils.FORGOT_PIN_KEY)) {
            Dialog dialog = Dialog.newInstance(getString(R.string.login_please_sign_in),
                    getString(R.string.login_forgot_dialog));
            dialog.show(getFragmentManager(), "0");
        }
    }

    @Override
    protected int getHeaderViewIdRes() {
        return 0;
    }

    @Override
    protected int getActivityLayoutRes() {
        return R.layout.activity_start;
    }

    @Override
    protected int getMainFragmentContainerRes() {
        return R.id.fragment_container;
    }

    @Override
    protected void createTransitManager() {
        transitManager = new StartTransitManager(this);
    }

    protected Class<? extends Fragment> getFragmentForStart() {
        return LoginFragment.class;
    }
}
