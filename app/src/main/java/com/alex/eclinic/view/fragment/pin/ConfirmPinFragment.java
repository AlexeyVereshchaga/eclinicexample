package com.alex.eclinic.view.fragment.pin;

import android.os.Bundle;
import android.view.View;

import com.alex.eclinic.R;
import com.alex.eclinic.TheApplication;
import com.alex.eclinic.utils.SharedPreferencesHelper;
import com.alex.eclinic.view.fragment.Dialog;
import com.alex.eclinic.view.fragment.ProfileFragment;

/**
 * Class description
 *
 * @author Alexey Vereshchaga
 */

public class ConfirmPinFragment extends PinFragment {

    private String pinConfirm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pinConfirm = getArguments().getString(CreatePinFragment.PIN_KEY);
    }

    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setTitle(getActivity().getString(R.string.fragm_pin_confirm));
            getHeaderController().setLeftButton(this, true, getActivity().getString(R.string.fragm_header_common_back));
            getHeaderController().setRightButton(this, true, getActivity().getString(R.string.fragm_pin_header_done));
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_right:
                if (pin.equals(pinConfirm)) {
                    SharedPreferencesHelper.savePin(pin);
                    getTransitManager().switchBranch(ProfileFragment.class);
//                    ((MainActivity) getActivity()).getSlidingMenu().getSideDrawer().toggleLeftDrawer();
                } else {
                    etPin.getText().clear();
                    pin = "";
                    Dialog dialog = Dialog.newInstance(getActivity().getString(R.string.pin_doesnt_match),
                            getActivity().getString(R.string.pin_doesnt_match_message));
                    dialog.show(getFragmentManager(), "0");
                }
                break;
            case R.id.btn_left:
                getTransitManager().back();
                break;
        }
    }
}

