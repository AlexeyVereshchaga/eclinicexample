package com.alex.eclinic.view.adapter;

import android.os.SystemClock;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.modelv3.RecordType;
import com.alex.eclinic.modelv3.response.Call;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.fragment.Dialog;

/**
 * 12.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class CallsAdapter extends ArrayAdapter<Call> {

    private static final String TAG = CallsAdapter.class.getSimpleName();

    private final int ANIMATION_DURATION = 200;

    private static final int SWIPE_MIN_DISTANCE = 25;
    private static final int SWIPE_MAX_OFF_PATH = 600;
    private static final int SWIPE_THRESHOLD_VELOCITY = 25;

    private View currentView;

    private int resource;
    private MainActivity mainActivity;
    private ItemCallback itemCallback;
    private long mLastClickTime;

    public CallsAdapter(MainActivity mainActivity, int resource) {
        super(mainActivity, resource);
        this.resource = resource;
        this.mainActivity = mainActivity;
    }

    static class ViewHolder {
        public FrameLayout flCall;
        public RelativeLayout rlArchive;
        public LinearLayout llCall;
        public ImageView ivStatus;
        public TextView tvPhone;
        public TextView tvTimeAgo;
        public TextView tvDob;
        public TextView tvResponse;
        public TextView tvArchive;
//        public TextView tvCallArchiveCancel;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(resource, null);
            holder = new ViewHolder();
            holder.flCall = (FrameLayout) rowView.findViewById(R.id.fl_call);
            holder.rlArchive = (RelativeLayout) rowView.findViewById(R.id.rl_archive);
            holder.llCall = (LinearLayout) rowView.findViewById(R.id.ll_call);
            holder.ivStatus = (ImageView) rowView.findViewById(R.id.iv_status);
            holder.tvPhone = (TextView) rowView.findViewById(R.id.tv_phone);
            holder.tvTimeAgo = (TextView) rowView.findViewById(R.id.tv_time_ago);
            holder.tvDob = (TextView) rowView.findViewById(R.id.tv_dob);
            holder.tvResponse = (TextView) rowView.findViewById(R.id.tv_response);
            holder.tvArchive = (TextView) rowView.findViewById(R.id.tv_archive);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        Call call = getItem(position);
        holder.tvPhone.setText(HelpUtils.getPhoneNumber(call.getFrom()));
        String dob = HelpUtils.getTextByRecordType(call, RecordType.DOB);

        holder.tvDob.setText(String.format(getContext().getString(R.string.user_dob_call), (dob == null || dob.isEmpty() || dob.equals("(blank)")
                ? getContext().getString(R.string.dob_not_specified)
                : dob + HelpUtils.getNumberOfYears(dob))));
        holder.tvTimeAgo.setText(HelpUtils.getTimeAgo(call.getTranscribedAt()));
        if (itemCallback != null) {
            holder.rlArchive.setClickable(true);
            holder.tvArchive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemCallback.onArchiveClick(position);
                    closeAnimation();

                }
            });

            final GestureDetector gestureDetector = new GestureDetector(getContext(), new MyGestureDetector(position, holder.flCall));
            holder.llCall.setOnTouchListener(new MyOnTouchListener(position, holder.llCall, gestureDetector));
        }
        HelpUtils.setResponseTextToCall(call, holder.tvResponse);
        HelpUtils.setCallStatusImage(call, holder.ivStatus);
        return rowView;
    }

    public void setItemCallback(ItemCallback itemCallback) {
        this.itemCallback = itemCallback;
    }

    public void openAnimation() {
        if (currentView != null) {
            currentView.findViewById(R.id.ll_call).animate()
                    .translationX(-currentView.findViewById(R.id.tv_archive).getWidth())
                    .setDuration(ANIMATION_DURATION);
        }
    }

    public void closeAnimation() {
        if (currentView != null) {
            currentView.findViewById(R.id.ll_call).animate()
                    .translationX(0)
                    .setDuration(ANIMATION_DURATION);
        }
        currentView = null;
    }


    public interface ItemCallback {
        void onArchiveClick(int position);

        void clickItem(int position);
    }

    private class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private int position;
        private View view;

        public MyGestureDetector(int position, View view) {
            this.position = position;
            this.view = view;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
//                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
//                    return false;
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    if (itemCallback != null) {
                        if (HelpUtils.isCallUnread(getItem(position))) {
                            Dialog dialog = Dialog.newInstance(null, mainActivity.getString(R.string.inbox_archive_message));
                            dialog.show(mainActivity.getFragmentManager(), HelpUtils.DIALOG_TAG);
                        } else {
                            closeAnimation();
                            currentView = view;
                            openAnimation();
                        }
                    }
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    closeAnimation();
                }
            } catch (Exception e) {
                Log.e(TAG, "Error: ", e);
            }
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
    }

    private class MyOnTouchListener implements View.OnTouchListener {
        private int position;
        private View view;
        private GestureDetector gestureDetector;
        private int lastX;

        public MyOnTouchListener(int position, View view, GestureDetector gestureDetector) {
            this.position = position;
            this.view = view;
            this.gestureDetector = gestureDetector;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    lastX = (int) event.getX();
                    break;
                case MotionEvent.ACTION_UP:
                    if (Math.abs(lastX - event.getX()) < SWIPE_MIN_DISTANCE && SystemClock.elapsedRealtime() - mLastClickTime > 1500) {
                        mLastClickTime = SystemClock.elapsedRealtime();
                        itemCallback.clickItem(position);
                        view.setPressed(true);
                    }
                    break;
            }
            return gestureDetector.onTouchEvent(event);
        }
    }
}
