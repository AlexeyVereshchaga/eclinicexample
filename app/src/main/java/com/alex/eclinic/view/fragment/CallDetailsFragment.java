package com.alex.eclinic.view.fragment;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.TheApplication;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.controller.transit.FragmentAction;
import com.alex.eclinic.modelv3.RecordType;
import com.alex.eclinic.modelv3.RemoteAction;
import com.alex.eclinic.modelv3.RemoteMessageType;
import com.alex.eclinic.modelv3.response.Call;
import com.alex.eclinic.modelv3.response.CallEvent;
import com.alex.eclinic.modelv3.response.CallWrapper;
import com.alex.eclinic.modelv3.response.Record;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.utils.SharedPreferencesHelper;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.activity.SwitchFragment;
import com.alex.eclinic.view.adapter.CallDetailsAdapter;
import com.danikula.videocache.HttpProxyCacheServer;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.alex.eclinic.controller.HeaderController.LEFT_BTN_ID;
import static com.alex.eclinic.controller.HeaderController.RIGHT_BTN_ID;

/**
 * 03.11.15.
 *
 * @author Alexey Vereshchaga
 */
public abstract class CallDetailsFragment extends AbstractFragment<MainActivity> implements
        View.OnClickListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    private static final String DATE_TEMPLATE = "h:mm a 'on' MM/dd/yyyy";
    private ListView lvCallDetails;
    private CallDetailsAdapter adapter;
    protected Call call;
    private MediaPlayer mediaPlayer;

    private ViewGroup header;
    private TextView tvCallerName;
    private ImageView ivCallerName;
    private TextView tvDob;
    private ImageView ivDob;
    private TextView tvProvider;
    private TextView tvTime;
    private TextView tvRecordText;
    private ImageView ivRecord;

    private LinearLayout llIncompleteOptions;
    private ImageView ivOptionCall;
    private ImageView ivOptionRecord;
    private ImageView ivOptionType;
    //    private ImageView ivOptionText;
    private ImageView ivOptionNote3;
    protected ImageView ivOptionDismiss;
    private ImageView ivOptionComplete;
    protected ImageView ivOptionForward;
    private ImageView ivOptionForwardUrgent;

    private LinearLayout llPlayer;
    private ImageButton ibPlay;
    private SeekBar sbPlayProgress;

    private long mLastClickTime;
    private Callbacks mCallbacks;
    private Handler seekHandler = new Handler();

    private Runnable run = new Runnable() {
        @Override
        public void run() {
            seekUpdate();
        }
    };

    public void seekUpdate() {
        if (mediaPlayer != null) {
            sbPlayProgress.setProgress(mediaPlayer.getCurrentPosition());
            seekHandler.postDelayed(run, 100);
        } else {
            seekHandler.removeCallbacks(run);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = ((MainActivity) activity).getActionWithFragments();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        call = HelpUtils.getObjectFromJsonString(getArguments().getString(HelpUtils.CALL_DETAILS_KEY), Call.class);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragm_call_details;
    }

    @Override
    protected void assignViews(View view) {
        lvCallDetails = (ListView) view.findViewById(R.id.events_list);

        header = (ViewGroup) getActivity().getLayoutInflater().inflate(
                R.layout.header_call, lvCallDetails, false);
        tvCallerName = (TextView) header.findViewById(R.id.tv_caller_name);
        ivCallerName = (ImageView) header.findViewById(R.id.iv_caller_name_play);
        tvDob = (TextView) header.findViewById(R.id.tv_dob);
        ivDob = (ImageView) header.findViewById(R.id.iv_dob_play);
        tvProvider = (TextView) header.findViewById(R.id.tv_provider);
        tvTime = (TextView) header.findViewById(R.id.tv_time);
        tvRecordText = (TextView) header.findViewById(R.id.tv_message);
        ivRecord = (ImageView) header.findViewById(R.id.iv_record_play);

        llIncompleteOptions = (LinearLayout) view.findViewById(R.id.ll_incomplete_options);
        ivOptionCall = (ImageView) view.findViewById(R.id.iv_option_call);
        ivOptionRecord = (ImageView) view.findViewById(R.id.iv_option_record);
        ivOptionType = (ImageView) view.findViewById(R.id.iv_option_type);
//        ivOptionText = (ImageView) view.findViewById(R.id.iv_option_chat);
        ivOptionNote3 = (ImageView) view.findViewById(R.id.iv_option_note_3);
        ivOptionDismiss = (ImageView) view.findViewById(R.id.iv_option_dismiss);
        ivOptionComplete = (ImageView) view.findViewById(R.id.iv_option_complete);
        ivOptionForward = (ImageView) view.findViewById(R.id.iv_option_forward);
        ivOptionForwardUrgent = (ImageView) view.findViewById(R.id.iv_option_forward_urgent);

        llPlayer = (LinearLayout) view.findViewById(R.id.ll_player);
        ibPlay = (ImageButton) view.findViewById(R.id.ib_play_pause);
        sbPlayProgress = (SeekBar) view.findViewById((R.id.sb_audio_progress));
    }

    @Override
    protected void initView(View view) {
        lvCallDetails.addHeaderView(header, null, false);
        tvCallerName.setText(call.getCallerIdName());
        ivCallerName.setOnClickListener(this);
        ivCallerName.setClickable(getRecordingUrl(RemoteMessageType.NAME) != null);
        String dob = HelpUtils.getTextByRecordType(call, RecordType.DOB);

        tvDob.setText(String.format(getResources().getString(R.string.call_details_dob),
                (dob == null || dob.isEmpty() || dob.equals("(blank)")
                        ? getActivity().getString(R.string.not_specified)
                        : dob + HelpUtils.getNumberOfYears(dob))));
        ivDob.setOnClickListener(this);
        ivDob.setClickable(getRecordingUrl(RemoteMessageType.DOB) != null);
        tvProvider.setText(String.format(getResources().getString(R.string.call_deyails_primary_provider),
                HelpUtils.getProviderName(call.getPatientProviderId(), (getBaseActivity().getUserAccountManager().getProviders()))));
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TEMPLATE, Locale.US);
        tvTime.setText(sdf.format(call.getTranscribedAt()));
        String recordText = HelpUtils.getTextByRecordType(call, RecordType.MESSAGE);

        tvRecordText.setText(recordText);
        tvRecordText.setVisibility(recordText == null ? View.GONE : View.VISIBLE);
        ivRecord.setOnClickListener(this);
        ivRecord.setClickable(getRecordingUrl(RemoteMessageType.MESSAGE) != null);

        adapter = new CallDetailsAdapter((MainActivity) getActivity(), R.layout.item_event);
        List<CallEvent> events = call.getCallEvents();
        if (!events.isEmpty()) {
            adapter.addAll(events);
        }
        lvCallDetails.setAdapter(adapter);

        ivOptionCall.setOnClickListener(this);
        ivOptionRecord.setOnClickListener(this);
        ivOptionType.setOnClickListener(this);
//        ivOptionText.setOnClickListener(this);
        ivOptionNote3.setOnClickListener(this);
        ivOptionDismiss.setOnClickListener(this);
//        ivOptionArchive.setOnClickListener(this);
        ivOptionComplete.setOnClickListener(this);
        ivOptionForward.setOnClickListener(this);
        ivOptionForwardUrgent.setOnClickListener(this);
        ibPlay.setOnClickListener(this);
        setViewsVisibility();
        sbPlayProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mediaPlayer != null && fromUser) {
                    mediaPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case LEFT_BTN_ID:
                getTransitManager().back();
                break;
            case RIGHT_BTN_ID:
                switchCallbackFragment();
                break;
            case R.id.iv_caller_name_play:
                releaseMP();
                playRecord(RemoteMessageType.NAME);
                break;
            case R.id.iv_dob_play:
                releaseMP();
                playRecord(RemoteMessageType.DOB);
                break;
            case R.id.iv_record_play:
                releaseMP();
                playRecord(RemoteMessageType.MESSAGE);
                break;
            case R.id.iv_option_call:
                switchCallbackFragment();
                break;
            case R.id.iv_option_record:
                switchRecordFragment();
                break;
            case R.id.iv_option_type:
                switchMessageFragment();
                break;
//            case R.id.iv_option_chat:
//                switchTextFragment();
//                break;
            case R.id.iv_option_note_3:
                switchNoteFragment();
                break;
//            case R.id.iv_option_archive:
//                sendCallToArchive();
//                break;
            case R.id.iv_option_dismiss:
                sendCompletedCall();
//                showDialog();
                break;
            case R.id.iv_option_complete:
                sendCompletedCall();
//                showDialog();
                break;
            case R.id.iv_option_forward:
                if (call.getCompletedAt() != null) {
                    switchForwardFragment(RemoteAction.FORWARD);
                } else if (call.getResponses() != null && !call.getResponses().isEmpty()) {
                    switchForwardFragment(RemoteAction.URGENT_FORWARD);
                }
                break;
            case R.id.iv_option_forward_urgent:
                switchForwardFragment(RemoteAction.URGENT_FORWARD);
                break;
            case R.id.ib_play_pause:
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        ibPlay.setImageResource(R.drawable.button_play);
                        mediaPlayer.pause();
                    } else {
                        ibPlay.setImageResource(R.drawable.button_pause);
                        mediaPlayer.start();
                    }
                }
                break;
        }
    }

//    private void showDialog() {
//        AccessDialog accessDialog = AccessDialog.newInstance(null, getResources().getString(R.string.dialog_access));
//        accessDialog.show(getFragmentManager(), HelpUtils.DIALOG_TAG);
//    }

    public void sendCompletedCall() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        ivOptionComplete.setClickable(false);
        DataManager.sendCompletedCall(this.call.getSid(), new BaseHandler<>(
                new CallRemoteListener(getActivity()), new TypeReference<Call>() {
        }));
    }

    private void switchForwardFragment(RemoteAction remoteAction) {
        Bundle bundle = new Bundle();
        bundle.putString(HelpUtils.CALL_SID_KEY, call.getSid());
        bundle.putString(ForwardFragment.ACTION_KEY, remoteAction.getAction());
        getTransitManager().switchFragment(this, FragmentAction.FORWARD_ACTION, bundle);
    }

    private void switchMessageFragment() {
        mCallbacks.onCallSwitched(SwitchFragment.MESSAGE, call.getSid());
    }

    private void switchCallbackFragment() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        mCallbacks.onCallSwitched(SwitchFragment.CALLBACK, call.getSid());
    }

    private void switchRecordFragment() {
        if (mCallbacks != null) {
            mCallbacks.onCallSwitched(SwitchFragment.RECORD, call.getSid());
        }
    }

    private void switchTextFragment() {
        mCallbacks.onCallSwitched(SwitchFragment.TEXT, call.getSid());
    }

    private void switchNoteFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(HelpUtils.CALL_SID_KEY, call.getSid());
        getTransitManager().switchFragment(this, FragmentAction.NOTE_ACTION, bundle);
    }

    private void playRecord(RemoteMessageType type) {
        String url = getRecordingUrl(type);
        llPlayer.setVisibility(View.VISIBLE);
        sbPlayProgress.setProgress(0);
        if (url != null) {
            try {
                mediaPlayer = new MediaPlayer();
                HttpProxyCacheServer proxy = TheApplication.getProxy(getActivity());
                String proxyUrl = proxy.getProxyUrl(url);
                mediaPlayer.setDataSource(proxyUrl);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                if (!SharedPreferencesHelper.isSpeakerState()) {
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
                }
                mediaPlayer.setOnPreparedListener(this);
                mediaPlayer.setOnCompletionListener(this);
                mediaPlayer.prepareAsync();
                seekUpdate();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getRecordingUrl(RemoteMessageType type) {
        List<Record> records = call.getRecords();
        if (records != null) {
            for (Record record : records) {
                if (record.getType().equalsIgnoreCase(type.name())) {
                    return record.getRecordingUrl();
                }
            }
        }
        return null;
    }

    private void releaseMP() {
        llPlayer.setVisibility(View.GONE);
        if (mediaPlayer != null) {
            try {
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroyView() {
        releaseMP();
        super.onDestroyView();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        sbPlayProgress.setMax(mp.getDuration());
        mp.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        releaseMP();
    }

    private void setViewsVisibility() {
        ivOptionComplete.setClickable(call.getCompletedAt() == null);
        llIncompleteOptions.setVisibility(call.getCompletedAt() == null ? View.VISIBLE : View.GONE);
        if (call.getCompletedAt() != null) {
            ivOptionComplete.setVisibility(View.VISIBLE);
            ivOptionDismiss.setVisibility(View.GONE);
            ivOptionForward.setVisibility(View.VISIBLE);
            ivOptionForwardUrgent.setVisibility(View.GONE);
        } else if (call.getResponses() != null && !call.getResponses().isEmpty()) {
            ivOptionDismiss.setVisibility(View.GONE);
            ivOptionComplete.setVisibility(View.VISIBLE);
            ivOptionForward.setVisibility(View.VISIBLE);
            ivOptionForwardUrgent.setVisibility(View.GONE);
        } else {
            ivOptionDismiss.setVisibility(View.VISIBLE);
            ivOptionComplete.setVisibility(View.GONE);
            ivOptionForward.setVisibility(View.GONE);
            ivOptionForwardUrgent.setVisibility(View.VISIBLE);
        }
        llPlayer.setVisibility(View.GONE);
    }

//    private void sendCallToArchive() {
//        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
//            return;
//        }
//        mLastClickTime = SystemClock.elapsedRealtime();
//
////        ivOptionArchive.setClickable(false);
//        Remote remote = new Remote();
//        remote.setAction(RemoteAction.ARCHIVE);
//        remote.setData(call.getSid());
//        getBaseActivity().getDataManager().sendAction(call.getSid(), remote, new BaseHandler<>(
//                new ArchiveRemoteListener(getActivity(), call.getSid()), Status.class));
//    }

    public void updateEvents() {
        DataManager.getCall(call.getSid(), new BaseHandler<>(new BaseRemoteListener<CallWrapper>(getActivity()) {
            @Override
            public void onSuccess(CallWrapper result) {
                if (result != null && result.getCall() != null) {
                    List<CallEvent> callEvents = result.getCall().getCallEvents();
                    call.setCallEvents(callEvents);
                    if (adapter != null) {
                        adapter.clear();
                        adapter.addAll(callEvents);
                    }
                }
            }
        }, new TypeReference<CallWrapper>() {
        }));
        setViewsVisibility();
    }

    public interface Callbacks {
        void onCallSwitched(SwitchFragment switchFragment, String sid);

        void onComplete();
    }

    private class CallRemoteListener extends BaseRemoteListener<Call> {

        public CallRemoteListener(Activity activity) {
            super(activity);
        }

        @Override
        public void onSuccess(Call result) {
            if (call != null) {
                call.setCompletedAt(new Date());
                if (activity != null) {
                    CallsFragment callsFragment = (CallsFragment) activity.getFragmentManager().findFragmentByTag(CallsFragment.class.getName());
                    if (callsFragment != null) {
                        callsFragment.getCalls();
                    }
                }
            }
        }

        @Override
        public void onFinishTask() {
            setViewsVisibility();
        }
    }
}
