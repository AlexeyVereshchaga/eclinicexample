package com.alex.eclinic.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.modelv3.response.ProfileProvider;

import java.util.List;
import java.util.Locale;

/**
 * 05.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class ProfileProvidersAdapter extends ArrayAdapter<ProfileProvider> {
    private final int resource;

    public ProfileProvidersAdapter(Context context, int resource) {
        super(context, resource);
        this.resource = resource;
    }

    static class ViewHolder {
        public TextView tvPartner;
        public TextView tvNumber;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(resource, null);
            holder = new ViewHolder();
            holder.tvPartner = (TextView) rowView.findViewById(R.id.tv_partner);
            holder.tvNumber = (TextView) rowView.findViewById(R.id.tv_number_of_calls);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        ProfileProvider provider = getItem(position);
        holder.tvPartner.setText(provider.getPartner().getName());
        if (provider.getIncompleteCalls() != null
                && provider.getIncompleteCalls() > 0) {
            holder.tvNumber.setVisibility(View.VISIBLE);
            holder.tvNumber.setText(String.format(Locale.US, "%d", provider.getIncompleteCalls()));
        } else {
            holder.tvNumber.setVisibility(View.GONE);
        }
        return rowView;
    }
}
