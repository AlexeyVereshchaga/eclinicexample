package com.alex.eclinic.view.fragment.inbox;


import com.alex.eclinic.R;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.fragment.CallDetailsFragment;

/**
 * Class description
 * 14.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class InboxCallDetailsFragment extends CallDetailsFragment {

    @Override
    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setLeftButton(this, true, 0, getActivity().getString(R.string.fragm_inbox_title));
            getHeaderController().setTitle(HelpUtils.getPhoneNumber(call.getFrom()));
            getHeaderController().setRightButton(this, true, R.drawable.call_details_icon, null);
        }
    }
}
