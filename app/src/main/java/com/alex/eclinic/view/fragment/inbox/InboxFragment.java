package com.alex.eclinic.view.fragment.inbox;

import android.app.Activity;
import android.view.View;
import android.widget.AbsListView;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.request.Remote;
import com.alex.eclinic.modelv3.RemoteAction;
import com.alex.eclinic.modelv3.response.Call;
import com.alex.eclinic.modelv3.response.CommonInfo;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.utils.SharedPreferencesHelper;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.activity.SwitchFragment;
import com.alex.eclinic.view.adapter.CallsAdapter;
import com.alex.eclinic.view.fragment.CallDetailsFragment;
import com.alex.eclinic.view.fragment.CallsFragment;
import com.alex.eclinic.view.fragment.listener.ArchiveRemoteListener;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;

/**
 * 06.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class InboxFragment extends CallsFragment {

    private CallDetailsFragment.Callbacks mCallbacks;
    private boolean archiving;

    @Override
    protected void initView(View view) {
        super.initView(view);
        lvCalls.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                callsAdapter.closeAnimation();
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = ((MainActivity) activity).getActionWithFragments();
    }

    public void onResume() {
        super.onResume();
        if (getBaseActivity() != null && (getBaseActivity().getUserAccountManager().getCommonInfoNew() == null || isNeedUpdate())) {
            getCalls();
        }
        setNeedUpdate(false);
        if (getHeaderController() != null) {
            getHeaderController().setTitle(getActivity().getString(R.string.fragm_inbox_title));
        }
        if (callsAdapter != null) {
            callsAdapter.setItemCallback(new CallsAdapter.ItemCallback() {
                @Override
                public void onArchiveClick(int position) {
                    sendCallToArchive(callsAdapter.getItem(position).getSid());
                }

                @Override
                public void clickItem(int position) {
                    switchCallDetails(position);
                }
            });
        }
        if (SharedPreferencesHelper.isNeedShowPopUp() && mCallbacks != null) {
            mCallbacks.onCallSwitched(SwitchFragment.POP_UP, null);
        }
    }

    public void getCalls() {
        callsAdapter.closeAnimation();
        ProfileProvider profileProvider = getBaseActivity().getUserAccountManager().getCurrentProfileProvider();
        if (profileProvider != null) {
            DataManager.getNewCalls(profileProvider.getId(), new BaseHandler<>(new BaseRemoteListener<CommonInfo>(getActivity()) {
                @Override
                public void onStartTask() {
                    refreshLayoutCalls.setRefreshing(true);
                    setNeedUpdate(true);
                }

                @Override
                public void onSuccess(CommonInfo result) {
                    if (getBaseActivity() != null) {
                        getBaseActivity().getUserAccountManager().setCommonInfoNew(result);
                    }
                    callsAdapter.clear();
                    callsAdapter.addAll(result.getCalls());
                }

                @Override
                public void onFinishTask() {
                    refreshLayoutCalls.setRefreshing(false);
                    setNeedUpdate(false);
                }
            }, new TypeReference<CommonInfo>() {
            }));
        }

    }

    @Override
    protected List<Call> getSavedCalls() {
        List<Call> calls = null;
        if (getBaseActivity().getUserAccountManager().getCommonInfoNew() != null) {
            calls = getBaseActivity().getUserAccountManager().getCommonInfoNew().getCalls();
        }
        return calls;
    }

    private void sendCallToArchive(String callSid) {
        if (!archiving) {
            Remote remote = new Remote();
            remote.setAction(RemoteAction.ARCHIVE);
            DataManager.sendAction(callSid, remote, new BaseHandler<>(
                    new ArchiveRemoteListener(getActivity(), callSid), new TypeReference<Status>() {
            }));
        }
    }

    public void setArchiving(boolean archiving) {
        this.archiving = archiving;
    }
}