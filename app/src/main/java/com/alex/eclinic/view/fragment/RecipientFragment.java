package com.alex.eclinic.view.fragment;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.Providers;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.adapter.RecipientAdapter;

import java.util.List;

/**
 * 28.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class RecipientFragment extends AbstractFragment<MainActivity> implements View.OnClickListener {
    private ListView lvRecipient;
    private RecipientAdapter adapter;
    private List<ProfileProvider> providers;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_recipient;
    }

    @Override
    protected void assignViews(View view) {
        lvRecipient = (ListView) view.findViewById(R.id.recipients_list);
    }

    @Override
    protected void initView(View view) {
//        adapter = new RecipientAdapter(getActivity(), R.layout.item_recipient, providers);
//        lvRecipient.setAdapter(adapter);
//        MainActivity mainActivity = getBaseActivity();
//        if (mainActivity.getProviders() != null) {
//            providers = new ArrayList<>(mainActivity.getProviders().getData());
////            removeCurrentProvider();
//        }
//        if (providers != null) {
//            adapter.addAll(providers);
//        }
//        lvRecipient.setOnItemClickListener(new ViewClickListener(getBaseActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setTitle(getActivity().getString(R.string.recipient_title));
            getHeaderController().setLeftButton(this, true, getActivity().getString(R.string.recipient_cancel));
        }
        getProviders();
    }

    @Override
    public void onClick(View v) {
        getTransitManager().back();
    }

    public void getProviders() {
//        DataManager.getProviders(new BaseHandler<>(new BaseRemoteListener<Providers>(getActivity()) {
//            @Override
//            public void onSuccess(Providers result) {
//                if (result != null && getBaseActivity() != null
//                        && getBaseActivity().getUserAccountManager() != null) {
//                    getBaseActivity().getUserAccountManager().setProviders(result);
////                    removeCurrentProvider();
//                    adapter.clear();
//                    adapter.addAll(getBaseActivity().getUserAccountManager().getProviders().getData());
//                    adapter.notifyDataSetChanged();
//                }
//
//            }
//        }, Providers.class));
    }

//    private void removeCurrentProvider() {
//        if (providers != null && ((MainActivity) getActivity()).getUser() != null) {
//            for (Provider provider : providers) {
//                if (provider.getId().equals(((MainActivity) getActivity()).getUser().getProvider().getId())) {
//                    providers.remove(provider);
//                    return;
//                }
//            }
//        }
//    }

    private class ViewClickListener implements AdapterView.OnItemClickListener {

        private RecipientFragment.OnRecipientChosen callback;

        public ViewClickListener(MainActivity activity) {
            callback = activity.getActionWithFragments();
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            callback.onChosen(adapter.getItem(position).getId());
            getTransitManager().back();
        }
    }

    public interface OnRecipientChosen {
        void onChosen(Integer providerId);
    }
}
