package com.alex.eclinic.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alex.eclinic.R;

import com.alex.eclinic.modelv3.response.Provider;
import com.tokenautocomplete.FilteredArrayAdapter;

import java.util.List;

/**
 * 28.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class RecipientAdapter extends FilteredArrayAdapter<Provider> {
    private int resource;

    public RecipientAdapter(Context context, int resource, List<Provider> objects) {
        super(context, resource, objects);
        this.resource = resource;
    }

    @Override
    protected boolean keepObject(Provider provider, String mask) {
        mask = mask.toLowerCase();
        return provider.getUser().getFirstName().toLowerCase().contains(mask) || provider.getUser().getLastName().toLowerCase().contains(mask);
    }

    static class ViewHolder {
        public TextView tvRecipientName;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(resource, null);
            holder = new ViewHolder();
            holder.tvRecipientName = (TextView) rowView.findViewById(R.id.tv_name);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        final Provider provider = getItem(position);
        holder.tvRecipientName.setText(String.format("%s %s", provider.getUser().getFirstName(), provider.getUser().getLastName()));
        return rowView;
    }
}
