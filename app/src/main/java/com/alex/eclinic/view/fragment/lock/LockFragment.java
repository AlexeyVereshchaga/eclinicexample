package com.alex.eclinic.view.fragment.lock;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.utils.Constant;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.utils.SharedPreferencesHelper;
import com.alex.eclinic.view.activity.LockActivity;
import com.alex.eclinic.view.fragment.AbstractFragment;
import com.alex.eclinic.view.fragment.Dialog;

/**
 * 09.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class LockFragment extends AbstractFragment<LockActivity> implements View.OnClickListener {

    protected EditText etPin;
    protected Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;
    protected ImageButton ibBackspace;
    private TextView tvForgotPass;

    @Override
    protected int getViewLayout() {
        return R.layout.fragm_lock;
    }

    @Override
    protected void assignViews(View view) {
        etPin = (EditText) view.findViewById(R.id.et_pin);
        etPin = (EditText) view.findViewById(R.id.et_pin);
        btn0 = (Button) view.findViewById(R.id.btn0);
        btn1 = (Button) view.findViewById(R.id.btn1);
        btn2 = (Button) view.findViewById(R.id.btn2);
        btn3 = (Button) view.findViewById(R.id.btn3);
        btn4 = (Button) view.findViewById(R.id.btn4);
        btn5 = (Button) view.findViewById(R.id.btn5);
        btn6 = (Button) view.findViewById(R.id.btn6);
        btn7 = (Button) view.findViewById(R.id.btn7);
        btn8 = (Button) view.findViewById(R.id.btn8);
        btn9 = (Button) view.findViewById(R.id.btn9);
        ibBackspace = (ImageButton) view.findViewById(R.id.ib_backspace);
        tvForgotPass = (TextView) view.findViewById(R.id.tv_forgot_pin);
    }

    @Override
    protected void initView(View view) {
        etPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ibBackspace.setClickable(!TextUtils.isEmpty(s));
                String currentPin = SharedPreferencesHelper.getPin();
                if (currentPin != null && currentPin.length() == s.length() && !currentPin.equals(s.toString())) {
                    etPin.setText("");
                    Dialog dialog = Dialog.newInstance(getString(R.string.lock_incorrect),
                            getString(R.string.lock_incorrect_pin_message));
                    dialog.show(getFragmentManager(), Constant.DIALOG_TAG);

                }
                if (s.toString().equals(SharedPreferencesHelper.getPin())) {
                    getBaseActivity().finish();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btn0.setOnClickListener(this);
        ibBackspace.setOnClickListener(this);
        ibBackspace.setClickable(false);
        tvForgotPass.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn1:
                etPin.getText().append("1");
                break;
            case R.id.btn2:
                etPin.getText().append("2");
                break;
            case R.id.btn3:
                etPin.getText().append("3");
                break;
            case R.id.btn4:
                etPin.getText().append("4");
                break;
            case R.id.btn5:
                etPin.getText().append("5");
                break;
            case R.id.btn6:
                etPin.getText().append("6");
                break;
            case R.id.btn7:
                etPin.getText().append("7");
                break;
            case R.id.btn8:
                etPin.getText().append("8");
                break;
            case R.id.btn9:
                etPin.getText().append("9");
                break;
            case R.id.btn0:
                etPin.getText().append("0");
                break;
            case R.id.ib_backspace:
                int length = etPin.getText().length();
                if (length > 0) {
                    etPin.getText().delete(length - 1, length);
                }
                break;
            case R.id.tv_forgot_pin:
                HelpUtils.logout(getActivity());
                break;
        }
    }
}
