package com.alex.eclinic.view.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.view.activity.MainActivity;
import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Class description
 * 09.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class AvailabilityFragment extends AbstractFragment<MainActivity> implements View.OnClickListener {

    public static final String AVAILABILITY_KEY = "availability";
    private Boolean availability;
    private RadioButton rbAvailable;
    private RadioButton rbUnavailable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        availability = getArguments().getBoolean(AVAILABILITY_KEY);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragm_availability;
    }

    @Override
    protected void assignViews(View view) {
        rbAvailable = (RadioButton) view.findViewById(R.id.rb_available);
        rbUnavailable = (RadioButton) view.findViewById(R.id.rb_unavailable);
    }

    @Override
    protected void initView(View view) {
        if (availability) {
            rbAvailable.setChecked(true);
        } else {
            rbUnavailable.setChecked(true);
        }
        rbAvailable.setOnClickListener(this);
        rbUnavailable.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setLeftButton(this, true, "Back");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_left:
                getTransitManager().back();
                break;
            case R.id.rb_available:
                availability = true;
                updateAvailability();
                break;
            case R.id.rb_unavailable:
                availability = false;
                updateAvailability();
                break;
        }
    }

    private void updateAvailability() {
        DataManager.updateProfileAvailability(getBaseActivity().getUserAccountManager().getCurrentProfileProvider().getId(), availability,
                new BaseHandler<>(new BaseRemoteListener(getActivity()) {
                }, new TypeReference<ProfileProvider>() {
                }));
        ProfileProvider provider = getBaseActivity().getUserAccountManager().getCurrentProfileProvider();
        if (provider != null) {
            provider.setPatientAvailable(availability);
        }
    }
}
