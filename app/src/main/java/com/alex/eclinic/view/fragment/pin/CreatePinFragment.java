package com.alex.eclinic.view.fragment.pin;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.transit.FragmentAction;
import com.alex.eclinic.view.fragment.Dialog;

import static com.alex.eclinic.controller.HeaderController.RIGHT_BTN_ID;

/**
 * Class description
 *
 * @author Alexey Vereshchaga
 */
public class CreatePinFragment extends PinFragment {

    public static final String PIN_KEY = "com.alex.eclinic.pin";


    @Override
    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setTitle(getActivity().getString(R.string.fragm_pin_title_create));
            getHeaderController().setRightButton(this, true, getActivity().getString(R.string.fragm_pin_header_next));
            getHeaderController().setLeftButton(null, false, null);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case RIGHT_BTN_ID:
                if (TextUtils.isEmpty(etPin.getText())) {
                    Dialog dialog = Dialog.newInstance(getActivity().getString(R.string.dialog_no_pin),
                            getActivity().getString(R.string.dialog_no_pin_message));
                    dialog.show(getFragmentManager(), "0");
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString(PIN_KEY, etPin.getText().toString());
                    getTransitManager().switchFragment(this, FragmentAction.CONFIRM_ACTION, bundle);
                }
                break;
        }
    }
}
