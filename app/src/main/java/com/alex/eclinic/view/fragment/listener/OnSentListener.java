package com.alex.eclinic.view.fragment.listener;


import com.alex.eclinic.modelv3.response.Event;

/**
 * Class description
 * 27.10.15.
 *
 * @author Alexey Vereshchaga
 */
public interface OnSentListener {
    void onSent();
}
