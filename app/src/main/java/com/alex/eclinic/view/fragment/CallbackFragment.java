package com.alex.eclinic.view.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.controller.UserAccountManager;

import com.alex.eclinic.modelv3.request.Remote;
import com.alex.eclinic.modelv3.RemoteAction;
import com.alex.eclinic.modelv3.EventType;
import com.alex.eclinic.modelv3.response.Event;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.activity.BaseActivity;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.fragment.listener.CallbackRemoteListener;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CallbackFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CallbackFragment extends Fragment implements View.OnClickListener {
    private String mSid;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param callSid Call sid.
     * @return A new instance of fragment CallbackFragment.
     */
    public static CallbackFragment newInstance(String callSid) {
        CallbackFragment fragment = new CallbackFragment();
        Bundle args = new Bundle();
        args.putString(HelpUtils.CALL_SID_KEY, callSid);
        fragment.setArguments(args);
        return fragment;
    }

    public CallbackFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSid = getArguments().getString(HelpUtils.CALL_SID_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragm_callback, container, false);
        view.findViewById(R.id.tv_thanks).setOnClickListener(this);
        return view;
    }

    private void sendCallback() {
        Event event = new Event();
        ProfileProvider provider = null;
        UserAccountManager accountManager = ((MainActivity) getActivity()).getUserAccountManager();
        if (accountManager.getUser() != null
                && accountManager.getUser().getProviders() != null) {
            provider = accountManager.getCurrentProfileProvider();
        }
        if (provider != null) {
            event.setProviderId(provider.getId());
            event.setType(EventType.CALL_BACK);
            event.setCreatedAt(new Date());
            Remote remote = new Remote();
            remote.setAction(RemoteAction.CALL_BACK);
            remote.setData(mSid);
            DataManager.sendAction(mSid, remote, new BaseHandler<>(new CallbackRemoteListener(event, (MainActivity) getActivity()), new TypeReference<Status>() {
            }));
        }
    }

    @Override
    public void onClick(View v) {
        sendCallback();
        ((BaseActivity) getActivity()).getTransitManager().back();
    }
}
