package com.alex.eclinic.view.fragment.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.modelv3.response.Provider;
import com.alex.eclinic.modelv3.response.User;
import com.tokenautocomplete.TokenCompleteTextView;

/**
 * 02.02.16.
 *
 * @author Alexey Vereshchaga
 */
public class ContactsCompletionView extends TokenCompleteTextView<Provider> {
    public ContactsCompletionView(Context context) {
        super(context);
    }

    public ContactsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ContactsCompletionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected View getViewForObject(Provider provider) {
        LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) l.inflate(R.layout.contact_token, (ViewGroup) ContactsCompletionView.this.getParent(), false);
        User user = provider.getUser();
        if (user != null) {
            ((TextView) view.findViewById(R.id.name))
                    .setText(String.format(getContext().getString(R.string.full_name_format),
                            user.getFirstName(), user.getLastName()));

        }

        return view;
    }

    @Override
    protected Provider defaultObject(String completionText) {
        Provider provider = new Provider();
        User user = new User();
        provider.setUser(user);
        return provider;
    }
}
