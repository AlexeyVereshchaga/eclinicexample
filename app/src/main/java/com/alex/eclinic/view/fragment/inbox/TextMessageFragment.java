package com.alex.eclinic.view.fragment.inbox;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.EventType;
import com.alex.eclinic.modelv3.RemoteAction;
import com.alex.eclinic.modelv3.request.Remote;
import com.alex.eclinic.modelv3.response.Event;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.modelv3.response.User;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.activity.BaseActivity;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.fragment.Dialog;
import com.alex.eclinic.view.fragment.listener.OnSentListener;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.Date;

import retrofit2.Call;

import static com.alex.eclinic.network.ErrorCode.NETWORK_NOT_AVAILABLE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TextMessageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TextMessageFragment extends Fragment implements View.OnClickListener {
    // the fragment initialization parameter
    private String mSid;
    private long mLastClickTime;

    private TextView tvCancel;
    private TextView tvSend;
    private EditText etMessage;
    private ProgressBar pbSending;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sid call sid.
     * @return A new instance of fragment TextMessageFragment.
     */
    public static TextMessageFragment newInstance(String sid) {
        TextMessageFragment fragment = new TextMessageFragment();
        Bundle args = new Bundle();
        args.putString(HelpUtils.CALL_SID_KEY, sid);
        fragment.setArguments(args);
        return fragment;
    }

    public TextMessageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSid = getArguments().getString(HelpUtils.CALL_SID_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragm_text_message, container, false);
        tvCancel = (TextView) view.findViewById(R.id.tv_cancel);
        tvSend = (TextView) view.findViewById(R.id.tv_send);
        etMessage = (EditText) view.findViewById(R.id.et_message);
        pbSending = (ProgressBar) view.findViewById(R.id.pb_sending);
        initView();
        return view;
    }

    private void initView() {
        initStateBeforeSend();
        tvCancel.setOnClickListener(this);
        tvSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                ((BaseActivity) getActivity()).getTransitManager().back();
                break;
            case R.id.tv_send:

                String message = etMessage.getText().toString();
                if (message.isEmpty()) {
                    if (getActivity() != null) {
                        Dialog dialog = Dialog.newInstance(null,
                                getActivity().getString(R.string.common_empty_message_dialog_text));
                        dialog.show(getActivity().getFragmentManager(), "0");
                    }
                } else {
                    tvSend.setClickable(false);
                    initStateAfterSend();
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    sendMessage(message);
                }
                break;
        }
    }

    private void initStateBeforeSend() {
        tvSend.setText(getResources().getString(R.string.message_send));
        etMessage.setVisibility(View.VISIBLE);
        pbSending.setVisibility(View.GONE);
        tvSend.setClickable(true);
    }

    private void initStateAfterSend() {
        tvSend.setText(getResources().getString(R.string.message_sending));
        etMessage.setVisibility(View.INVISIBLE);
        pbSending.setVisibility(View.VISIBLE);
    }

    private void sendMessage(String message) {
        User user = ((MainActivity) getActivity()).getUserAccountManager().getUser();
        ProfileProvider profileProvider = ((MainActivity) getActivity()).getUserAccountManager().getCurrentProfileProvider();
        if (user != null && profileProvider != null) {
            Remote remote = new Remote();
            remote.setAction(RemoteAction.TEXT_MESSAGE);
            remote.setData(message);
            Event event = new Event();
            event.setProviderId(profileProvider.getId());
            event.setType(EventType.TEXT_TO_SPEECH);
            event.setCreatedAt(new Date());
            event.setTextToPatient(message);

            DataManager.sendAction(mSid, remote, new BaseHandler<>(
                    new MessageRemoteListener(event, (MainActivity) getActivity()), new TypeReference<Status>() {
            }));
        }
    }

    @Override
    public void onStop() {
        if (etMessage != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etMessage.getWindowToken(), 0);
        }
        super.onStop();
    }

    private class MessageRemoteListener extends BaseRemoteListener<Status> {
        private OnSentListener callback;
        private Event event;

        public MessageRemoteListener(Event event, MainActivity activity) {
            super(activity);
            this.event = event;
            callback = activity.getActionWithFragments();
        }

        @Override
        public void onFailure(Integer failureCode, Call<Status> call, BaseHandler<Status> handler) {
            super.onFailure(failureCode, call, handler);
            if (failureCode.equals(NETWORK_NOT_AVAILABLE.getCode())) {
                if (activity != null) {
                    Dialog dialog = Dialog.newInstance(activity.getString(R.string.message_inet_offline_title),
                            activity.getString(R.string.message_inet_offline_text));
                    dialog.show(activity.getFragmentManager(), "0");
                }
                Log.e(TAG_NETWORK_NOT_AVAILABLE, NETWORK_NOT_AVAILABLE.getMessage());
                if (activity.getFragmentManager().findFragmentById(R.id.fragment_container_external) != null
                        && activity.getFragmentManager().findFragmentById(R.id.fragment_container_external).getClass().getName()
                        .equals(TextMessageFragment.class.getName())) {
                    initStateBeforeSend();
                }
            }
        }

        @Override
        public void onFailure(Throwable throwable) {
            super.onFailure(throwable);
            if (activity != null) {
                if (activity.getFragmentManager().findFragmentById(R.id.fragment_container_external) != null
                        && activity.getFragmentManager().findFragmentById(R.id.fragment_container_external).getClass().getName()
                        .equals(TextMessageFragment.class.getName())) {
                    initStateBeforeSend();
                }
            }
        }

        @Override
        public void onSuccess(Status result) {
            if (activity != null && result.getStatus().equals("OK")) {
                Toast.makeText(activity, R.string.message_sending_success, Toast.LENGTH_SHORT).show();
                callback.onSent();
                if (activity.getFragmentManager().findFragmentById(R.id.fragment_container_external) != null
                        && activity.getFragmentManager().findFragmentById(R.id.fragment_container_external).getClass().getName()
                        .equals(TextMessageFragment.class.getName())) {
                    ((BaseActivity) activity).getTransitManager().back();
                }
            }
        }
    }

}
