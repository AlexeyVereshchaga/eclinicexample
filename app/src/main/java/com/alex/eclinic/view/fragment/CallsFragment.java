package com.alex.eclinic.view.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.response.Call;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.UserProfile;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.utils.ExpandAnimation;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.adapter.CallsAdapter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import static com.alex.eclinic.controller.HeaderController.LEFT_BTN_ID;
import static com.alex.eclinic.controller.HeaderController.RIGHT_BTN_ID;
import static com.alex.eclinic.controller.transit.FragmentAction.CALL_DETAILS_ACTION;

/**
 * Activity host for the main part of the application
 *
 * @author Alexey Vereshchaga
 */
public abstract class CallsFragment extends AbstractFragment<MainActivity> implements View.OnClickListener {

    protected ListView lvCalls;
    protected CallsAdapter callsAdapter;
    protected SwipeRefreshLayout refreshLayoutCalls;
    private Boolean needUpdate = true;
    private final int ANIMATION_DURATION = 500;
    private int height;

    @Override
    protected int getViewLayout() {
        return R.layout.fragm_calls;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getProvidersController() != null) {
            getProvidersController().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    getProvidersController().getListView().setVisibility(View.GONE);
                    ProfileProvider provider = getProvidersController().getProfileProvidersAdapter().getItem(position);
                    getBaseActivity().getUserAccountManager().setCurrentProfileProvider(provider);
                    getBaseActivity().getUserAccountManager().clearCommonInfo();
                    getCalls();
                    if (callsAdapter != null) {
                        callsAdapter.clear();
                    }
                }
            });
        }
    }

    @Override
    protected void assignViews(View view) {
        lvCalls = (ListView) view.findViewById(android.R.id.list);
        refreshLayoutCalls = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
    }

    @Override
    protected void initView(View view) {
        callsAdapter = new CallsAdapter((MainActivity) getActivity(), R.layout.item_call);

        refreshLayoutCalls.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUserProfile();
                getProviders();
                getCalls();
                getIncompleteCalls();
            }
        });
        lvCalls.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvCalls.setEmptyView(view.findViewById(android.R.id.empty));
        lvCalls.setAdapter(callsAdapter);

        List<Call> calls = getSavedCalls();
        if (calls != null) {
            callsAdapter.addAll(calls);
        }
    }

    protected void setOnItemClickListener() {
        lvCalls.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                lvCalls.setOnItemClickListener(null);
                switchCallDetails(position);
            }
        });
    }

    protected void switchCallDetails(int position) {
        Bundle bundle = new Bundle();
        ObjectMapper mapper = new ObjectMapper();
        try {
            bundle.putString(HelpUtils.CALL_DETAILS_KEY, mapper.writeValueAsString(callsAdapter.getItem(position)));
        } catch (JsonProcessingException e) {
            Log.e(CallsFragment.class.getSimpleName(), "", e);
        }
        getTransitManager().switchFragment(CallsFragment.this, CALL_DETAILS_ACTION, bundle);
    }

    private void getProviders() {
        final MainActivity mainActivity = getBaseActivity();
        if (mainActivity != null
                && mainActivity.getUserAccountManager() != null) {
            mainActivity.getUserAccountManager().requestProviders();
        }
    }

    private void getIncompleteCalls() {
        final MainActivity mainActivity = getBaseActivity();
        if (mainActivity != null
                && mainActivity.getUserAccountManager() != null) {
            mainActivity.getUserAccountManager().requestIncompleteCalls();
        }
    }

    private void getUserProfile() {
        final MainActivity mainActivity = getBaseActivity();
        if (mainActivity != null && mainActivity.getUserAccountManager() != null
                && mainActivity.getUserAccountManager().getUser() == null) {
            DataManager.getUserProfile(new BaseHandler<>(new BaseRemoteListener<UserProfile>(getActivity()) {
                @Override
                public void onSuccess(UserProfile result) {
                    if (result != null
                            && result.getUser() != null
                            && mainActivity != null
                            && getProvidersController() != null) {
                        mainActivity.getUserAccountManager().setUser(result.getUser());
                        mainActivity.getUserAccountManager().setProfileProviders(result.getUser().getProviders());
                        mainActivity.getUserAccountManager().updateCurrentProfileProvider();
                        getProvidersController().getProfileProvidersAdapter().clear();
                        getProvidersController().getProfileProvidersAdapter().addAll(result.getUser().getProviders());
                        mainActivity.getSlidingMenu().setName(result.getUser().getFirstName() + " " + result.getUser().getLastName());
                    }
                }
            }, new TypeReference<UserProfile>() {
            }));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setLeftButton(this, true, R.drawable.bar_item_sidebar, null);
            if (getBaseActivity().getUserAccountManager() != null
                    && getBaseActivity().getUserAccountManager().getProfileProviders() != null
                    && getBaseActivity().getUserAccountManager().getProfileProviders().size() > 1) {
                getHeaderController().setRightButton(this, true, R.drawable.ic_user_practices, null);
            }
            if (getBaseActivity().getUserAccountManager().getTotalIncomplete() > 0
                    && getBaseActivity().getUserAccountManager().getProfileProviders() != null
                    && getBaseActivity().getUserAccountManager().getProfileProviders().size() > 1) {
                getHeaderController().getCallsNumberView().setVisibility(View.VISIBLE);
            }
        }
        getUserProfile();
        getProviders();
        getIncompleteCalls();
        getCalls();
        refreshLayoutCalls.setRefreshing(false);
//        getCalls();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case LEFT_BTN_ID:
                getBaseActivity().getSlidingMenu().getSideDrawer().toggleLeftDrawer();
                break;
            case RIGHT_BTN_ID:
                if (getProvidersController() != null) {
                    ListView listView = getProvidersController().getListView();
                    if (listView.getVisibility() == View.INVISIBLE) {
                        //todo ambiguous temporary solution
                        ExpandAnimation a = new ExpandAnimation(listView, 0, ExpandAnimation.COLLAPSE);
                        height = a.getHeight();
                        listView.startAnimation(a);
                        ExpandAnimation a1 = new ExpandAnimation(listView, ANIMATION_DURATION, ExpandAnimation.EXPAND);
                        a1.setHeight(height);
                        listView.startAnimation(a1);
                    } else if (listView.getVisibility() == View.VISIBLE) {
                        ExpandAnimation a = new ExpandAnimation(listView, ANIMATION_DURATION, ExpandAnimation.COLLAPSE);
                        height = a.getHeight();
                        listView.startAnimation(a);
                    } else {
                        if (listView.getHeight() != 0) {
                            height = listView.getHeight();
                        }
                        ExpandAnimation a = new ExpandAnimation(listView, ANIMATION_DURATION, ExpandAnimation.EXPAND);
                        a.setHeight(height);
                        listView.startAnimation(a);
                    }
                }
                break;
        }
    }

//    listView.setVisibility(listView.getVisibility() == View.VISIBLE
//    ? View.GONE
//    : View.VISIBLE);

    @Override
    public void onPause() {
        refreshLayoutCalls.setRefreshing(false);
        super.onPause();
    }

//    public void removeCall(String sId) {
//        if (callsAdapter != null && getSavedCalls() != null) {
//            for (Call call : getSavedCalls()) {
//                if (call.getSid().equals(sId)) {
//                    getSavedCalls().remove(call);
//                    callsAdapter.remove(call);
//                    callsAdapter.notifyDataSetChanged();
//                    return;
//                }
//            }
//        }
//    }

    public Boolean isNeedUpdate() {
        return needUpdate;
    }

    public void setNeedUpdate(Boolean needUpdate) {
        this.needUpdate = needUpdate;
    }

    public abstract void getCalls();

    public void setRefreshing(final boolean refreshing) {
        refreshLayoutCalls.setRefreshing(refreshing);
    }

    protected abstract List<Call> getSavedCalls();

}
