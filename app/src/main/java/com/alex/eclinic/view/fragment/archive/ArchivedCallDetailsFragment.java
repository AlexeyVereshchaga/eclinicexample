package com.alex.eclinic.view.fragment.archive;

import com.alex.eclinic.R;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.fragment.CallDetailsFragment;

/**
 * Class description
 * 14.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class ArchivedCallDetailsFragment extends CallDetailsFragment {

    @Override
    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setLeftButton(this, true, 0, getActivity().getString(R.string.fragm_header_common_back));
            getHeaderController().setTitle(HelpUtils.getPhoneNumber(call.getFrom()));
            getHeaderController().setRightButton(this, true, R.drawable.call_details_icon, null);
        }
        ivOptionForward.setClickable(false);
    }
}
