package com.alex.eclinic.view.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alex.eclinic.BuildConfig;
import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.response.Access;
import com.alex.eclinic.modelv3.response.Response;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.network.Server;
import com.alex.eclinic.utils.Constant;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.utils.SharedPreferencesHelper;
import com.alex.eclinic.view.activity.StartActivity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Date;

import retrofit2.Call;


/**
 * Class description
 * 30.09.15.
 *
 * @author Alexey Vereshchaga
 */
public class LoginFragment extends AbstractFragment<StartActivity> implements View.OnClickListener {

    public static final String PROFILE = "Profile";
    private Boolean loginState = true;

    private EditText etEmail;
    private EditText etPassword;
    private Button btnSignIn;
    private ImageView ivLogo;
    private TextView tvForgotPass;
    private Spinner spinner;
    private Call request;


    @Override
    protected int getViewLayout() {
        return R.layout.fragm_login;
    }

    @Override
    protected void assignViews(View view) {
        etEmail = (EditText) view.findViewById(R.id.et_email_address);
        etPassword = (EditText) view.findViewById(R.id.et_password);
        btnSignIn = (Button) view.findViewById(R.id.btn_sign_in);
        ivLogo = (ImageView) view.findViewById(R.id.iv_logo);
        tvForgotPass = (TextView) view.findViewById(R.id.tv_forgot_pass);
        spinner = (Spinner) view.findViewById(R.id.spinner_status);
    }

    @Override
    protected void initView(View view) {
        if (SharedPreferencesHelper.getPin() != null
                && !SharedPreferencesHelper.getAccessToken().isEmpty()
                && !SharedPreferencesHelper.getRefreshToken().isEmpty()) {
            Bundle bundle = new Bundle();
            bundle.putString(HelpUtils.TYPE_EXTRA_KEY, getActivity().getIntent().getStringExtra(HelpUtils.TYPE_EXTRA_KEY));
            getBaseActivity().startMainActivity(bundle);
        }
        ivLogo.setOnClickListener(this);
        btnSignIn.setOnClickListener(this);
        btnSignIn.setClickable(false);
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activateSignInButton();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        etEmail.addTextChangedListener(watcher);
        etPassword.addTextChangedListener(watcher);
        tvForgotPass.setOnClickListener(this);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.servers, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        if (BuildConfig.DEBUG) {
            spinner.setVisibility(View.VISIBLE);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    switch (position) {
                        case 0:
                            SharedPreferencesHelper.saveServer(Server.PROD);
                            break;
                        case 1:
                            SharedPreferencesHelper.saveServer(Server.DEMO);
                            break;
                        case 2:
                            SharedPreferencesHelper.saveServer(Server.REVIEW);
                            break;
                        default:
                            SharedPreferencesHelper.saveServer(Server.PROD);
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void activateSignInButton() {
        boolean email = HelpUtils.isValidEmail(etEmail.getText());
        boolean pass = HelpUtils.isValidPassword(etPassword.getText());
        btnSignIn.setClickable(loginState ? email && pass : email);
    }

    @Override
    public void onPause() {
        super.onPause();
        activateSignInButton();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_in:
                setViewsState(false);
                if (request != null) {
                    request.cancel();
                }
                btnSignIn.setText(getActivity().getResources().getText(R.string.login_working));
                if (loginState) {
                    loginV3();
                } else {
                    resetPassword();
                }
                break;
            case R.id.tv_forgot_pass:
                changeLoginViewsText();
                activateSignInButton();
                break;
        }
    }

    private void resetPassword() {
        DataManager.resetPassword(etEmail.getText().toString(), new BaseHandler<Response>(
                new BaseRemoteListener<com.alex.eclinic.modelv3.response.Response>(getActivity()) {
                    @Override
                    public void onSuccess(com.alex.eclinic.modelv3.response.Response result) {
                        if (result != null) {
                            Toast.makeText(getActivity(), result.getSuccess(), Toast.LENGTH_LONG).show();
                        }
                        changeLoginViewsText();
                        setViewsState(true);
                    }

                    @Override
                    public void onFinishTask() {
                        changeLoginViewsText();
                        setViewsState(true);
                    }
                }, new TypeReference<Response>() {
        }));
    }

    private void loginV3() {
        request = DataManager.login(etEmail.getText().toString(), etPassword.getText().toString(),
                new BaseHandler<>(new BaseRemoteListener<Access>(getActivity()) {
                    @Override
                    public void onSuccess(Access result) {
                        if (result != null) {
                            SharedPreferencesHelper.saveExpiredAt(new Date().getTime() + result.getExpiresIn() * 1000);
                            SharedPreferencesHelper.saveAccessToken(result.getAccessToken());
                            SharedPreferencesHelper.saveRefreshToken(result.getRefreshToken());
                            Bundle bundle = new Bundle();
                            ObjectMapper mapper = new ObjectMapper();
                            try {
                                bundle.putString(PROFILE, mapper.writeValueAsString(result.getUser()));
                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                            getBaseActivity().startMainActivity(bundle);
                        }
                    }

                    @Override
                    public void onFailure(Integer failureCode, Call<Access> call, BaseHandler<Access> handler) {
                        changeLoginViewsText();
                        setViewsState(true);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        super.onFailure(throwable);
                        SharedPreferencesHelper.deleteAccessToken();
                        SharedPreferencesHelper.deleteRefreshToken();
                        changeLoginViewsText();
                        setViewsState(true);
                    }

                    @Override
                    public void onFailure(Integer errorCode, Access error) {
                        if (error != null && error.getErrorDescription() != null) {
                            Dialog dialog = Dialog.newInstance(activity.getString(R.string.common_error),
                                    error.getErrorDescription());
                            dialog.show(getFragmentManager(), Constant.DIALOG_TAG);
                        }
                    }
                }, new TypeReference<Access>() {
                }));
    }

    private void setViewsState(Boolean state) {
        btnSignIn.setClickable(state);
        etPassword.setEnabled(state);
        etEmail.setEnabled(state);
    }

    private void changeLoginViewsText() {
        etPassword.setVisibility(loginState ? View.INVISIBLE : View.VISIBLE);
        btnSignIn.setText(loginState ? R.string.reset_button : R.string.login_sign_in);
        tvForgotPass.setText(loginState ? R.string.login_cancel_recovery : R.string.login_forgot_password);
        loginState = !loginState;
    }
}
