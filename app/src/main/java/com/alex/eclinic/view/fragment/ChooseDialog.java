package com.alex.eclinic.view.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.alex.eclinic.R;

/**
 * 08.02.16.
 *
 * @author Alexey Vereshchaga
 */
public class ChooseDialog extends DialogFragment implements View.OnClickListener {

    private Button btnGallery;
    private Button btnCamera;
    private Button btnCancel;

    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
//        String title = getResources().getString(R.string.forward_take_photo);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
//        builder.setPositiveButton(R.string.choose_ok, null);
//        builder.setNegativeButton(R.string.choose_cancel, null);
//        builder.setTitle(title);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View v = inflater.inflate(R.layout.dialog_choose, null);
        v.findViewById(R.id.btn_gallery).setOnClickListener(this);
        v.findViewById(R.id.btn_camera).setOnClickListener(this);
        v.findViewById(R.id.btn_cancel).setOnClickListener(this);
        builder.setView(v);
        return builder.create();
    }

    @Override
    public void onClick(View v) {
        ForwardFragment fragment = (ForwardFragment) getFragmentManager().findFragmentByTag(ForwardFragment.class.getName());
        switch (v.getId()) {
            case R.id.btn_gallery:
                if (fragment != null) {
                    dismiss();
                    fragment.choosePhoto();
                }
                break;
            case R.id.btn_camera:
                if (fragment != null) {
                    dismiss();
                    fragment.dispatchTakePictureIntent();
                }
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }
}
