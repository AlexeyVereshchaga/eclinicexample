package com.alex.eclinic.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.RemoteAction;
import com.alex.eclinic.modelv3.request.Remote;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.activity.BaseActivity;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.fragment.listener.BaseActionRemoteListener;
import com.fasterxml.jackson.core.type.TypeReference;

import retrofit2.Call;

import static com.alex.eclinic.controller.HeaderController.LEFT_BTN_ID;
import static com.alex.eclinic.controller.HeaderController.RIGHT_BTN_ID;


/**
 * Class description
 * 19.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class NoteFragment extends AbstractFragment<MainActivity> implements View.OnClickListener {

    private String mSid;
    private Call mCall;
    private long mLastClickTime;
    private EditText etNote;
    private ProgressBar pbSending;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSid = getArguments().getString(HelpUtils.CALL_SID_KEY);
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragm_note;
    }

    @Override
    protected void assignViews(View view) {
        etNote = (EditText) view.findViewById(R.id.et_forward);
        pbSending = (ProgressBar) view.findViewById(R.id.pb_sending);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setTitle(getActivity().getString(R.string.note_title));
            getHeaderController().setLeftButton(this, true, getActivity().getString(R.string.note_cancel));
            getHeaderController().setRightButton(this, true, getActivity().getString(R.string.note_send));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case LEFT_BTN_ID:
                getTransitManager().back();
                break;
            case RIGHT_BTN_ID:
                if (mCall != null) {
                    mCall.cancel();
                }
                String noteText = etNote.getText().toString();
                if (noteText.isEmpty()) {
                    if (getActivity() != null) {
                        Dialog dialog = Dialog.newInstance(null,
                                getActivity().getString(R.string.common_empty_message_dialog_text));
                        dialog.show(getActivity().getFragmentManager(), "0");
                    }
                } else {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    pbSending.setVisibility(View.VISIBLE);
                    getHeaderController().getRightButton().setClickable(false);
                    Remote remote = new Remote();
                    remote.setAction(RemoteAction.NOTE);
                    remote.setMessage(noteText);
                    mCall = DataManager.sendAction(mSid, remote, new BaseHandler<>(
                            new NoteRemoteListener(getBaseActivity()), new TypeReference<Status>() {
                    }));
                }
                break;
        }
    }

    private class NoteRemoteListener extends BaseActionRemoteListener {

        public NoteRemoteListener(MainActivity activity) {
            super(activity, NoteFragment.class);
        }

        @Override
        public void onFinishTask() {
            if (activity != null && ((BaseActivity) activity).getHeaderController() != null) {
                ((BaseActivity) activity).getHeaderController().getRightButton().setClickable(true);
                if (pbSending != null) {
                    pbSending.setVisibility(View.GONE);
                }
            }
        }

        @Override
        protected int getSuccessStringRes() {
            return R.string.note_send_success;
        }

    }
}
