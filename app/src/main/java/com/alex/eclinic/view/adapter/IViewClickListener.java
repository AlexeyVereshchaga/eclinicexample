package com.alex.eclinic.view.adapter;

import android.view.View;

/**
 * Class description
 * 12.10.15.
 *
 * @author Alexey Vereshchaga
 */
public interface IViewClickListener<T, K> {

    void clickView(T adapter, K entity, View view);
}
