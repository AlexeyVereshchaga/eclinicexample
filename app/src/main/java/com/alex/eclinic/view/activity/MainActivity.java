package com.alex.eclinic.view.activity;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.ProfileProvidersController;
import com.alex.eclinic.controller.SlidingMenuController;
import com.alex.eclinic.controller.UserAccountManager;
import com.alex.eclinic.controller.transit.MainTransitManager;
import com.alex.eclinic.gcm.RegistrationIntentService;
import com.alex.eclinic.modelv3.response.User;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.utils.SharedPreferencesHelper;
import com.alex.eclinic.view.fragment.CallsFragment;
import com.alex.eclinic.view.fragment.LoginFragment;
import com.alex.eclinic.view.fragment.inbox.InboxFragment;
import com.alex.eclinic.view.fragment.pin.ConfirmPinFragment;
import com.alex.eclinic.view.fragment.pin.CreatePinFragment;
import com.alex.eclinic.view.fragment.pin.PinFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 * 30.09.15.
 *
 * @author Alexey Vereshchaga
 */
public class MainActivity extends BaseActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = MainActivity.class.getName();

    private SlidingMenuController slidingMenu;
    private UserAccountManager userAccountManager = new UserAccountManager(this);
    private ActionWithFragments actionWithFragments;
    private Boolean paused;
    private ProfileProvidersController profileProvidersController;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!getTransitManager().getCurrentFragment().getClass().getName().equals(CreatePinFragment.class.getName())
                    && !getTransitManager().getCurrentFragment().getClass().getName().equals(ConfirmPinFragment.class.getName())) {
                Intent lockIntent = new Intent(getBaseContext(), LockActivity.class);
                lockIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(lockIntent);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPartnersController();
        if (getIntent().getExtras() != null && getIntent().getExtras().getString(LoginFragment.PROFILE) != null) {
            User user = HelpUtils.getObjectFromJsonString(
                    getIntent().getExtras().getString(LoginFragment.PROFILE),
                    User.class);
            if (user != null) {
                userAccountManager.setUser(user);
                userAccountManager.setProfileProviders(user.getProviders());
                getProfileProvidersController().getProfileProvidersAdapter().clear();
                getProfileProvidersController().getProfileProvidersAdapter().addAll(userAccountManager.getProfileProviders());
            }
        }

        slidingMenu = new SlidingMenuController(this);
        if (userAccountManager != null
                && userAccountManager.getUser() != null) {
            slidingMenu.setName(userAccountManager.getUser().getFirstName() + " " + userAccountManager.getUser().getLastName());
        }
        if (getFragmentForStart().getName().equals(InboxFragment.class.getName())) {
            View view = slidingMenu.getSideDrawer().getLeftBehindView();
            view.findViewById(R.id.v_inbox).setVisibility(View.VISIBLE);
        }
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        startRequests();
        actionWithFragments = new ActionWithFragments(this);
    }

    private void initPartnersController() {
        if (getHeaderViewIdRes() != 0) {
            profileProvidersController = new ProfileProvidersController(getApplicationContext());
            profileProvidersController.setListView((ListView) findViewById(R.id.header_list_view));
            profileProvidersController.init();
        }
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(HelpUtils.FOREGROUND_EVENT));
        paused = false;
        super.onResume();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getStringExtra(HelpUtils.TYPE_EXTRA_KEY) != null) {
            getTransitManager().switchBranch(InboxFragment.class);
            CallsFragment callsFragment = (CallsFragment) getFragmentManager().findFragmentByTag(CallsFragment.class.getName());
            if (callsFragment != null) {
                callsFragment.getCalls();
            }
        }
        super.onNewIntent(intent);
    }

    @Override
    protected void onPause() {
        // Unregister since the activity is paused.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        paused = true;
        super.onPause();
    }

    @Override
    protected int getActivityLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected int getHeaderViewIdRes() {
        return R.id.header;
    }

    @Override
    protected int getMainFragmentContainerRes() {
        return R.id.fragment_container;
    }

    @Override
    protected void createTransitManager() {
        transitManager = new MainTransitManager(this);
    }

    @Override
    protected Class<? extends Fragment> getFragmentForStart() {
        return SharedPreferencesHelper.getPin() == null ? CreatePinFragment.class : InboxFragment.class;
    }

    private void startRequests() {
        if (userAccountManager != null) {
            userAccountManager.requestUser();
            userAccountManager.requestProviders();
        }
    }

    public SlidingMenuController getSlidingMenu() {
        return slidingMenu;
    }

    public Boolean isPaused() {
        return paused;
    }

    public ActionWithFragments getActionWithFragments() {
        return actionWithFragments;
    }

    public UserAccountManager getUserAccountManager() {
        return userAccountManager;
    }

    public ProfileProvidersController getProfileProvidersController() {
        return profileProvidersController;
    }

    @Override
    protected void onDestroy() {
        HelpUtils.clearExternalCache(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0
                && slidingMenu.getSideDrawer().isClosed()
                && !(getTransitManager().getCurrentFragment() instanceof PinFragment)) {
            slidingMenu.getSideDrawer().toggleLeftDrawer();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}

