package com.alex.eclinic.view.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import com.alex.eclinic.controller.HeaderController;
import com.alex.eclinic.controller.transit.FragmentAction;
import com.alex.eclinic.view.fragment.CallsFragment;
import com.alex.eclinic.view.fragment.archive.ArchivedCallsFragment;
import com.alex.eclinic.view.fragment.forward.ForwardedCallsFragment;
import com.alex.eclinic.view.fragment.inbox.InboxFragment;
import com.fsm.transit.core.ITransitManager;

/**
 * 30.09.15.
 *
 * @author Alexey Vereshchaga
 */
public abstract class BaseActivity extends Activity {

    protected ITransitManager<FragmentAction> transitManager;
    protected HeaderController headerController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getActivityLayoutRes());
        createTransitManager();
        getTransitManager().setCurrentContainer(getMainFragmentContainerRes());
        initHeaderController();
        String fragmentTag = getFragmentForStart().getName();
        if (fragmentTag.equals(InboxFragment.class.getName())
                || fragmentTag.equals(ArchivedCallsFragment.class.getName())
                || fragmentTag.equals(ForwardedCallsFragment.class.getName())) {
            fragmentTag = CallsFragment.class.getName();
        }
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(getMainFragmentContainerRes(), Fragment.instantiate(this, getFragmentForStart().getName()), fragmentTag)
                    .commitAllowingStateLoss();
        }
    }

    /**
     * set layout resource for current activity (for setContentView method)
     *
     * @return layout resource
     */
    protected abstract int getActivityLayoutRes();

    /**
     * set FrameLayout id for fragment container
     *
     * @return id for transit manager
     */
    protected abstract int getMainFragmentContainerRes();

    /**
     * create concrete TransitManager object for activity page transitions
     */
    protected abstract void createTransitManager();

    /**
     * method provide access to transit manager object for current activity
     *
     * @return TransitManager object, what manipulate fragments
     */
    public ITransitManager<FragmentAction> getTransitManager() {
        return transitManager;
    }

    /**
     * set fragment for start after activity open
     *
     * @return Fragment for start
     */
    protected abstract Class<? extends Fragment> getFragmentForStart();


    protected int getHeaderViewIdRes() {
        return 0;
    }

    protected void initHeaderController() {
        if (getHeaderViewIdRes() != 0) {
            headerController = new HeaderController(getApplicationContext());
            getHeaderController().setViewHeader(findViewById(getHeaderViewIdRes()));
            getHeaderController().setTransitManager(transitManager);
            getHeaderController().init();
        }
    }

    public HeaderController getHeaderController() {
        return headerController;
    }


    /**
     * start main activity and finish current
     *
     * @param src args
     */
    public void startMainActivity(Bundle src) {
        Intent intent = new Intent(this, MainActivity.class);
        if (src != null) {
            intent.putExtras(src);
        }
        startActivity(intent);
        finish();
    }

    public void startMainActivity() {
        startMainActivity(null);
    }
}
