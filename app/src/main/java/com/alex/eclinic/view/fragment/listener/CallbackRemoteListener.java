package com.alex.eclinic.view.fragment.listener;

import android.util.Log;

import com.alex.eclinic.R;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.modelv3.response.Event;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.fragment.Dialog;

import retrofit2.Call;

import static com.alex.eclinic.network.ErrorCode.NETWORK_NOT_AVAILABLE;

/**
 * 27.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class CallbackRemoteListener extends BaseRemoteListener<Status> {
    private OnSentListener callback;
    private Event event;

    public CallbackRemoteListener(Event event, MainActivity activity) {
        super(activity);
        this.event = event;
        callback = activity.getActionWithFragments();
    }

    @Override
    public void onFailure(Integer failureCode, Call<Status> call, BaseHandler<Status> handler) {
        super.onFailure(failureCode, call, handler);
        if (activity != null) {
//            backFromCallback();
            if (failureCode.equals(NETWORK_NOT_AVAILABLE.getCode())) {
                Dialog dialog = Dialog.newInstance(activity.getString(R.string.callback_offline_inet_title),
                        activity.getString(R.string.callback_offline_inet_text));
                dialog.show(activity.getFragmentManager(), "0");
                Log.e(TAG_NETWORK_NOT_AVAILABLE, NETWORK_NOT_AVAILABLE.getMessage());
            }
        }
    }

    @Override
    public void onSuccess(Status result) {
        if (activity != null && result.getStatus().equals("OK")) {
            callback.onSent();
        }
    }

//    private void backFromCallback() {
//        BaseActivity baseActivity = (BaseActivity) activity;
//        if (activity.getFragmentManager() != null
//                && activity.getFragmentManager().findFragmentById(R.id.fragment_container_external)
//                .getClass().getName().equals(CallbackFragment.class.getName())) {
//            baseActivity.getTransitManager().back();
//        }
//    }
}

