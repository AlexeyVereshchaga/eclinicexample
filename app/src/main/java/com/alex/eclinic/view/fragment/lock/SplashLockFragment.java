package com.alex.eclinic.view.fragment.lock;

import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.response.Response;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.view.activity.LockActivity;
import com.alex.eclinic.view.fragment.AbstractFragment;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.Date;

import retrofit2.Call;

/**
 * 09.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class SplashLockFragment extends AbstractFragment<LockActivity> {
    private static final int DELAY_MILLIS = 10000;
    public static final int DEFAULT_SPLASH_VISIBILITY = 1500;
    private ProgressBar progressBar;
    private Call call;
    private Handler handler;
    private Long startTime;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            checkServer();
        }
    };

    @Override
    protected int getViewLayout() {
        return R.layout.fragm_splash_lock;
    }

    @Override
    protected void assignViews(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (call == null) {
            checkServer();
        }
    }

    private void checkServer() {
        call = DataManager.checkServer(new BaseHandler<>(new BaseRemoteListener<Response>(getActivity()) {

            @Override
            public void onStartTask() {
                if (progressBar != null) {
                    progressBar.setVisibility(View.VISIBLE);
                }
                startTime = new Date().getTime();
            }

            @Override
            public void onSuccess(Response result) {
                long delay = 0;
                long currentTime = new Date().getTime();
                if (currentTime - startTime < DEFAULT_SPLASH_VISIBILITY) {
                    delay = DEFAULT_SPLASH_VISIBILITY - (currentTime - startTime);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        removeFragment();
                    }
                }, delay);
            }

            @Override
            public void onFinishTask() {
                if (progressBar != null) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                super.onFailure(throwable);
                if (getActivity() != null) {
                    handler = new Handler();
                    handler.postDelayed(runnable, DELAY_MILLIS);
                } else if (handler != null) {
                    handler.removeCallbacks(runnable);
                    handler = null;
                }
            }
        }, new TypeReference<Response>() {
        }));
    }

    private void removeFragment() {
        if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction().remove(SplashLockFragment.this).commitAllowingStateLoss();
        }
    }
}
