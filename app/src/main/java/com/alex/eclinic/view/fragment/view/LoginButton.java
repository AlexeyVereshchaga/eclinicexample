package com.alex.eclinic.view.fragment.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.alex.eclinic.R;

/**
 * Class description
 * 09.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class LoginButton extends Button {
    private static final int[] STATE_CLICKABLE = {R.attr.state_clickable};

    public LoginButton(Context context) {
        super(context);
    }

    public LoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoginButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int[] onCreateDrawableState(final int extraSpace) {
        if (isClickable()) {
            final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
            mergeDrawableStates(drawableState, STATE_CLICKABLE);
            return drawableState;
        } else {
            return super.onCreateDrawableState(extraSpace);
        }
    }

    @Override
    public void setClickable(final boolean clickable) {
        super.setClickable(clickable);
        refreshDrawableState();
    }
}
