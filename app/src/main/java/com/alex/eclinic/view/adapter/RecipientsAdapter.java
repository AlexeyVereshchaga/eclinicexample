package com.alex.eclinic.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.modelv3.response.Provider;
import com.alex.eclinic.view.activity.MainActivity;

/**
 * 23.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class RecipientsAdapter extends ArrayAdapter<Provider> {

    public RecipientsAdapter(MainActivity mainActivity, int resource) {
        super(mainActivity, resource);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        TextView rowView = (TextView) inflater.inflate(R.layout.spinner_item, parent, false);
        rowView.setText(String.format(getContext().getResources().getString(R.string.full_name_format),
                getItem(position).getUser().getFirstName(),
                getItem(position).getUser().getLastName()));
        return rowView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        CheckedTextView dropDownView = (CheckedTextView) inflater.inflate(R.layout.simple_spinner_dropdown_item, parent, false);
        dropDownView.setText(String.format(getContext().getResources().getString(R.string.full_name_format),
                getItem(position).getUser().getFirstName(),
                getItem(position).getUser().getLastName()));
        return dropDownView;
    }
}
