package com.alex.eclinic.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alex.eclinic.R;
import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.RemoteAction;
import com.alex.eclinic.modelv3.request.Remote;
import com.alex.eclinic.modelv3.response.Event;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.Provider;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.activity.BaseActivity;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.adapter.RecipientsAdapter;
import com.alex.eclinic.view.fragment.listener.OnSentListener;
import com.alex.eclinic.view.fragment.view.NoDefaultSpinner;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;

import static com.alex.eclinic.controller.HeaderController.LEFT_BTN_ID;
import static com.alex.eclinic.controller.HeaderController.RIGHT_BTN_ID;
import static com.alex.eclinic.network.ErrorCode.NETWORK_NOT_AVAILABLE;

/**
 * 28.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class ForwardFragment extends AbstractFragment<MainActivity> implements View.OnClickListener {

    public static final String ACTION_KEY = "Action";

    private final static String TAG = ForwardFragment.class.getSimpleName();
    private static final int PICK_IMAGE = 1;
    private static final int REQUEST_TAKE_PHOTO = 2;
    public static final String URGENT_STATUS = "urgent";
    public static final String NON_URGENT_STATUS = "non-urgent";
    public static final String EXTERNAL_PROVIDER = "External Provider";

    private List<String> photos;
    private List<String> statuses;
    private RemoteAction remoteAction;

    //    private ContactsCompletionView contactsCompletionView;
    private EditText etForward;
    private ProgressBar pbSending;
    private NoDefaultSpinner spinnerStatus;
    private NoDefaultSpinner spinnerRecipient;
    //    private ImageView ivSearch;
    private ImageView ivAttachment;
    private TextView etAttachment;

    private String mSid;
    private long mLastClickTime;
    private Integer providerId;
    private String mCurrentPhotoPath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSid = getArguments().getString(HelpUtils.CALL_SID_KEY);
            remoteAction = RemoteAction.fromString(getArguments().getString(ACTION_KEY));
            statuses = new ArrayList<>();
            setStatuses(remoteAction);
        }
        setHasOptionsMenu(true);
    }

    //// TODO: 10.02.16 temporary hardcode
    private void setStatuses(RemoteAction remoteAction) {
        switch (remoteAction) {
            case URGENT_FORWARD:
                statuses.add(URGENT_STATUS);
                break;
            case FORWARD:
                statuses.add(NON_URGENT_STATUS);
                break;
            default:
                statuses.add(URGENT_STATUS);
                statuses.add(NON_URGENT_STATUS);
        }
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragm_forward;
    }

    @Override
    protected void assignViews(View view) {
        etForward = (EditText) view.findViewById(R.id.et_forward);
        pbSending = (ProgressBar) view.findViewById(R.id.pb_sending);
//        contactsCompletionView = (ContactsCompletionView) view.findViewById(R.id.ccv_recipient);
        spinnerStatus = (NoDefaultSpinner) view.findViewById(R.id.spinner_status);
        spinnerRecipient = (NoDefaultSpinner) view.findViewById(R.id.spinner_recipient);
//        ivSearch = (ImageView) view.findViewById(R.id.iv_search);
        ivAttachment = (ImageView) view.findViewById(R.id.iv_attach);
        etAttachment = (TextView) view.findViewById(R.id.et_attachment);
    }

    @Override
    protected void initView(View view) {
        MainActivity mainActivity = getBaseActivity();
        List<Provider> providers = mainActivity.getUserAccountManager().getProviders();
        final List<Provider> filteredProviders = filterProviders(providers);
        if (filteredProviders != null) {
            final RecipientsAdapter adapterRecipients = new RecipientsAdapter(getBaseActivity(), R.layout.spinner_item);
            adapterRecipients.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            adapterRecipients.addAll(filteredProviders);
            spinnerRecipient.setAdapter(adapterRecipients);
            spinnerRecipient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    providerId = adapterRecipients.getItem(position).getId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
//            RecipientAdapter adapter = new RecipientAdapter(getActivity(), R.layout.item_recipient, providers);
//            contactsCompletionView.setAdapter(adapter);
//            contactsCompletionView.setThreshold(0);
//            contactsCompletionView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
//            contactsCompletionView.allowDuplicates(false);
//            contactsCompletionView.setOnClickListener(this);
        }

        ArrayAdapter<CharSequence> adapterStatus = new ArrayAdapter<>(getActivity(), R.layout.spinner_item);//ArrayAdapter.createFromResource(getActivity(), R.array.forward_statuses, R.layout.spinner_item);
        adapterStatus.addAll(statuses);
        adapterStatus.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setVisibility(View.VISIBLE);
        spinnerStatus.setAdapter(adapterStatus);
        spinnerStatus.setSelection(adapterStatus.getPosition(remoteAction.equals(RemoteAction.FORWARD) ? NON_URGENT_STATUS : URGENT_STATUS));
        if (!remoteAction.equals(RemoteAction.NONE)) {
            spinnerStatus.setEnabled(false);
        }
//        ivSearch.setOnClickListener(this);
        ivAttachment.setOnClickListener(this);
    }

    private List<Provider> filterProviders(List<Provider> providers) {
        List<Provider> filteredProviders = new ArrayList<>();
        ProfileProvider profileProvider = getBaseActivity().getUserAccountManager().getCurrentProfileProvider();
        if (providers != null && profileProvider != null && profileProvider.getPartnerId() != null) {
            for (Provider provider : providers) {
                if (profileProvider.getPartnerId().equals(provider.getPartner().getId())
                        && !provider.getTitle().equalsIgnoreCase(EXTERNAL_PROVIDER)
                        && provider.getUser() != null
                        && !(provider.getUser().getFirstName() == null && provider.getUser().getLastName() == null)) {
                    filteredProviders.add(provider);
                }
            }
        }
        return filteredProviders;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getHeaderController() != null) {
            getHeaderController().setTitle(getActivity().getString(R.string.forward_title));
            getHeaderController().setLeftButton(this, true, getActivity().getString(R.string.forward_cancel));
            getHeaderController().setRightButton(this, true, getActivity().getString(R.string.forward_send));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case LEFT_BTN_ID:
                getTransitManager().back();
                break;
            case RIGHT_BTN_ID:
                if (providerId == null) {
                    Dialog dialog = Dialog.newInstance(null,
                            getActivity().getString(R.string.forward_empty_recipient));
                    dialog.show(getActivity().getFragmentManager(), "0");
                    return;
                }
                String forwardText = etForward.getText().toString();
                if (forwardText.isEmpty()) {
                    if (getActivity() != null) {
                        Dialog dialog = Dialog.newInstance(null,
                                getActivity().getString(R.string.common_empty_message_dialog_text));
                        dialog.show(getActivity().getFragmentManager(), "0");
                    }
                } else {
                    pbSending.setVisibility(View.VISIBLE);
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    getHeaderController().getRightButton().setClickable(false);
                    sendForward(forwardText);
                }
                break;
//            case R.id.iv_search:
//                contactsCompletionView.showDropDown();
//                contactsCompletionView.getObjects();
//                break;
            case R.id.iv_attach:
                ChooseDialog chooseDialog = new ChooseDialog();
                chooseDialog.show(getFragmentManager(), "1");
                break;
        }
    }

    private void sendForward(String message) {
        Remote remote = new Remote();
        //todo
        switch ((String) spinnerStatus.getSelectedItem()) {
            case URGENT_STATUS:
                remote.setAction(RemoteAction.URGENT_FORWARD);
                break;
            case NON_URGENT_STATUS:
                remote.setAction(RemoteAction.FORWARD);
                break;
        }
        remote.setMessage(message);
        remote.setForwardId(providerId);
        Event event = new Event();
        if (getBaseActivity().getUserAccountManager() != null
                && getBaseActivity().getUserAccountManager().getCurrentProfileProvider() != null) {
            event.setProviderId(getBaseActivity().getUserAccountManager().getCurrentProfileProvider().getId());
        }
        event.setForwardedAt(new Date());
        event.setCreatedAt(new Date());
        event.setMessage(message);
        DataManager.sendAction(mSid, remote,
                new BaseHandler<>(new ForwardRemoteListener((MainActivity) getActivity(), mSid, event), new TypeReference<Status>() {
                }));
    }


    public void updateRecipient(Integer providerId) {
        this.providerId = providerId;
    }

    public void choosePhoto() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Log.e(TAG, "Error: ", ex);
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (photos == null) {
            photos = new ArrayList<>();
        }
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PICK_IMAGE:
                    Uri uri = data.getData();
                    // Get the File path from the Uri
                    String path = HelpUtils.getPath(getActivity(), uri);
                    if (path != null) {
                        photos.add(path);

                    }
                    break;
                case REQUEST_TAKE_PHOTO:
                    if (mCurrentPhotoPath != null) {
                        photos.add(mCurrentPhotoPath);
                    }
                    break;
            }
        }
        if (!photos.isEmpty()) {
            etAttachment.setText(getResources().getQuantityString(R.plurals.numberOfImages, photos.size(), photos.size()));
        }
    }

    private class ForwardRemoteListener extends BaseRemoteListener<Status> {
        private OnSentListener callback;
        private Event event;
        private String callSid;

        public ForwardRemoteListener(MainActivity activity, String callSid, Event event) {
            super(activity);
            this.callSid = callSid;
            this.event = event;
            callback = activity.getActionWithFragments();
        }

        @Override
        public void onFailure(Integer failureCode, Call<Status> call, BaseHandler<Status> handler) {
            super.onFailure(failureCode, call, handler);
            if (failureCode.equals(NETWORK_NOT_AVAILABLE.getCode())) {
                if (activity != null) {
                    Dialog dialog = Dialog.newInstance(activity.getString(R.string.action_network_not_available_title),
                            activity.getString(R.string.action_network_not_available_text));
                    dialog.show(activity.getFragmentManager(), HelpUtils.ERROR_DIALOG_TAG);
                }
                Log.e(TAG_NETWORK_NOT_AVAILABLE, NETWORK_NOT_AVAILABLE.getMessage());
            }
        }

        @Override
        public void onSuccess(Status result) {
            if (activity != null) {
                BaseActivity baseActivity = (BaseActivity) activity;
                if (result.getStatus().equals("OK")) {
                    if (baseActivity.getTransitManager() != null
                            && baseActivity.getTransitManager().getCurrentFragment().getClass().getName().equals(ForwardFragment.class.getName())) {
                        baseActivity.getTransitManager().back();
                    }
                    Toast.makeText(activity, R.string.forward_success, Toast.LENGTH_SHORT).show();
                    callback.onSent();
                }
            }
        }

//        @Override
//        public void onFailure(Status error) {
//            if (error != null && error.getError() != null) {
//                Dialog dialog = Dialog.newInstance(activity.getString(R.string.forward_conflict_title), error.getError());
//                dialog.show(activity.getFragmentManager(), HelpUtils.ERROR_DIALOG_TAG);
//            }
//        }

        @Override
        public void onFinishTask() {
            if (pbSending != null) {
                pbSending.setVisibility(View.GONE);
            }
            getHeaderController().getRightButton().setClickable(true);
        }
    }
}
