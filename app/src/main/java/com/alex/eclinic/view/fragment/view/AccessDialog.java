package com.alex.eclinic.view.fragment.view;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.alex.eclinic.R;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.fragment.CallDetailsFragment;

/**
 * 09.02.16.
 *
 * @author Alexey Vereshchaga
 */
public class AccessDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private CallDetailsFragment.Callbacks mCallbacks;

    public static AccessDialog newInstance(String title, String message) {
        AccessDialog frag = new AccessDialog();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(MESSAGE, message);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = ((MainActivity) activity).getActionWithFragments();
    }

    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString(TITLE);
        String message = getArguments().getString(MESSAGE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        builder.setPositiveButton(getResources().getString(R.string.dialog_yes), null);
        builder.setNegativeButton(getResources().getString(R.string.dialog_no), null);
        builder.setTitle(title);
        builder.setMessage(message);
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                ContextCompat.getColor(getActivity(), R.color.dialog_button));
        ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(
                ContextCompat.getColor(getActivity(), R.color.dialog_button));
    }

    //todo remove if not necessary
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case AlertDialog.BUTTON_POSITIVE:
                mCallbacks.onComplete();
                break;
        }
    }
}