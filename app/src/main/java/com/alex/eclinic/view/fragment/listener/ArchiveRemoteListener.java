package com.alex.eclinic.view.fragment.listener;

import android.app.Activity;
import android.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.alex.eclinic.R;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.fragment.Dialog;
import com.alex.eclinic.view.fragment.inbox.InboxFragment;

import retrofit2.Call;

import static com.alex.eclinic.network.ErrorCode.NETWORK_NOT_AVAILABLE;

/**
 * 26.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class ArchiveRemoteListener extends BaseRemoteListener<Status> {

    private String callSid;

    public ArchiveRemoteListener(Activity activity, String callSid) {
        super(activity);
        this.callSid = callSid;
    }

    @Override
    public void onStartTask() {
        Fragment fragment = activity.getFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null
                && fragment.getClass().getName().equals(InboxFragment.class.getName())) {
            InboxFragment inboxFragment = (InboxFragment) fragment;
            inboxFragment.setArchiving(true);
            inboxFragment.setRefreshing(true);
        }
    }

    @Override
    public void onFailure(Integer failureCode, Call<Status> call, BaseHandler<Status> handler) {
        super.onFailure(failureCode, call, handler);
        if (failureCode.equals(NETWORK_NOT_AVAILABLE.getCode())) {
            if (activity != null) {
                Dialog dialog = Dialog.newInstance(activity.getString(R.string.action_network_not_available_title),
                        activity.getString(R.string.action_network_not_available_text));
                dialog.show(activity.getFragmentManager(), "0");
            }
            Log.e(TAG_NETWORK_NOT_AVAILABLE, NETWORK_NOT_AVAILABLE.getMessage());
        }
    }

    @Override
    public void onFailure(Integer errorCode, Status error) {
        if (error != null) {
            Dialog dialog = Dialog.newInstance(activity.getString(R.string.archive_conflict_title), error.getError());
            dialog.show(activity.getFragmentManager(), HelpUtils.ERROR_DIALOG_TAG);
        }
    }

    @Override
    public void onSuccess(Status result) {
        if (activity != null) {
            Fragment fragment = activity.getFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment != null
                    && fragment.getClass().getName().equals(InboxFragment.class.getName())) {
                InboxFragment inboxFragment = (InboxFragment) fragment;
                inboxFragment.getCalls();
            }
            Toast.makeText(activity, R.string.archive_send_success, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFinishTask() {
        Fragment fragment = activity.getFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null
                && fragment.getClass().getName().equals(InboxFragment.class.getName())) {
            InboxFragment inboxFragment = (InboxFragment) fragment;
            inboxFragment.setArchiving(false);
            inboxFragment.setRefreshing(false);
        }
    }
}
