package com.alex.eclinic;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.alex.eclinic.network.Server;
import com.alex.eclinic.utils.Constant;
import com.alex.eclinic.utils.Foreground;
import com.alex.eclinic.utils.HelpUtils;
import com.danikula.videocache.FileNameGenerator;
import com.danikula.videocache.HttpProxyCacheServer;
import com.danikula.videocache.Md5FileNameGenerator;

/**
 * Class description
 * 30.09.15.
 *
 * @author Alexey Vereshchaga
 */

public class TheApplication extends Application {

    private static TheApplication sApplication;
    private HttpProxyCacheServer proxy;
    Foreground.Listener myListener = new Foreground.Listener() {
        public void onBecameForeground() {
            Intent intent = new Intent(HelpUtils.FOREGROUND_EVENT);
            LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
        }

        public void onBecameBackground() {
        }
    };

    public static TheApplication getInstance() {
        return sApplication;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        Foreground.init(this);
        Foreground.get(this).addListener(myListener);
    }

    public static HttpProxyCacheServer getProxy(Context context) {
        TheApplication app = (TheApplication) context.getApplicationContext();
        app.proxy = app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
        return app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        FileNameGenerator nameGenerator = new Md5FileNameGenerator(getExternalCacheDir());
        return new HttpProxyCacheServer(nameGenerator);
    }
}
