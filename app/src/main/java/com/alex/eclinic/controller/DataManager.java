package com.alex.eclinic.controller;

import android.app.Activity;

import com.alex.eclinic.BuildConfig;
import com.alex.eclinic.modelv3.CallType;
import com.alex.eclinic.modelv3.GrantType;
import com.alex.eclinic.modelv3.RemoteAction;
import com.alex.eclinic.modelv3.request.Credentials;
import com.alex.eclinic.modelv3.request.Provider;
import com.alex.eclinic.modelv3.request.ProviderAvailability;
import com.alex.eclinic.modelv3.request.ProviderRemote;
import com.alex.eclinic.modelv3.request.Remote;
import com.alex.eclinic.modelv3.request.Reset;
import com.alex.eclinic.modelv3.request.Token;
import com.alex.eclinic.modelv3.response.Access;
import com.alex.eclinic.modelv3.response.CallWrapper;
import com.alex.eclinic.modelv3.response.CommonInfo;
import com.alex.eclinic.modelv3.response.CountInfo;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.Providers;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.modelv3.response.UserProfile;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.ClinicApi;
import com.alex.eclinic.network.RefreshRemoteListener;
import com.alex.eclinic.network.WrapperUnauthorized;
import com.alex.eclinic.utils.SharedPreferencesHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;


/**
 * 07.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class DataManager {

    private static List<WrapperUnauthorized> callsUnauthorized = new ArrayList<>();
    public static Boolean isRefreshed = false;

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(SharedPreferencesHelper.getServer())
            .addConverterFactory(JacksonConverterFactory.create())
            .client(createNewOkHttpClient())
            .build();

    private static ClinicApi clinicApi = retrofit.create(ClinicApi.class);

    private static OkHttpClient createNewOkHttpClient() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = null;
        if (BuildConfig.DEBUG) {
            loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        }
        if (loggingInterceptor != null) {
            clientBuilder.addInterceptor(loggingInterceptor);
        }
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + SharedPreferencesHelper.getAccessToken()).build();
                return chain.proceed(request);
            }
        };
        clientBuilder.addInterceptor(interceptor);
        return clientBuilder.build();
    }

    //api v3
    public static Call<Access> login(String username, String password, BaseHandler<Access> handler) {
        handler.onStart();
        Credentials credentials = new Credentials();
        credentials.setUsername(username);
        credentials.setPassword(password);
        credentials.setGrantType(GrantType.PASSWORD);
        Call<Access> call = clinicApi.getToken(credentials);
        call.enqueue(handler);
        return call;
    }

    public static Call<com.alex.eclinic.modelv3.response.Response> resetPassword(String email, BaseHandler<com.alex.eclinic.modelv3.response.Response> handler) {
        handler.onStart();
        Reset reset = new Reset();
        reset.setEmail(email);
        Call<com.alex.eclinic.modelv3.response.Response> call = clinicApi.resetPassword(reset);
        call.enqueue(handler);
        return call;
    }

    public static Call<UserProfile> getUserProfile(BaseHandler<UserProfile> handler) {
        handler.onStart();
        Call<UserProfile> call = clinicApi.getUserProfile();
        call.enqueue(handler);
        return call;
    }

    public static Call<CommonInfo> getNewCalls(Integer providerId, BaseHandler<CommonInfo> handler) {
        return getCalls(providerId, CallType.NEW, handler);
    }

    public static Call<CommonInfo> getForwardedCalls(Integer providerId, BaseHandler<CommonInfo> handler) {
        return getCalls(providerId, CallType.FORWARDED, handler);
    }

    public static Call<CommonInfo> getCompletedCalls(Integer providerId, BaseHandler<CommonInfo> handler) {
        return getCalls(providerId, CallType.COMPLETE, handler);
    }

    public static Call<CommonInfo> getArchivedCalls(Integer providerId, BaseHandler<CommonInfo> handler) {
        return getCalls(providerId, CallType.ARCHIVE, handler);
    }

    private static Call<CommonInfo> getCalls(Integer providerId, CallType callType, BaseHandler<CommonInfo> handler) {
        handler.onStart();
        Call<CommonInfo> call = clinicApi.getCalls(providerId, callType.name().toLowerCase());
        call.enqueue(handler);
        return call;
    }

    public static Call<Status> sendToken(Token token, Callback<Status> callback) {
        Call<Status> call = clinicApi.sendToken(token);
        call.enqueue(callback);
        return call;
    }

    public static Call<Status> sendAction(String callId, Remote remote, BaseHandler<Status> handler) {
        handler.onStart();
        Call<Status> call = clinicApi.sendAction(callId, remote);
        call.enqueue(handler);
        return call;
    }

    public static Call<Status> uploadAudioFile(String callId, RequestBody requestBody, BaseHandler<Status> handler) {
        handler.onStart();
        RequestBody requestBody1 = RequestBody.create(MediaType.parse("text/plain"), RemoteAction.VOICE_MESSAGE.getAction());
        Call<Status> call = clinicApi.upload(callId, requestBody1, requestBody);
        call.enqueue(handler);
        return call;
    }

    public static Call<com.alex.eclinic.modelv3.response.Call> sendCompletedCall(String callId, BaseHandler<com.alex.eclinic.modelv3.response.Call> handler) {
        handler.onStart();
        Call<com.alex.eclinic.modelv3.response.Call> call = clinicApi.sendCompletedCall(callId);
        call.enqueue(handler);
        return call;
    }

    public static Call<com.alex.eclinic.modelv3.response.Provider> updateProfileProvider(Integer providerId, Provider provider, BaseHandler<com.alex.eclinic.modelv3.response.Provider> handler) {
        handler.onStart();
        ProviderRemote providerRemote = new ProviderRemote();
        providerRemote.setProvider(provider);
        Call<com.alex.eclinic.modelv3.response.Provider> call = clinicApi.updateProfileProvider(providerId, providerRemote);
        call.enqueue(handler);
        return call;
    }

    public static Call<ProfileProvider> updateProfileAvailability(Integer providerId, Boolean availability, BaseHandler<ProfileProvider> handler) {
        handler.onStart();
        ProviderAvailability providerAvailability = new ProviderAvailability();
        providerAvailability.setPatientAvailable(availability);
        Call<ProfileProvider> call = clinicApi.updateProfileAvailability(providerId, providerAvailability);
        call.enqueue(handler);
        return call;
    }

    public static Call<Providers> getProviders(BaseHandler<Providers> handler) {
        handler.onStart();
        Call<Providers> call = clinicApi.getProviders();
        call.enqueue(handler);
        return call;
    }

    public static Call<CountInfo> getIncompleteCalls(BaseHandler<CountInfo> handler) {
        handler.onStart();
        Call<CountInfo> call = clinicApi.getIncompleteCalls();
        call.enqueue(handler);
        return call;
    }

    public static Call<CallWrapper> getCall(String callId, BaseHandler<CallWrapper> handler) {
        handler.onStart();
        Call<CallWrapper> call = clinicApi.getCall(callId);
        call.enqueue(handler);
        return call;
    }

    public static Call<com.alex.eclinic.modelv3.response.Response> checkServer(BaseHandler<com.alex.eclinic.modelv3.response.Response> handler) {
        handler.onStart();
        Call<com.alex.eclinic.modelv3.response.Response> call = clinicApi.getSuccess();
        call.enqueue(handler);
        return call;
    }

    /**
     * If the request falls, then the user log out.
     *
     * @param refreshToken
     * @param handler
     * @return request
     */
    private static Call<Access> refreshToken(String refreshToken, BaseHandler<Access> handler) {
        handler.onStart();
        Credentials credentials = new Credentials();
        credentials.setRefreshToken(refreshToken);
        credentials.setGrantType(GrantType.REFRESH_TOKEN);
        Call<Access> call = clinicApi.getToken(credentials);
        call.enqueue(handler);
        return call;
    }

    public static void refreshToken(Activity activity) {
        if (!isRefreshed) {
            refreshToken(SharedPreferencesHelper.getRefreshToken(), new BaseHandler<>(new RefreshRemoteListener(activity), new TypeReference<Access>() {
            }));
        }
    }

    public static void repeatRequests() {
        if (!callsUnauthorized.isEmpty()) {
            Iterator<WrapperUnauthorized> iterator = callsUnauthorized.iterator();
            while (iterator.hasNext()) {
                WrapperUnauthorized wrapperUnauthorized = iterator.next();
                Call clonedCall = wrapperUnauthorized.getCall().clone();
                clonedCall.enqueue(wrapperUnauthorized.getHandler());
                iterator.remove();
            }
        }
    }

    public static void addCallUnauthorized(WrapperUnauthorized wrapperUnauthorized) {
        callsUnauthorized.add(wrapperUnauthorized);
    }

    public static List<WrapperUnauthorized> getCallsUnauthorized() {
        return callsUnauthorized;
    }
}
