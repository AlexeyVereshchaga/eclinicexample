package com.alex.eclinic.controller;

import android.view.View;

import com.alex.eclinic.R;
import com.alex.eclinic.modelv3.response.CallInfo;
import com.alex.eclinic.modelv3.response.CommonInfo;
import com.alex.eclinic.modelv3.response.CountInfo;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.Provider;
import com.alex.eclinic.modelv3.response.Providers;
import com.alex.eclinic.modelv3.response.User;
import com.alex.eclinic.modelv3.response.UserProfile;
import com.alex.eclinic.network.BaseHandler;
import com.alex.eclinic.network.BaseRemoteListener;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.adapter.ProfileProvidersAdapter;
import com.alex.eclinic.view.fragment.CallsFragment;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * 03.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class UserAccountManager {

    private MainActivity mainActivity;

    private User user;
    private CommonInfo commonInfoNew;
    private CommonInfo commonInfoArchive;
    private CommonInfo commonInfoForward;
    private List<ProfileProvider> profileProviders;
    private ProfileProvider currentProfileProvider;
    private List<Provider> providers;
    private Map<Integer, Integer> providerIdCountMap;
    private int totalIncomplete;

    public UserAccountManager(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void requestUser() {
        if (user == null && mainActivity != null) {
            DataManager.getUserProfile(new BaseHandler<>(new BaseRemoteListener<UserProfile>(mainActivity) {
                @Override
                public void onSuccess(UserProfile result) {
                    if (result != null) {
                        user = result.getUser();
                    }
                    if (user != null) {
                        profileProviders = user.getProviders();
                        ProfileProvidersAdapter profileProvidersAdapter = mainActivity.getProfileProvidersController().getProfileProvidersAdapter();
                        profileProvidersAdapter.clear();
                        profileProvidersAdapter.addAll(profileProviders);
                        updateCurrentProfileProvider();
                        if (mainActivity != null
                                && profileProviders != null
                                && profileProviders.size() > 1
                                && mainActivity.getTransitManager().getCurrentFragment().getClass().getSuperclass().equals(CallsFragment.class)) {
                            CallsFragment fragment = (CallsFragment) mainActivity.getTransitManager().getCurrentFragment();
                            mainActivity.getHeaderController().setRightButton(fragment, true, R.drawable.ic_user_practices, null);
                        }
                        setIncompleteData();
                    }
                }
            }, new TypeReference<UserProfile>() {
            }));
        }
    }

    public void updateCurrentProfileProvider() {
        if (currentProfileProvider != null) {
            Integer id = currentProfileProvider.getId();
            for (ProfileProvider provider : profileProviders) {
                if (provider.getId().equals(id)) {
                    currentProfileProvider = provider;
                    return;
                }
            }
        }
    }

    public void requestProviders() {
        if (mainActivity != null) {
            DataManager.getProviders(new BaseHandler<>(new BaseRemoteListener<Providers>(mainActivity) {
                @Override
                public void onSuccess(Providers result) {
                    providers = result.getProviders();
                }
            }, new TypeReference<Providers>() {
            }));
        }
    }

    public void requestIncompleteCalls() {
        if (mainActivity != null) {
            DataManager.getIncompleteCalls(new BaseHandler<>(new BaseRemoteListener<CountInfo>(mainActivity) {
                @Override
                public void onSuccess(CountInfo result) {
                    if (result != null) {
                        if (providerIdCountMap == null) {
                            providerIdCountMap = new HashMap<>();
                        }
                        providerIdCountMap.clear();
                        for (CallInfo callInfo : result.getCallInfoList()) {
                            providerIdCountMap.put(callInfo.getProviderId(), callInfo.getIncompleteCalls());
                        }
                    }
                    setIncompleteData();
                }
            }, new TypeReference<CountInfo>() {
            }));
        }
    }

    private void setIncompleteData() {
        if (mainActivity != null && providerIdCountMap != null) {
            totalIncomplete = 0;
            for (Integer count : providerIdCountMap.values()) {
                totalIncomplete += count;
            }
            if (totalIncomplete > 0) {
                mainActivity.getHeaderController().getCallsNumberView().setText(String.format(Locale.US, "%d", totalIncomplete));
                if (profileProviders != null
                        && profileProviders.size() > 1
                        && mainActivity.getTransitManager().getCurrentFragment().getClass().getSuperclass().equals(CallsFragment.class)) {
                    mainActivity.getHeaderController().getCallsNumberView().setVisibility(View.VISIBLE);
                }
            } else {
                mainActivity.getHeaderController().getCallsNumberView().setVisibility(View.GONE);
            }
            mainActivity.getProfileProvidersController().getProfileProvidersAdapter().clear();
            if (profileProviders != null && !profileProviders.isEmpty() && profileProviders.size() > 1) {
                for (ProfileProvider profileProvider : profileProviders) {
                    profileProvider.setIncompleteCalls(providerIdCountMap.get(profileProvider.getId()));
                }
                mainActivity.getProfileProvidersController().getProfileProvidersAdapter().addAll(profileProviders);
            }
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CommonInfo getCommonInfoNew() {
        return commonInfoNew;
    }

    public void setCommonInfoNew(CommonInfo commonInfoNew) {
        this.commonInfoNew = commonInfoNew;
    }

    public CommonInfo getCommonInfoArchive() {
        return commonInfoArchive;
    }

    public void setCommonInfoArchive(CommonInfo commonInfoArchive) {
        this.commonInfoArchive = commonInfoArchive;
    }

    public CommonInfo getCommonInfoForward() {
        return commonInfoForward;
    }

    public void setCommonInfoForward(CommonInfo commonInfoForward) {
        this.commonInfoForward = commonInfoForward;
    }

    public List<ProfileProvider> getProfileProviders() {
        return profileProviders;
    }

    public void setProfileProviders(List<ProfileProvider> profileProviders) {
        this.profileProviders = profileProviders;
    }


    public ProfileProvider getCurrentProfileProvider() {
        if (currentProfileProvider == null && profileProviders != null && !profileProviders.isEmpty()) {
            currentProfileProvider = profileProviders.get(0);
            if (mainActivity != null && currentProfileProvider.getPartner() != null && profileProviders.size() > 1) {
                setPartnerName(currentProfileProvider.getPartner().getName());
            }
        }
        return currentProfileProvider;
    }

    private void setPartnerName(String name) {
        mainActivity.getHeaderController().getPartnerNameView().setVisibility(View.VISIBLE);
        mainActivity.getHeaderController().getPartnerNameView().setText(name);
    }

    public void setCurrentProfileProvider(ProfileProvider currentProfileProvider) {
        this.currentProfileProvider = currentProfileProvider;
        setPartnerName(currentProfileProvider.getPartner().getName());
    }

    public List<Provider> getProviders() {
        return providers;
    }

    public void clearCommonInfo() {
        if (commonInfoNew != null) {
            commonInfoNew.getCalls().clear();
        }
        if (commonInfoArchive != null) {
            commonInfoArchive.getCalls().clear();
        }
        if (commonInfoForward != null) {
            commonInfoForward.getCalls().clear();
        }
    }

    public int getTotalIncomplete() {
        return totalIncomplete;
    }
}
