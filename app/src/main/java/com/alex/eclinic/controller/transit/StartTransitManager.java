package com.alex.eclinic.controller.transit;

import android.app.Activity;

import com.fsm.transit.core.AbstractTransitManager;

/**
 * This class represent all transition between fragments in StartActivity
 * 01.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class StartTransitManager extends AbstractTransitManager<FragmentAction> {
    public StartTransitManager(Activity activity) {
        super(activity);
    }
}
