package com.alex.eclinic.controller.transit;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import com.alex.eclinic.R;
import com.alex.eclinic.view.fragment.AvailabilityFragment;
import com.alex.eclinic.view.fragment.CallsFragment;
import com.alex.eclinic.view.fragment.ForwardFragment;
import com.alex.eclinic.view.fragment.NoteFragment;
import com.alex.eclinic.view.fragment.ProfileFragment;
import com.alex.eclinic.view.fragment.RecipientFragment;
import com.alex.eclinic.view.fragment.archive.ArchivedCallDetailsFragment;
import com.alex.eclinic.view.fragment.archive.ArchivedCallsFragment;
import com.alex.eclinic.view.fragment.forward.ForwardedCallDetailsFragment;
import com.alex.eclinic.view.fragment.forward.ForwardedCallsFragment;
import com.alex.eclinic.view.fragment.inbox.InboxCallDetailsFragment;
import com.alex.eclinic.view.fragment.inbox.InboxFragment;
import com.alex.eclinic.view.fragment.pin.ChangePinFragment;
import com.alex.eclinic.view.fragment.pin.ConfirmChangePinFragment;
import com.alex.eclinic.view.fragment.pin.ConfirmPinFragment;
import com.alex.eclinic.view.fragment.pin.CreatePinFragment;
import com.fsm.transit.bridge.FragmentAnimation;
import com.fsm.transit.core.AbstractTransitManager;
import com.fsm.transit.core.TransitData;
import com.fsm.transit.core.TransitResultData;

/**
 * Class description
 *
 * @author Alexey Vereshchaga
 */
public class MainTransitManager extends AbstractTransitManager<FragmentAction> {

//    public static final String ROOT_CALLS_FRAGMENT_TAG = CallsFragment.class.getName();

    public MainTransitManager(Activity activity) {
        super(activity);
    }

    @Override
    public <T extends Fragment> T switchBranch(Class<T> fragmentClass) {
        FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(0);
            fragmentManager.popBackStack(backStackEntry.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        FragmentTransaction ft = fragmentManager.beginTransaction();
        String fragmentTag = fragmentClass.getName();
        if (fragmentTag.equals(InboxFragment.class.getName())
                || fragmentTag.equals(ArchivedCallsFragment.class.getName())
                || fragmentTag.equals(ForwardedCallsFragment.class.getName())) {
            fragmentTag = CallsFragment.class.getName();
        }
        T fragment = (T) Fragment.instantiate(activity.getBaseContext(), fragmentClass.getName());
        ft.replace(currentContainer, fragment, fragmentTag);
        ft.commit();
        return fragment;
    }

    {
        FragmentAnimation animation = new FragmentAnimation(R.anim.fragm_out_to_right, R.anim.fragm_in_from_right);
        transitionsMap.put(new TransitData<>(CreatePinFragment.class, FragmentAction.CONFIRM_ACTION), new TransitResultData<FragmentAction>(ConfirmPinFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(ProfileFragment.class, FragmentAction.AVAILABILITY_ACTION), new TransitResultData<FragmentAction>(AvailabilityFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(InboxFragment.class, FragmentAction.CALL_DETAILS_ACTION), new TransitResultData<FragmentAction>(InboxCallDetailsFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(ForwardedCallsFragment.class, FragmentAction.CALL_DETAILS_ACTION), new TransitResultData<FragmentAction>(ForwardedCallDetailsFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(ArchivedCallsFragment.class, FragmentAction.CALL_DETAILS_ACTION), new TransitResultData<FragmentAction>(ArchivedCallDetailsFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(ArchivedCallsFragment.class, FragmentAction.ARCHIVED_CALL_DETAILS_ACTION), new TransitResultData<FragmentAction>(ArchivedCallDetailsFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(InboxCallDetailsFragment.class, FragmentAction.NOTE_ACTION), new TransitResultData<FragmentAction>(NoteFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(ArchivedCallDetailsFragment.class, FragmentAction.NOTE_ACTION), new TransitResultData<FragmentAction>(NoteFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(ForwardedCallDetailsFragment.class, FragmentAction.NOTE_ACTION), new TransitResultData<FragmentAction>(NoteFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(ProfileFragment.class, FragmentAction.CHANGE_PIN_ACTION), new TransitResultData<FragmentAction>(ChangePinFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(ChangePinFragment.class, FragmentAction.CONFIRM_CHANGE_PIN_ACTION), new TransitResultData<FragmentAction>(ConfirmChangePinFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(ArchivedCallDetailsFragment.class, FragmentAction.FORWARD_ACTION), new TransitResultData<FragmentAction>(ForwardFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(InboxCallDetailsFragment.class, FragmentAction.FORWARD_ACTION), new TransitResultData<FragmentAction>(ForwardFragment.class, animation, true));
        transitionsMap.put(new TransitData<>(ForwardedCallDetailsFragment.class, FragmentAction.FORWARD_ACTION), new TransitResultData<FragmentAction>(ForwardFragment.class, animation, true));
//        transitionsMap.put(new TransitData<>(ForwardFragment.class, FragmentAction.RECIPIENT_ACTION), new TransitResultData<FragmentAction>(RecipientFragment.class, animation, true));
    }
}
