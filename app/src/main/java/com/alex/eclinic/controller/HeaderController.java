package com.alex.eclinic.controller;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alex.eclinic.R;

/**
 * Class description
 * 30.09.15.
 *
 * @author Alexey Vereshchaga
 */
public class HeaderController extends AbstractHeaderController {
    public static final int RIGHT_BTN_ID = R.id.btn_right;
    public static final int LEFT_BTN_ID = R.id.btn_left;
    public static final int TITLE_ID = R.id.title;
    public static final int CALLS_NUMBER_ID = R.id.tv_calls_number;
    public static final int PARTNER_NAME = R.id.tv_partner_name;


    private Button leftButton;
    private Button rightButton;
    private TextView titleView;
    private TextView callsNumberView;
    private TextView partnerNameView;
    private String title;
    private Context context;

    public HeaderController(Context context) {
        this.context = context;
    }

    @Override
    public void init() {
        leftButton = (Button) viewHeader.findViewById(LEFT_BTN_ID);
        rightButton = (Button) viewHeader.findViewById(RIGHT_BTN_ID);
        titleView = (TextView) viewHeader.findViewById(TITLE_ID);
        callsNumberView = (TextView) viewHeader.findViewById(CALLS_NUMBER_ID);
        partnerNameView = (TextView) viewHeader.findViewById(PARTNER_NAME);
    }

    public void setRightButton(View.OnClickListener onClickListener, boolean show, int img, String text) {
        if (getRightButton() != null) {
            getRightButton().setOnClickListener(onClickListener);
            getRightButton().setVisibility(show ? View.VISIBLE : View.INVISIBLE);
            if (img != 0) {
                getRightButton().setBackgroundDrawable(context.getResources().getDrawable(img));
            }
            if (text != null) {
                getRightButton().setText(text);
            }
        }
    }

    public void setRightButton(View.OnClickListener onClickListener, boolean show, String text) {
        setRightButton(onClickListener, show, 0, text);
    }

    public void setLeftButton(View.OnClickListener onClickListener, boolean show, int img, String text) {
        if (getLeftButton() != null) {
            getLeftButton().setOnClickListener(onClickListener);
            getLeftButton().setVisibility(show ? View.VISIBLE : View.INVISIBLE);
            if (img != 0) {
                getLeftButton().setBackgroundDrawable(context.getResources().getDrawable(img));
            }
            if (text != null) {
                getLeftButton().setText(text);
            }
        }
    }

    public void setLeftButton(View.OnClickListener onClickListener, boolean show, String text) {
        setLeftButton(onClickListener, show, 0, text);
    }

    public void setTitle(String title) {
        if (title != null) {
            this.title = title;
            titleView.setText(title);
        }
    }

    public RelativeLayout getGeneralHeaderView() {
        return (RelativeLayout) viewHeader;
    }

    public Button getLeftButton() {
        return leftButton;
    }

    public Button getRightButton() {
        return rightButton;
    }

    public TextView getTitleView() {
        return titleView;
    }

    public String getTitle() {
        return title;
    }

    public TextView getCallsNumberView() {
        return callsNumberView;
    }

    public TextView getPartnerNameView() {
        return partnerNameView;
    }
}
