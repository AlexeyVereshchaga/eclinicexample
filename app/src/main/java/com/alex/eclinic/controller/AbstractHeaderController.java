package com.alex.eclinic.controller;

import android.view.View;

import com.fsm.transit.core.ITransitManager;

/**
 * Class description
 * 30.09.15.
 *
 * @author Alexey Vereshchaga
 */
public abstract class AbstractHeaderController {

    protected ITransitManager transitManager;
    protected View viewHeader;

    public void setTransitManager(ITransitManager transitManager) {
        this.transitManager = transitManager;
    }

    public void setViewHeader(View viewHeader) {
        this.viewHeader = viewHeader;
    }

    public abstract void init();

    public void showHeader(boolean flag) {
        viewHeader.setVisibility(flag ? View.VISIBLE : View.GONE);
    }
}
