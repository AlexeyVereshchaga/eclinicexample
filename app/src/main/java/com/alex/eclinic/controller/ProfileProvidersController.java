package com.alex.eclinic.controller;

import android.content.Context;
import android.widget.AdapterView;
import android.widget.ListView;

import com.alex.eclinic.R;
import com.alex.eclinic.view.adapter.ProfileProvidersAdapter;

/**
 * 05.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class ProfileProvidersController {

    public static final int LIST_ID = R.id.header_list_view;

    private ProfileProvidersAdapter profileProvidersAdapter;
    private ListView listView;
    private Context context;

    public ProfileProvidersController(Context context) {
        this.context = context;
    }

    public void setListView(ListView listView) {
        this.listView = listView;
    }

    public void init() {
        profileProvidersAdapter = new ProfileProvidersAdapter(context, R.layout.item_partner);
        listView.setAdapter(profileProvidersAdapter);
    }

    public ProfileProvidersAdapter getProfileProvidersAdapter() {
        return profileProvidersAdapter;
    }

    public void setProfileProvidersAdapter(ProfileProvidersAdapter profileProvidersAdapter) {
        this.profileProvidersAdapter = profileProvidersAdapter;
    }

    public ListView getListView() {
        return listView;
    }

    public void setOnItemClickListener (AdapterView.OnItemClickListener onItemClickListener){
        listView.setOnItemClickListener(onItemClickListener);
    }
}
