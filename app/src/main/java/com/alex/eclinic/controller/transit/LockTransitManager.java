package com.alex.eclinic.controller.transit;

import android.app.Activity;

import com.fsm.transit.core.AbstractTransitManager;

/**
 * 09.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class LockTransitManager extends AbstractTransitManager<FragmentAction> {
    public LockTransitManager(Activity activity) {
        super(activity);
    }
}
