package com.alex.eclinic.controller;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.view.activity.MainActivity;
import com.alex.eclinic.view.fragment.CallsFragment;
import com.alex.eclinic.view.fragment.ProfileFragment;
import com.alex.eclinic.view.fragment.archive.ArchivedCallsFragment;
import com.alex.eclinic.view.fragment.forward.ForwardedCallsFragment;
import com.alex.eclinic.view.fragment.inbox.InboxFragment;
import com.navdrawer.SimpleSideDrawer;

/**
 * 06.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class SlidingMenuController implements View.OnClickListener {

    private MainActivity activity;
    private SimpleSideDrawer slidingMenu;
    private View mainMenuView;
    private TextView tvName;


    public SlidingMenuController(MainActivity activity) {
        this.activity = activity;
        initSlidingMenu();
    }

    public void initSlidingMenu() {
        slidingMenu = new SimpleSideDrawer(activity);
        slidingMenu.setLeftBehindContentView(R.layout.menu_left);
        mainMenuView = slidingMenu.getLeftBehindView();
        tvName = (TextView) mainMenuView.findViewById(R.id.tv_name);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mainMenuView.getLayoutParams();
        params.width = HelpUtils.getScreenWidth(activity)
                - activity.getResources().getDimensionPixelSize(R.dimen.header_imageBtn_dimension)
                - activity.getResources().getDimensionPixelSize(R.dimen.header_button_margin);
        mainMenuView.setLayoutParams(params);
        mainMenuView.findViewById(R.id.tv_profile).setOnClickListener(this);
        mainMenuView.findViewById(R.id.ll_inbox).setOnClickListener(this);
        mainMenuView.findViewById(R.id.ll_archived_calls).setOnClickListener(this);
        mainMenuView.findViewById(R.id.ll_forvarded_calls).setOnClickListener(this);
    }

    public SimpleSideDrawer getSideDrawer() {
        return slidingMenu;
    }

    @Override
    public void onClick(View v) {
        CallsFragment callsFragment = null;
        switch (v.getId()) {
            case R.id.tv_profile:
                slidingMenu.toggleLeftDrawer();
                activity.getTransitManager().switchBranch(ProfileFragment.class);
                mainMenuView.findViewById(R.id.v_inbox).setVisibility(View.INVISIBLE);
                mainMenuView.findViewById(R.id.v_archived).setVisibility(View.INVISIBLE);
                mainMenuView.findViewById(R.id.v_forwarded).setVisibility(View.INVISIBLE);
                break;
            case R.id.ll_inbox:
                slidingMenu.toggleLeftDrawer();
                callsFragment = activity.getTransitManager().switchBranch(InboxFragment.class);
                mainMenuView.findViewById(R.id.v_inbox).setVisibility(View.VISIBLE);
                mainMenuView.findViewById(R.id.v_archived).setVisibility(View.INVISIBLE);
                mainMenuView.findViewById(R.id.v_forwarded).setVisibility(View.INVISIBLE);
                break;
            case R.id.ll_archived_calls:
                slidingMenu.toggleLeftDrawer();
                callsFragment = activity.getTransitManager().switchBranch(ArchivedCallsFragment.class);
                mainMenuView.findViewById(R.id.v_archived).setVisibility(View.VISIBLE);
                mainMenuView.findViewById(R.id.v_inbox).setVisibility(View.INVISIBLE);
                mainMenuView.findViewById(R.id.v_forwarded).setVisibility(View.INVISIBLE);
                break;
            case R.id.ll_forvarded_calls:
                slidingMenu.toggleLeftDrawer();
                callsFragment = activity.getTransitManager().switchBranch(ForwardedCallsFragment.class);
                mainMenuView.findViewById(R.id.v_forwarded).setVisibility(View.VISIBLE);
                mainMenuView.findViewById(R.id.v_archived).setVisibility(View.INVISIBLE);
                mainMenuView.findViewById(R.id.v_inbox).setVisibility(View.INVISIBLE);
                break;
        }
        if (callsFragment != null) {
            callsFragment.setNeedUpdate(true);
        }
    }

    public void setName(String name) {
        tvName.setText(name);
    }
}
