package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Partner extends BaseModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("twilio_phone_number")
    private String twilioPhoneNumber;
    @JsonProperty("office_phone_number")
    private String officePhoneNumber;
    @JsonProperty("office_fax_number")
    private String officeFaxNumber;
    @JsonProperty("web_address")
    private String webAddress;
    @JsonProperty("address_1")
    private String address1;
    @JsonProperty("address_2")
    private String address2;
    @JsonProperty("city")
    private String city;
    @JsonProperty("state_province")
    private String stateProvince;
    @JsonProperty("postal_code")
    private String postalCode;
    @JsonProperty("country")
    private String country;
    @JsonProperty("fallback_email")
    private String fallbackEmail;
    @JsonProperty("fallback_phone_number")
    private String fallbackPhoneNumber;
    @JsonProperty("timezone")
    private String timezone;
    @JsonProperty("linked_partners")
    private List<Object> linkedPartners = new ArrayList<>();

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The twilioPhoneNumber
     */
    @JsonProperty("twilio_phone_number")
    public String getTwilioPhoneNumber() {
        return twilioPhoneNumber;
    }

    /**
     * @param twilioPhoneNumber The twilio_phone_number
     */
    @JsonProperty("twilio_phone_number")
    public void setTwilioPhoneNumber(String twilioPhoneNumber) {
        this.twilioPhoneNumber = twilioPhoneNumber;
    }

    /**
     * @return The officePhoneNumber
     */
    @JsonProperty("office_phone_number")
    public String getOfficePhoneNumber() {
        return officePhoneNumber;
    }

    /**
     * @param officePhoneNumber The office_phone_number
     */
    @JsonProperty("office_phone_number")
    public void setOfficePhoneNumber(String officePhoneNumber) {
        this.officePhoneNumber = officePhoneNumber;
    }

    /**
     * @return The officeFaxNumber
     */
    @JsonProperty("office_fax_number")
    public String getOfficeFaxNumber() {
        return officeFaxNumber;
    }

    /**
     * @param officeFaxNumber The office_fax_number
     */
    @JsonProperty("office_fax_number")
    public void setOfficeFaxNumber(String officeFaxNumber) {
        this.officeFaxNumber = officeFaxNumber;
    }

    /**
     * @return The webAddress
     */
    @JsonProperty("web_address")
    public String getWebAddress() {
        return webAddress;
    }

    /**
     * @param webAddress The web_address
     */
    @JsonProperty("web_address")
    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    /**
     * @return The address1
     */
    @JsonProperty("address_1")
    public String getAddress1() {
        return address1;
    }

    /**
     * @param address1 The address_1
     */
    @JsonProperty("address_1")
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * @return The address2
     */
    @JsonProperty("address_2")
    public String getAddress2() {
        return address2;
    }

    /**
     * @param address2 The address_2
     */
    @JsonProperty("address_2")
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * @return The city
     */
    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The stateProvince
     */
    @JsonProperty("state_province")
    public String getStateProvince() {
        return stateProvince;
    }

    /**
     * @param stateProvince The state_province
     */
    @JsonProperty("state_province")
    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    /**
     * @return The postalCode
     */
    @JsonProperty("postal_code")
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode The postal_code
     */
    @JsonProperty("postal_code")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return The country
     */
    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return The fallbackEmail
     */
    @JsonProperty("fallback_email")
    public String getFallbackEmail() {
        return fallbackEmail;
    }

    /**
     * @param fallbackEmail The fallback_email
     */
    @JsonProperty("fallback_email")
    public void setFallbackEmail(String fallbackEmail) {
        this.fallbackEmail = fallbackEmail;
    }

    /**
     * @return The fallbackPhoneNumber
     */
    @JsonProperty("fallback_phone_number")
    public String getFallbackPhoneNumber() {
        return fallbackPhoneNumber;
    }

    /**
     * @param fallbackPhoneNumber The fallback_phone_number
     */
    @JsonProperty("fallback_phone_number")
    public void setFallbackPhoneNumber(String fallbackPhoneNumber) {
        this.fallbackPhoneNumber = fallbackPhoneNumber;
    }

    /**
     * @return The timezone
     */
    @JsonProperty("timezone")
    public String getTimezone() {
        return timezone;
    }

    /**
     * @param timezone The timezone
     */
    @JsonProperty("timezone")
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    /**
     * @return The linkedPartners
     */
    @JsonProperty("linked_partners")
    public List<Object> getLinkedPartners() {
        return linkedPartners;
    }

    /**
     * @param linkedPartners The linked_partners
     */
    @JsonProperty("linked_partners")
    public void setLinkedPartners(List<Object> linkedPartners) {
        this.linkedPartners = linkedPartners;
    }

}
