package com.alex.eclinic.modelv3.response;

import android.support.annotation.NonNull;

import com.alex.eclinic.modelv3.BaseModel;
import com.alex.eclinic.utils.HelpUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Call extends BaseModel implements Comparable<Call> {

    @JsonProperty("sid")
    private String sid;
    @JsonProperty("is_urgent")
    private Boolean isUrgent;
    @JsonProperty("patient_provider_id")
    private Integer patientProviderId;
    @JsonProperty("completed_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = HelpUtils.CALL_DATE_FORMAT)
    private Date completedAt;
    @JsonProperty("transcribed_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = HelpUtils.CALL_DATE_FORMAT)
    private Date transcribedAt;
    @JsonProperty("archived_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = HelpUtils.CALL_DATE_FORMAT)
    private Date archivedAt;
    @JsonProperty("callback_number")
    private String callbackNumber;
    @JsonProperty("from")
    private String from;
    @JsonProperty("caller_id_name")
    private String callerIdName;
    @JsonProperty("from_city")
    private String fromCity;
    @JsonProperty("from_state")
    private String fromState;
    @JsonProperty("from_zip")
    private String fromZip;
    @JsonProperty("from_country")
    private String fromCountry;
    @JsonProperty("to")
    private String to;
    @JsonProperty("to_city")
    private String toCity;
    @JsonProperty("to_state")
    private String toState;
    @JsonProperty("to_zip")
    private String toZip;
    @JsonProperty("to_country")
    private String toCountry;
    @JsonProperty("language")
    private String language;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("completed_by_provider_id")
    private Integer completedByProviderId;
    @JsonProperty("completion_response_id")
    private Integer completionResponseId;
    @JsonProperty("completion_message")
    private String completionMessage;
    @JsonProperty("completed_by_user_id")
    private Integer completedByUserId;
    @JsonProperty("patient_message")
    private String patientMessage;
    @JsonProperty("patient_name")
    private String patientName;
    @JsonProperty("patient_dob")
    private String patientDob;
    @JsonProperty("call_path")
    private String callPath;
    @JsonProperty("next_call_leg")
    private Object nextCallLeg;
    @JsonProperty("provider_options")
    private Object providerOptions;
    @JsonProperty("completed_by")
    private String completedBy;
    @JsonProperty("completed_action")
    private String completedAction;
    @JsonProperty("forwards")
    private List<Event> forwards;
    @JsonProperty("responses")
    private List<Event> responses = new ArrayList<>();
    @JsonProperty("recordings")
    private List<Record> records = new ArrayList<>();
    @JsonProperty("patient_provider")
    private PatientProvider patientProvider;
    @JsonProperty("partner")
    private Partner partner;
    @JsonProperty("call_events")
    private List<CallEvent> callEvents = new ArrayList<>();
    @JsonProperty("completion_response")
    private Event completionEvent;
    @JsonProperty("completed_by_user")
    private CallUser completedByUser;

    /**
     * @return The sid
     */
    @JsonProperty("sid")
    public String getSid() {
        return sid;
    }

    /**
     * @param sid The sid
     */
    @JsonProperty("sid")
    public void setSid(String sid) {
        this.sid = sid;
    }

    /**
     * @return The isUrgent
     */
    @JsonProperty("is_urgent")
    public Boolean getIsUrgent() {
        return isUrgent;
    }

    /**
     * @param isUrgent The is_urgent
     */
    @JsonProperty("is_urgent")
    public void setIsUrgent(Boolean isUrgent) {
        this.isUrgent = isUrgent;
    }

    /**
     * @return The patientProviderId
     */
    @JsonProperty("patient_provider_id")
    public Integer getPatientProviderId() {
        return patientProviderId;
    }

    /**
     * @param patientProviderId The patient_provider_id
     */
    @JsonProperty("patient_provider_id")
    public void setPatientProviderId(Integer patientProviderId) {
        this.patientProviderId = patientProviderId;
    }

    /**
     * @return The completedAt
     */
    @JsonProperty("completed_at")
    public Date getCompletedAt() {
        return completedAt;
    }

    /**
     * @param completedAt The completed_at
     */
    @JsonProperty("completed_at")
    public void setCompletedAt(Date completedAt) {
        this.completedAt = completedAt;
    }

    /**
     * @return The transcribedAt
     */
    @JsonProperty("transcribed_at")
    public Date getTranscribedAt() {
        return transcribedAt;
    }

    /**
     * @param transcribedAt The transcribed_at
     */
    @JsonProperty("transcribed_at")
    public void setTranscribedAt(Date transcribedAt) {
        this.transcribedAt = transcribedAt;
    }

    /**
     * @return The archivedAt
     */
    @JsonProperty("archived_at")
    public Date getArchivedAt() {
        return archivedAt;
    }

    /**
     * @param archivedAt The archived_at
     */
    @JsonProperty("archived_at")
    public void setArchivedAt(Date archivedAt) {
        this.archivedAt = archivedAt;
    }

    /**
     * @return The callbackNumber
     */
    @JsonProperty("callback_number")
    public String getCallbackNumber() {
        return callbackNumber;
    }

    /**
     * @param callbackNumber The callback_number
     */
    @JsonProperty("callback_number")
    public void setCallbackNumber(String callbackNumber) {
        this.callbackNumber = callbackNumber;
    }

    /**
     * @return The from
     */
    @JsonProperty("from")
    public String getFrom() {
        return from;
    }

    /**
     * @param from The from
     */
    @JsonProperty("from")
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return The callerIdName
     */
    @JsonProperty("caller_id_name")
    public String getCallerIdName() {
        return callerIdName;
    }

    /**
     * @param callerIdName The caller_id_name
     */
    @JsonProperty("caller_id_name")
    public void setCallerIdName(String callerIdName) {
        this.callerIdName = callerIdName;
    }

    /**
     * @return The fromCity
     */
    @JsonProperty("from_city")
    public String getFromCity() {
        return fromCity;
    }

    /**
     * @param fromCity The from_city
     */
    @JsonProperty("from_city")
    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    /**
     * @return The fromState
     */
    @JsonProperty("from_state")
    public String getFromState() {
        return fromState;
    }

    /**
     * @param fromState The from_state
     */
    @JsonProperty("from_state")
    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    /**
     * @return The fromZip
     */
    @JsonProperty("from_zip")
    public String getFromZip() {
        return fromZip;
    }

    /**
     * @param fromZip The from_zip
     */
    @JsonProperty("from_zip")
    public void setFromZip(String fromZip) {
        this.fromZip = fromZip;
    }

    /**
     * @return The fromCountry
     */
    @JsonProperty("from_country")
    public String getFromCountry() {
        return fromCountry;
    }

    /**
     * @param fromCountry The from_country
     */
    @JsonProperty("from_country")
    public void setFromCountry(String fromCountry) {
        this.fromCountry = fromCountry;
    }

    /**
     * @return The to
     */
    @JsonProperty("to")
    public String getTo() {
        return to;
    }

    /**
     * @param to The to
     */
    @JsonProperty("to")
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return The toCity
     */
    @JsonProperty("to_city")
    public String getToCity() {
        return toCity;
    }

    /**
     * @param toCity The to_city
     */
    @JsonProperty("to_city")
    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    /**
     * @return The toState
     */
    @JsonProperty("to_state")
    public String getToState() {
        return toState;
    }

    /**
     * @param toState The to_state
     */
    @JsonProperty("to_state")
    public void setToState(String toState) {
        this.toState = toState;
    }

    /**
     * @return The toZip
     */
    @JsonProperty("to_zip")
    public String getToZip() {
        return toZip;
    }

    /**
     * @param toZip The to_zip
     */
    @JsonProperty("to_zip")
    public void setToZip(String toZip) {
        this.toZip = toZip;
    }

    /**
     * @return The toCountry
     */
    @JsonProperty("to_country")
    public String getToCountry() {
        return toCountry;
    }

    /**
     * @param toCountry The to_country
     */
    @JsonProperty("to_country")
    public void setToCountry(String toCountry) {
        this.toCountry = toCountry;
    }

    /**
     * @return The language
     */
    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    /**
     * @param language The language
     */
    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return The createdAt
     */
    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The completedByProviderId
     */
    @JsonProperty("completed_by_provider_id")
    public Integer getCompletedByProviderId() {
        return completedByProviderId;
    }

    /**
     * @param completedByProviderId The completed_by_provider_id
     */
    @JsonProperty("completed_by_provider_id")
    public void setCompletedByProviderId(Integer completedByProviderId) {
        this.completedByProviderId = completedByProviderId;
    }

    /**
     * @return The completionResponseId
     */
    @JsonProperty("completion_response_id")
    public Integer getCompletionResponseId() {
        return completionResponseId;
    }

    /**
     * @param completionResponseId The completion_response_id
     */
    @JsonProperty("completion_response_id")
    public void setCompletionResponseId(Integer completionResponseId) {
        this.completionResponseId = completionResponseId;
    }

    /**
     * @return The completionMessage
     */
    @JsonProperty("completion_message")
    public String getCompletionMessage() {
        return completionMessage;
    }

    /**
     * @param completionMessage The completion_message
     */
    @JsonProperty("completion_message")
    public void setCompletionMessage(String completionMessage) {
        this.completionMessage = completionMessage;
    }

    /**
     * @return The completedByUserId
     */
    @JsonProperty("completed_by_user_id")
    public Integer getCompletedByUserId() {
        return completedByUserId;
    }

    /**
     * @param completedByUserId The completed_by_user_id
     */
    @JsonProperty("completed_by_user_id")
    public void setCompletedByUserId(Integer completedByUserId) {
        this.completedByUserId = completedByUserId;
    }

    /**
     * @return The patientMessage
     */
    @JsonProperty("patient_message")
    public String getPatientMessage() {
        return patientMessage;
    }

    /**
     * @param patientMessage The patient_message
     */
    @JsonProperty("patient_message")
    public void setPatientMessage(String patientMessage) {
        this.patientMessage = patientMessage;
    }

    /**
     * @return The patientName
     */
    @JsonProperty("patient_name")
    public String getPatientName() {
        return patientName;
    }

    /**
     * @param patientName The patient_name
     */
    @JsonProperty("patient_name")
    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    /**
     * @return The patientDob
     */
    @JsonProperty("patient_dob")
    public String getPatientDob() {
        return patientDob;
    }

    /**
     * @param patientDob The patient_dob
     */
    @JsonProperty("patient_dob")
    public void setPatientDob(String patientDob) {
        this.patientDob = patientDob;
    }

    /**
     * @return The callPath
     */
    @JsonProperty("call_path")
    public String getCallPath() {
        return callPath;
    }

    /**
     * @param callPath The call_path
     */
    @JsonProperty("call_path")
    public void setCallPath(String callPath) {
        this.callPath = callPath;
    }

    /**
     * @return The nextCallLeg
     */
    @JsonProperty("next_call_leg")
    public Object getNextCallLeg() {
        return nextCallLeg;
    }

    /**
     * @param nextCallLeg The next_call_leg
     */
    @JsonProperty("next_call_leg")
    public void setNextCallLeg(Object nextCallLeg) {
        this.nextCallLeg = nextCallLeg;
    }

    /**
     * @return The providerOptions
     */
    @JsonProperty("provider_options")
    public Object getProviderOptions() {
        return providerOptions;
    }

    /**
     * @param providerOptions The provider_options
     */
    @JsonProperty("provider_options")
    public void setProviderOptions(Object providerOptions) {
        this.providerOptions = providerOptions;
    }

    /**
     * @return The completedBy
     */
    @JsonProperty("completed_by")
    public String getCompletedBy() {
        return completedBy;
    }

    /**
     * @param completedBy The completed_by
     */
    @JsonProperty("completed_by")
    public void setCompletedBy(String completedBy) {
        this.completedBy = completedBy;
    }

    /**
     * @return The completedAction
     */
    @JsonProperty("completed_action")
    public String getCompletedAction() {
        return completedAction;
    }

    /**
     * @param completedAction The completed_action
     */
    @JsonProperty("completed_action")
    public void setCompletedAction(String completedAction) {
        this.completedAction = completedAction;
    }

    public List<Event> getForwards() {
        return forwards;
    }

    public void setForwards(List<Event> forwards) {
        this.forwards = forwards;
    }

    /**
     * @return The responses
     */
    @JsonProperty("responses")
    public List<Event> getResponses() {
        return responses;
    }

    /**
     * @param responses The responses
     */
    @JsonProperty("responses")
    public void setResponses(List<Event> responses) {
        this.responses = responses;
    }

    /**
     * @return The recordings
     */
    @JsonProperty("recordings")
    public List<Record> getRecords() {
        return records;
    }

    /**
     * @param records The recordings
     */
    @JsonProperty("recordings")
    public void setRecords(List<Record> records) {
        this.records = records;
    }

    /**
     * @return The patientProvider
     */
    @JsonProperty("patient_provider")
    public PatientProvider getPatientProvider() {
        return patientProvider;
    }

    /**
     * @param patientProvider The patient_provider
     */
    @JsonProperty("patient_provider")
    public void setPatientProvider(PatientProvider patientProvider) {
        this.patientProvider = patientProvider;
    }

    /**
     * @return The partner
     */
    @JsonProperty("partner")
    public Partner getPartner() {
        return partner;
    }

    /**
     * @param partner The partner
     */
    @JsonProperty("partner")
    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public List<CallEvent> getCallEvents() {
        if (!callEvents.isEmpty()) {
            Iterator<CallEvent> iterator = callEvents.iterator();
            while (iterator.hasNext()) {
                CallEvent callEvent = iterator.next();
                if (EventTypeV3.CREATE.name().equalsIgnoreCase(callEvent.getType())
                        || EventTypeV3.NOTIFICATION.name().equalsIgnoreCase(callEvent.getType())) {
                    iterator.remove();
                }
            }
        }
        Collections.sort(callEvents);
        return callEvents;
    }

    public void setCallEvents(List<CallEvent> callEvents) {
        this.callEvents = callEvents;
    }

    /**
     * @return The completionResponse
     */
    @JsonProperty("completion_response")
    public Event getCompletionEvent() {
        return completionEvent;
    }

    /**
     * @param completionEvent The completion_response
     */
    @JsonProperty("completion_response")
    public void setCompletionEvent(Event completionEvent) {
        this.completionEvent = completionEvent;
    }

    /**
     * @return The completedByUser
     */
    @JsonProperty("completed_by_user")
    public CallUser getCompletedByUser() {
        return completedByUser;
    }

    /**
     * @param completedByUser The completed_by_user
     */
    @JsonProperty("completed_by_user")
    public void setCompletedByUser(CallUser completedByUser) {
        this.completedByUser = completedByUser;
    }

    @Override
    public int compareTo(@NonNull Call another) {
        if (getTranscribedAt() == null || another.getTranscribedAt() == null)
            return 0;
        return getTranscribedAt().compareTo(another.getTranscribedAt());
    }
}
