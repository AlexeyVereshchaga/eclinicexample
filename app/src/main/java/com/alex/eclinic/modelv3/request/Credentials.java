package com.alex.eclinic.modelv3.request;

import com.alex.eclinic.modelv3.BaseModel;
import com.alex.eclinic.modelv3.GrantType;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 15.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class Credentials extends BaseModel {
    private String username;
    private String password;
    @JsonProperty("refresh_token")
    private String refreshToken;
    @JsonProperty("grant_type")
    private GrantType grantType;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public GrantType getGrantType() {
        return grantType;
    }

    public void setGrantType(GrantType grantType) {
        this.grantType = grantType;
    }
}
