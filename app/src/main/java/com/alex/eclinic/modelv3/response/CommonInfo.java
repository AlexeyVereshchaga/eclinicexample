
package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.ErrorModel;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommonInfo extends ErrorModel {

    @JsonProperty("meta")
    private Meta meta;
    @JsonProperty("calls")
    private List<Call> calls = new ArrayList<Call>();

    /**
     * @return The meta
     */
    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    /**
     * @param meta The meta
     */
    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     * @return The calls
     */
    @JsonProperty("calls")
    public List<Call> getCalls() {
        Collections.sort(calls, Collections.reverseOrder());
        return calls;
    }

    /**
     * @param calls The calls
     */
    @JsonProperty("calls")
    public void setCalls(List<Call> calls) {
        this.calls = calls;
    }

}
