package com.alex.eclinic.modelv3.request;


import com.alex.eclinic.modelv3.BaseModel;
import com.alex.eclinic.modelv3.RemoteAction;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 19.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class Remote extends BaseModel {

    private RemoteAction action;
    private String message;
    private String data;
    @JsonProperty("forward_to")
    private Integer forwardId;
    @JsonProperty("completion_message")
    private String completionMessage;

    public RemoteAction getAction() {
        return action;
    }

    public void setAction(RemoteAction action) {
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getForwardId() {
        return forwardId;
    }

    public void setForwardId(Integer forwardId) {
        this.forwardId = forwardId;
    }

    public String getCompletionMessage() {
        return completionMessage;
    }

    public void setCompletionMessage(String completionMessage) {
        this.completionMessage = completionMessage;
    }
}
