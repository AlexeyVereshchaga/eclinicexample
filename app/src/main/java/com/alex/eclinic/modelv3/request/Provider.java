package com.alex.eclinic.modelv3.request;

import com.alex.eclinic.modelv3.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 23.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class Provider extends BaseModel {
    @JsonProperty("email_address")
    private String eMailAddress;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("allow_mobile_notifications")
    private Boolean allowMobileNotifications;
    @JsonProperty("allow_email_notifications")
    private Boolean allowEmailNotifications;
    @JsonProperty("allow_voice_notifications")
    private Boolean allowVoiceNotifications;
    @JsonProperty("allow_sms_notifications")
    private Boolean allowSmsNotifications;

    public Provider() {
    }

    public Provider(String eMailAddress, String phoneNumber, Boolean allowMobileNotifications,
                    Boolean allowEmailNotifications, Boolean allowVoiceNotifications,
                    Boolean allowSmsNotifications) {
        this.eMailAddress = eMailAddress;
        this.phoneNumber = phoneNumber;
        this.allowMobileNotifications = allowMobileNotifications;
        this.allowEmailNotifications = allowEmailNotifications;
        this.allowVoiceNotifications = allowVoiceNotifications;
        this.allowSmsNotifications = allowSmsNotifications;
    }

    public String geteMailAddress() {
        return eMailAddress;
    }

    public void seteMailAddress(String eMailAddress) {
        this.eMailAddress = eMailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getAllowMobileNotifications() {
        return allowMobileNotifications;
    }

    public void setAllowMobileNotifications(Boolean allowMobileNotifications) {
        this.allowMobileNotifications = allowMobileNotifications;
    }

    public Boolean getAllowEmailNotifications() {
        return allowEmailNotifications;
    }

    public void setAllowEmailNotifications(Boolean allowEmailNotifications) {
        this.allowEmailNotifications = allowEmailNotifications;
    }

    public Boolean getAllowVoiceNotifications() {
        return allowVoiceNotifications;
    }

    public void setAllowVoiceNotifications(Boolean allowVoiceNotifications) {
        this.allowVoiceNotifications = allowVoiceNotifications;
    }

    public Boolean getAllowSmsNotifications() {
        return allowSmsNotifications;
    }

    public void setAllowSmsNotifications(Boolean allowSmsNotifications) {
        this.allowSmsNotifications = allowSmsNotifications;
    }
}
