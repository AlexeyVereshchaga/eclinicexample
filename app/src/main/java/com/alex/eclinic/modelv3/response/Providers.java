
package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.ErrorModel;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Providers extends ErrorModel {

    @JsonProperty("providers")
    private List<Provider> providers = new ArrayList<Provider>();

    /**
     * @return The providers
     */
    @JsonProperty("providers")
    public List<Provider> getProviders() {
        return providers;
    }

    /**
     * @param providers The providers
     */
    @JsonProperty("providers")
    public void setProviders(List<Provider> providers) {
        this.providers = providers;
    }

}
