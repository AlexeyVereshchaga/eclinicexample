package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 27.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class CallInfo extends BaseModel {

    @JsonProperty("incomplete_calls")
    private Integer incompleteCalls;
    @JsonProperty("completed_calls")
    private Integer completedCalls;
    @JsonProperty("provider_id")
    private Integer providerId;

    public Integer getIncompleteCalls() {
        return incompleteCalls;
    }

    public void setIncompleteCalls(Integer incompleteCalls) {
        this.incompleteCalls = incompleteCalls;
    }

    public Integer getCompletedCalls() {
        return completedCalls;
    }

    public void setCompletedCalls(Integer completedCalls) {
        this.completedCalls = completedCalls;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }
}
