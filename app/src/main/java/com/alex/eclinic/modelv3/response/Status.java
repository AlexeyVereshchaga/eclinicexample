package com.alex.eclinic.modelv3.response;


import com.alex.eclinic.modelv3.BaseModel;

/**
 * Class description
 * 19.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class Status extends BaseModel {

    private String status;
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
