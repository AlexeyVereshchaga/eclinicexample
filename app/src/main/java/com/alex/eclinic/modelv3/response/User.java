
package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class User extends BaseModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("email")
    private String email;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("roles")
    private List<String> roles = new ArrayList<String>();
    @JsonProperty("profile_image")
    private String profileImage;
    @JsonProperty("profile_image_retina")
    private String profileImageRetina;
    @JsonProperty("providers")
    private List<ProfileProvider> providers = new ArrayList<ProfileProvider>();
    @JsonProperty("partners")
    private List<Partner> partners = new ArrayList<Partner>();

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The firstName
     */
    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName The first_name
     */
    @JsonProperty("first_name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return The lastName
     */
    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName The last_name
     */
    @JsonProperty("last_name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return The roles
     */
    @JsonProperty("roles")
    public List<String> getRoles() {
        return roles;
    }

    /**
     * @param roles The roles
     */
    @JsonProperty("roles")
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    /**
     * @return The profileImage
     */
    @JsonProperty("profile_image")
    public String getProfileImage() {
        return profileImage;
    }

    /**
     * @param profileImage The profile_image
     */
    @JsonProperty("profile_image")
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * @return The profileImageRetina
     */
    @JsonProperty("profile_image_retina")
    public String getProfileImageRetina() {
        return profileImageRetina;
    }

    /**
     * @param profileImageRetina The profile_image_retina
     */
    @JsonProperty("profile_image_retina")
    public void setProfileImageRetina(String profileImageRetina) {
        this.profileImageRetina = profileImageRetina;
    }

    /**
     * @return The providers
     */
    @JsonProperty("providers")
    public List<ProfileProvider> getProviders() {
        return providers;
    }

    /**
     * @param providers The providers
     */
    @JsonProperty("providers")
    public void setProviders(List<ProfileProvider> providers) {
        this.providers = providers;
    }

    /**
     * @return The partners
     */
    @JsonProperty("partners")
    public List<Partner> getPartners() {
        return partners;
    }

    /**
     * @param partners The partners
     */
    @JsonProperty("partners")
    public void setPartners(List<Partner> partners) {
        this.partners = partners;
    }

}
