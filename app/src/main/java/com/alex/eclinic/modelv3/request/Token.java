package com.alex.eclinic.modelv3.request;

import com.alex.eclinic.modelv3.BaseModel;

/**
 * Class description
 * 16.11.15.
 *
 * @author Alexey Vereshchaga
 */
public class Token extends BaseModel {

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String token;
}
