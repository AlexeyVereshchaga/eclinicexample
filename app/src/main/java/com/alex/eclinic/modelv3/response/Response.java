package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.ErrorModel;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 18.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class Response extends ErrorModel {

    @JsonProperty("success")
    private String success;
    @JsonProperty("status")
    private String status;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
