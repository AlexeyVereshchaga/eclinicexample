package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.ErrorModel;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * 27.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class CountInfo extends ErrorModel {

    @JsonProperty("data")
    private List<CallInfo> callInfoList = new ArrayList<>();

    public List<CallInfo> getCallInfoList() {
        return callInfoList;
    }

    public void setCallInfoList(List<CallInfo> callInfoList) {
        this.callInfoList = callInfoList;
    }
}
