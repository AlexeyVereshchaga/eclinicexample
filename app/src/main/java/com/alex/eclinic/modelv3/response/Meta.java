
package com.alex.eclinic.modelv3.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Meta {

    @JsonProperty("total")
    private Integer total;
    @JsonProperty("per_page")
    private Integer perPage;
    @JsonProperty("current_page")
    private Integer currentPage;
    @JsonProperty("last_page")
    private Integer lastPage;
    @JsonProperty("from")
    private Integer from;
    @JsonProperty("to")
    private Integer to;

    /**
     * @return The total
     */
    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * @return The perPage
     */
    @JsonProperty("per_page")
    public Integer getPerPage() {
        return perPage;
    }

    /**
     * @param perPage The per_page
     */
    @JsonProperty("per_page")
    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    /**
     * @return The currentPage
     */
    @JsonProperty("current_page")
    public Integer getCurrentPage() {
        return currentPage;
    }

    /**
     * @param currentPage The current_page
     */
    @JsonProperty("current_page")
    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * @return The lastPage
     */
    @JsonProperty("last_page")
    public Integer getLastPage() {
        return lastPage;
    }

    /**
     * @param lastPage The last_page
     */
    @JsonProperty("last_page")
    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

    /**
     * @return The from
     */
    @JsonProperty("from")
    public Integer getFrom() {
        return from;
    }

    /**
     * @param from The from
     */
    @JsonProperty("from")
    public void setFrom(Integer from) {
        this.from = from;
    }

    /**
     * @return The to
     */
    @JsonProperty("to")
    public Integer getTo() {
        return to;
    }

    /**
     * @param to The to
     */
    @JsonProperty("to")
    public void setTo(Integer to) {
        this.to = to;
    }

}
