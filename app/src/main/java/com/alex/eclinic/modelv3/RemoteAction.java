package com.alex.eclinic.modelv3;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Class description
 * 02.11.15.
 *
 * @author Alexey Vereshchaga
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public enum RemoteAction {
    ARCHIVE("archive"),
    FORWARD("forward"),
    URGENT_FORWARD("urgent_forward"),
    CALL_BACK("call_back"),
    NOTE("note"),
    TEXT_MESSAGE("text_message"),
    VOICE_MESSAGE("voice_message"),
    NONE("none");

    private String action;

    RemoteAction(String action) {
        this.action = action;
    }

    @JsonCreator
    public static RemoteAction fromString(String action) {
        return action == null
                ? null
                : RemoteAction.valueOf(action.toUpperCase());
    }

    @JsonValue
    public String getAction() {
        return action;
    }
}
