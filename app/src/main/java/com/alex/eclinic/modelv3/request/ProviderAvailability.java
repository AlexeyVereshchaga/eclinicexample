package com.alex.eclinic.modelv3.request;

import com.alex.eclinic.modelv3.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 23.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class ProviderAvailability extends BaseModel {

    @JsonProperty("patient_available")
    private Boolean patientAvailable;

    public Boolean getPatientAvailable() {
        return patientAvailable;
    }

    public void setPatientAvailable(Boolean patientAvailable) {
        this.patientAvailable = patientAvailable;
    }
}
