
package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.BaseModel;
import com.alex.eclinic.modelv3.ErrorModel;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Provider extends ErrorModel{

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("email_address")
    private String emailAddress;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("pager_email_address")
    private String pagerEmailAddress;
    @JsonProperty("pager_phone_number")
    private String pagerPhoneNumber;
    @JsonProperty("allow_sms_notifications")
    private Boolean allowSmsNotifications;
    @JsonProperty("allow_voice_notifications")
    private Boolean allowVoiceNotifications;
    @JsonProperty("allow_email_notifications")
    private Boolean allowEmailNotifications;
    @JsonProperty("allow_mobile_notifications")
    private Boolean allowMobileNotifications;
    @JsonProperty("allow_pager_notifications")
    private Boolean allowPagerNotifications;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("patient_available")
    private Boolean patientAvailable;
    @JsonProperty("points_of_contact")
    private List<Object> pointsOfContact = new ArrayList<Object>();
    @JsonProperty("type")
    private String type;
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("color")
    private String color;
    @JsonProperty("user")
    private User user;
    @JsonProperty("partner")
    private Partner partner;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The emailAddress
     */
    @JsonProperty("email_address")
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @param emailAddress The email_address
     */
    @JsonProperty("email_address")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @return The phoneNumber
     */
    @JsonProperty("phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber The phone_number
     */
    @JsonProperty("phone_number")
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return The pagerEmailAddress
     */
    @JsonProperty("pager_email_address")
    public String getPagerEmailAddress() {
        return pagerEmailAddress;
    }

    /**
     * @param pagerEmailAddress The pager_email_address
     */
    @JsonProperty("pager_email_address")
    public void setPagerEmailAddress(String pagerEmailAddress) {
        this.pagerEmailAddress = pagerEmailAddress;
    }

    /**
     * @return The pagerPhoneNumber
     */
    @JsonProperty("pager_phone_number")
    public String getPagerPhoneNumber() {
        return pagerPhoneNumber;
    }

    /**
     * @param pagerPhoneNumber The pager_phone_number
     */
    @JsonProperty("pager_phone_number")
    public void setPagerPhoneNumber(String pagerPhoneNumber) {
        this.pagerPhoneNumber = pagerPhoneNumber;
    }

    /**
     * @return The allowSmsNotifications
     */
    @JsonProperty("allow_sms_notifications")
    public Boolean getAllowSmsNotifications() {
        return allowSmsNotifications;
    }

    /**
     * @param allowSmsNotifications The allow_sms_notifications
     */
    @JsonProperty("allow_sms_notifications")
    public void setAllowSmsNotifications(Boolean allowSmsNotifications) {
        this.allowSmsNotifications = allowSmsNotifications;
    }

    /**
     * @return The allowVoiceNotifications
     */
    @JsonProperty("allow_voice_notifications")
    public Boolean getAllowVoiceNotifications() {
        return allowVoiceNotifications;
    }

    /**
     * @param allowVoiceNotifications The allow_voice_notifications
     */
    @JsonProperty("allow_voice_notifications")
    public void setAllowVoiceNotifications(Boolean allowVoiceNotifications) {
        this.allowVoiceNotifications = allowVoiceNotifications;
    }

    /**
     * @return The allowEmailNotifications
     */
    @JsonProperty("allow_email_notifications")
    public Boolean getAllowEmailNotifications() {
        return allowEmailNotifications;
    }

    /**
     * @param allowEmailNotifications The allow_email_notifications
     */
    @JsonProperty("allow_email_notifications")
    public void setAllowEmailNotifications(Boolean allowEmailNotifications) {
        this.allowEmailNotifications = allowEmailNotifications;
    }

    /**
     * @return The allowMobileNotifications
     */
    @JsonProperty("allow_mobile_notifications")
    public Boolean getAllowMobileNotifications() {
        return allowMobileNotifications;
    }

    /**
     * @param allowMobileNotifications The allow_mobile_notifications
     */
    @JsonProperty("allow_mobile_notifications")
    public void setAllowMobileNotifications(Boolean allowMobileNotifications) {
        this.allowMobileNotifications = allowMobileNotifications;
    }

    /**
     * @return The allowPagerNotifications
     */
    @JsonProperty("allow_pager_notifications")
    public Boolean getAllowPagerNotifications() {
        return allowPagerNotifications;
    }

    /**
     * @param allowPagerNotifications The allow_pager_notifications
     */
    @JsonProperty("allow_pager_notifications")
    public void setAllowPagerNotifications(Boolean allowPagerNotifications) {
        this.allowPagerNotifications = allowPagerNotifications;
    }

    /**
     * @return The createdAt
     */
    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The patientAvailable
     */
    @JsonProperty("patient_available")
    public Boolean getPatientAvailable() {
        return patientAvailable;
    }

    /**
     * @param patientAvailable The patient_available
     */
    @JsonProperty("patient_available")
    public void setPatientAvailable(Boolean patientAvailable) {
        this.patientAvailable = patientAvailable;
    }

    /**
     * @return The pointsOfContact
     */
    @JsonProperty("points_of_contact")
    public List<Object> getPointsOfContact() {
        return pointsOfContact;
    }

    /**
     * @param pointsOfContact The points_of_contact
     */
    @JsonProperty("points_of_contact")
    public void setPointsOfContact(List<Object> pointsOfContact) {
        this.pointsOfContact = pointsOfContact;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The fullName
     */
    @JsonProperty("full_name")
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName The full_name
     */
    @JsonProperty("full_name")
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return The color
     */
    @JsonProperty("color")
    public String getColor() {
        return color;
    }

    /**
     * @param color The color
     */
    @JsonProperty("color")
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return The user
     */
    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }

    @JsonProperty("partner")
    public Partner getPartner() {
        return partner;
    }

    @JsonProperty("partner")
    public void setPartner(Partner partner) {
        this.partner = partner;
    }
}
