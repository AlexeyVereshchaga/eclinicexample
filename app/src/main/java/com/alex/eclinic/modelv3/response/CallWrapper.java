package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.ErrorModel;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 04.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class CallWrapper extends ErrorModel {
    @JsonProperty("call")
    private Call call;

    public Call getCall() {
        return call;
    }

    public void setCall(Call call) {
        this.call = call;
    }
}
