package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.ErrorModel;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 25.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class IncompleteCount extends ErrorModel {

    @JsonProperty("urgent")
    private Integer urgent;
    @JsonProperty("non_urgent")
    private Integer nonUrgent;
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("provider_id")
    private Integer providerId;

    public Integer getUrgent() {
        return urgent;
    }

    public void setUrgent(Integer urgent) {
        this.urgent = urgent;
    }

    public Integer getNonUrgent() {
        return nonUrgent;
    }

    public void setNonUrgent(Integer nonUrgent) {
        this.nonUrgent = nonUrgent;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }
}
