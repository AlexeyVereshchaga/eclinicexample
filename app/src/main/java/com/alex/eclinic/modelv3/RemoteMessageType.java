package com.alex.eclinic.modelv3;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Class description
 * 02.11.15.
 *
 * @author Alexey Vereshchaga
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public enum RemoteMessageType {
    NAME("name"),
    MESSAGE("message"),
    DOB("dob");

    private String type;

    RemoteMessageType(String type) {
        this.type = type;
    }

    @JsonCreator
    public static RemoteMessageType fromString(String type) {
        return type == null
                ? null
                : RemoteMessageType.valueOf(type.toUpperCase());
    }

    @JsonValue
    public String getType() {
        return type;
    }
}
