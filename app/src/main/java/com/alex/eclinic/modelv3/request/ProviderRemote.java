package com.alex.eclinic.modelv3.request;

import com.alex.eclinic.modelv3.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 23.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class ProviderRemote extends BaseModel {
    @JsonProperty("provider")
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }
}
