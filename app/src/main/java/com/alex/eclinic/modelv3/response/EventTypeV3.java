package com.alex.eclinic.modelv3.response;

/**
 * 04.07.16.
 *
 * @author Alexey Vereshchaga
 */
public enum EventTypeV3 {
    CREATE, NOTIFICATION, RESPONSE, CALLBACK, SNOOZED, DISMISS, ARCHIVE, FORWARD, URGENT_FORWARD, NOTE, MODIFY_CALL, COMPLETE
}
