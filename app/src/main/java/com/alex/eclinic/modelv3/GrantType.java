package com.alex.eclinic.modelv3;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 15.04.16.
 *
 * @author Alexey Vereshchaga
 */
public enum GrantType {

    @JsonProperty("password")
    PASSWORD,
    @JsonProperty("refresh_token")
    REFRESH_TOKEN;
}
