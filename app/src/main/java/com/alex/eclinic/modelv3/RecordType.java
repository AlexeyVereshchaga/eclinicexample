package com.alex.eclinic.modelv3;

/**
 * Class description
 * 02.11.15.
 *
 * @author Alexey Vereshchaga
 */

public enum RecordType {
    NAME,
    MESSAGE,
    DOB;
}
