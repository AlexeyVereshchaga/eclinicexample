package com.alex.eclinic.modelv3.request;

import com.alex.eclinic.modelv3.BaseModel;

/**
 * 09.02.16.
 *
 * @author Alexey Vereshchaga
 */
public class Reset extends BaseModel {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
