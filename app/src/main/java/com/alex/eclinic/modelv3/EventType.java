package com.alex.eclinic.modelv3;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Class description
 * 04.11.15.
 *
 * @author Alexey Vereshchaga
 */
public enum EventType {
    TEXT_MESSAGE("text_message"),
    TEXT_TO_SPEECH("text_to_speech"),
    VOICE_RECORDING("voice_recording"),
    CALL_BACK("call_back"),
    DISMISS("dismiss");

    private String type;

    EventType(String type) {
        this.type = type;
    }

    @JsonCreator
    public static EventType fromString(String type) {
        return type == null
                ? null
                : EventType.valueOf(type.toUpperCase());
    }

    @JsonValue
    public String getType() {
        return type;
    }
}
