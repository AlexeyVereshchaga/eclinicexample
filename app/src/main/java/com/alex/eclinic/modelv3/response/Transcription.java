
package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Transcription extends BaseModel{

    @JsonProperty("sid")
    private String sid;
    @JsonProperty("text")
    private String text;
    @JsonProperty("created_at")
    private String createdAt;

    /**
     * @return The sid
     */
    @JsonProperty("sid")
    public String getSid() {
        return sid;
    }

    /**
     * @param sid The sid
     */
    @JsonProperty("sid")
    public void setSid(String sid) {
        this.sid = sid;
    }

    /**
     * @return The text
     */
    @JsonProperty("text")
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The createdAt
     */
    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
