
package com.alex.eclinic.modelv3.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Access extends Authentication {

    @JsonProperty("user")
    private User user;

    /**
     * @return The user
     */
    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }

}
