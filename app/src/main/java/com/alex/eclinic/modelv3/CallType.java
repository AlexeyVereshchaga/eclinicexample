package com.alex.eclinic.modelv3;

/**
 * 17.05.16.
 *
 * @author Alexey Vereshchaga
 */
public enum CallType {
    NEW, FORWARDED, COMPLETE, ARCHIVE
}
