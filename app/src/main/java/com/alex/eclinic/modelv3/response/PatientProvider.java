
package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PatientProvider extends BaseModel{

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("patient_available")
    private Boolean patientAvailable;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("position")
    private Integer position;
    @JsonProperty("type")
    private String type;
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("digit_index")
    private String digitIndex;
    @JsonProperty("color")
    private String color;
    @JsonProperty("user")
    private CallUser user;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The patientAvailable
     */
    @JsonProperty("patient_available")
    public Boolean getPatientAvailable() {
        return patientAvailable;
    }

    /**
     * @param patientAvailable The patient_available
     */
    @JsonProperty("patient_available")
    public void setPatientAvailable(Boolean patientAvailable) {
        this.patientAvailable = patientAvailable;
    }

    /**
     * @return The createdAt
     */
    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The position
     */
    @JsonProperty("position")
    public Integer getPosition() {
        return position;
    }

    /**
     * @param position The position
     */
    @JsonProperty("position")
    public void setPosition(Integer position) {
        this.position = position;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The fullName
     */
    @JsonProperty("full_name")
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName The full_name
     */
    @JsonProperty("full_name")
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return The digitIndex
     */
    @JsonProperty("digit_index")
    public String getDigitIndex() {
        return digitIndex;
    }

    /**
     * @param digitIndex The digit_index
     */
    @JsonProperty("digit_index")
    public void setDigitIndex(String digitIndex) {
        this.digitIndex = digitIndex;
    }

    /**
     * @return The color
     */
    @JsonProperty("color")
    public String getColor() {
        return color;
    }

    /**
     * @param color The color
     */
    @JsonProperty("color")
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return The user
     */
    @JsonProperty("user")
    public CallUser getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    @JsonProperty("user")
    public void setUser(CallUser user) {
        this.user = user;
    }

}
