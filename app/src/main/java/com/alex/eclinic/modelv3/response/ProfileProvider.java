
package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProfileProvider extends BaseModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("patient_available")
    private Boolean patientAvailable;
    @JsonProperty("email")
    private String email;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("allow_mobile_notifications")
    private Boolean allowMobileNotifications;
    @JsonProperty("allow_email_notifications")
    private Boolean allowEmailNotifications;
    @JsonProperty("allow_voice_notifications")
    private Boolean allowVoiceNotifications;
    @JsonProperty("allow_sms_notifications")
    private Boolean allowSmsNotifications;
    @JsonProperty("partner_id")
    private Integer partnerId;
    @JsonProperty("partner")
    private Partner partner;
    private Integer incompleteCalls;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The patientAvailable
     */
    @JsonProperty("patient_available")
    public Boolean getPatientAvailable() {
        return patientAvailable;
    }

    /**
     * @param patientAvailable The patient_available
     */
    @JsonProperty("patient_available")
    public void setPatientAvailable(Boolean patientAvailable) {
        this.patientAvailable = patientAvailable;
    }

    /**
     * @return The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The phoneNumber
     */
    @JsonProperty("phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber The phone_number
     */
    @JsonProperty("phone_number")
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return The allowMobileNotifications
     */
    @JsonProperty("allow_mobile_notifications")
    public Boolean getAllowMobileNotifications() {
        return allowMobileNotifications;
    }

    /**
     * @param allowMobileNotifications The allow_mobile_notifications
     */
    @JsonProperty("allow_mobile_notifications")
    public void setAllowMobileNotifications(Boolean allowMobileNotifications) {
        this.allowMobileNotifications = allowMobileNotifications;
    }

    /**
     * @return The allowEmailNotifications
     */
    @JsonProperty("allow_email_notifications")
    public Boolean getAllowEmailNotifications() {
        return allowEmailNotifications;
    }

    /**
     * @param allowEmailNotifications The allow_email_notifications
     */
    @JsonProperty("allow_email_notifications")
    public void setAllowEmailNotifications(Boolean allowEmailNotifications) {
        this.allowEmailNotifications = allowEmailNotifications;
    }

    /**
     * @return The allowVoiceNotifications
     */
    @JsonProperty("allow_voice_notifications")
    public Boolean getAllowVoiceNotifications() {
        return allowVoiceNotifications;
    }

    /**
     * @param allowVoiceNotifications The allow_voice_notifications
     */
    @JsonProperty("allow_voice_notifications")
    public void setAllowVoiceNotifications(Boolean allowVoiceNotifications) {
        this.allowVoiceNotifications = allowVoiceNotifications;
    }

    /**
     * @return The allowSmsNotifications
     */
    @JsonProperty("allow_sms_notifications")
    public Boolean getAllowSmsNotifications() {
        return allowSmsNotifications;
    }

    /**
     * @param allowSmsNotifications The allow_sms_notifications
     */
    @JsonProperty("allow_sms_notifications")
    public void setAllowSmsNotifications(Boolean allowSmsNotifications) {
        this.allowSmsNotifications = allowSmsNotifications;
    }

    /**
     * @return The partnerId
     */
    @JsonProperty("partner_id")
    public Integer getPartnerId() {
        return partnerId;
    }

    /**
     * @param partnerId The partner_id
     */
    @JsonProperty("partner_id")
    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    /**
     * @return The partner
     */
    @JsonProperty("partner")
    public Partner getPartner() {
        return partner;
    }

    /**
     * @param partner The partner
     */
    @JsonProperty("partner")
    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public Integer getIncompleteCalls() {
        return incompleteCalls;
    }

    public void setIncompleteCalls(Integer incompleteCalls) {
        this.incompleteCalls = incompleteCalls;
    }
}
