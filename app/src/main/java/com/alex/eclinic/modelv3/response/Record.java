
package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.BaseModel;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Record extends BaseModel{

    @JsonProperty("sid")
    private String sid;
    @JsonProperty("type")
    private String type;
    @JsonProperty("recording_url")
    private String recordingUrl;
    @JsonProperty("recording_duration")
    private String recordingDuration;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("transcription")
    private Transcription transcription;

    /**
     * @return The sid
     */
    @JsonProperty("sid")
    public String getSid() {
        return sid;
    }

    /**
     * @param sid The sid
     */
    @JsonProperty("sid")
    public void setSid(String sid) {
        this.sid = sid;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The recordingUrl
     */
    @JsonProperty("recording_url")
    public String getRecordingUrl() {
        return recordingUrl;
    }

    /**
     * @param recordingUrl The recording_url
     */
    @JsonProperty("recording_url")
    public void setRecordingUrl(String recordingUrl) {
        this.recordingUrl = recordingUrl;
    }

    /**
     * @return The recordingDuration
     */
    @JsonProperty("recording_duration")
    public String getRecordingDuration() {
        return recordingDuration;
    }

    /**
     * @param recordingDuration The recording_duration
     */
    @JsonProperty("recording_duration")
    public void setRecordingDuration(String recordingDuration) {
        this.recordingDuration = recordingDuration;
    }

    /**
     * @return The createdAt
     */
    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The transcription
     */
    @JsonProperty("transcription")
    public Transcription getTranscription() {
        return transcription;
    }

    /**
     * @param transcription The transcription
     */
    @JsonProperty("transcription")
    public void setTranscription(Transcription transcription) {
        this.transcription = transcription;
    }

}
