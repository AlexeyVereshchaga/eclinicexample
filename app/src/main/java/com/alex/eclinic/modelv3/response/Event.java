
package com.alex.eclinic.modelv3.response;

import com.alex.eclinic.modelv3.BaseModel;
import com.alex.eclinic.modelv3.EventType;
import com.alex.eclinic.utils.HelpUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * This class is a mix of two classes from the list of "responses" and "recordings", since these classes are used on the client in the same list.
 */
public class Event extends BaseModel implements Comparable<Event> {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("call_id")
    private Integer callId;
    @JsonProperty("provider_id")
    private Integer providerId;
    @JsonProperty("type")
    private EventType type;
    @JsonProperty("text_to_patient")
    private String textToPatient;
    @JsonProperty("recording_to_patient_file_name")
    private String recordingToPatientFileName;
    @JsonProperty("recording_to_patient_file_size")
    private Long recordingToPatientFileSize;
    @JsonProperty("recording_to_patient_content_type")
    private String recordingToPatientContentType;
    @JsonProperty("recording_to_patient_updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = HelpUtils.CALL_DATE_FORMAT)
    private Date recordingToPatientUpdatedAt;
    @JsonProperty("created_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = HelpUtils.CALL_DATE_FORMAT)
    private Date createdAt;
    @JsonProperty("updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = HelpUtils.CALL_DATE_FORMAT)
    private Date updatedAt;
    @JsonProperty("forward_to_provider_id")
    private Integer forwardToProviderId;
    @JsonProperty("message")
    private String message;
    @JsonProperty("voice_message_file_name")
    private String voiceMessageFileName;
    @JsonProperty("voice_message_file_size")
    private Long voiceMessageFileSize;
    @JsonProperty("voice_message_content_type")
    private String voiceMessageContentType;
    @JsonProperty("voice_message_updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = HelpUtils.CALL_DATE_FORMAT)
    private Date voiceMessageUpdatedAt;
    @JsonProperty("forwarded_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = HelpUtils.CALL_DATE_FORMAT)
    private Date forwardedAt;
    @JsonProperty("voice_message_url")
    private String voiceMessageUrl;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The callId
     */
    @JsonProperty("call_id")
    public Integer getCallId() {
        return callId;
    }

    /**
     * @param callId The call_id
     */
    @JsonProperty("call_id")
    public void setCallId(Integer callId) {
        this.callId = callId;
    }

    /**
     * @return The providerId
     */
    @JsonProperty("provider_id")
    public Integer getProviderId() {
        return providerId;
    }

    /**
     * @param providerId The provider_id
     */
    @JsonProperty("provider_id")
    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public EventType getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(EventType type) {
        this.type = type;
    }

    /**
     * @return The textToPatient
     */
    @JsonProperty("text_to_patient")
    public String getTextToPatient() {
        return textToPatient;
    }

    /**
     * @param textToPatient The text_to_patient
     */
    @JsonProperty("text_to_patient")
    public void setTextToPatient(String textToPatient) {
        this.textToPatient = textToPatient;
    }

    /**
     * @return The recordingToPatientFileName
     */
    @JsonProperty("recording_to_patient_file_name")
    public String getRecordingToPatientFileName() {
        return recordingToPatientFileName;
    }

    /**
     * @param recordingToPatientFileName The recording_to_patient_file_name
     */
    @JsonProperty("recording_to_patient_file_name")
    public void setRecordingToPatientFileName(String recordingToPatientFileName) {
        this.recordingToPatientFileName = recordingToPatientFileName;
    }

    /**
     * @return The recordingToPatientFileSize
     */
    @JsonProperty("recording_to_patient_file_size")
    public Long getRecordingToPatientFileSize() {
        return recordingToPatientFileSize;
    }

    /**
     * @param recordingToPatientFileSize The recording_to_patient_file_size
     */
    @JsonProperty("recording_to_patient_file_size")
    public void setRecordingToPatientFileSize(Long recordingToPatientFileSize) {
        this.recordingToPatientFileSize = recordingToPatientFileSize;
    }

    /**
     * @return The recordingToPatientContentType
     */
    @JsonProperty("recording_to_patient_content_type")
    public String getRecordingToPatientContentType() {
        return recordingToPatientContentType;
    }

    /**
     * @param recordingToPatientContentType The recording_to_patient_content_type
     */
    @JsonProperty("recording_to_patient_content_type")
    public void setRecordingToPatientContentType(String recordingToPatientContentType) {
        this.recordingToPatientContentType = recordingToPatientContentType;
    }

    /**
     * @return The recordingToPatientUpdatedAt
     */
    @JsonProperty("recording_to_patient_updated_at")
    public Date getRecordingToPatientUpdatedAt() {
        return recordingToPatientUpdatedAt;
    }

    /**
     * @param recordingToPatientUpdatedAt The recording_to_patient_updated_at
     */
    @JsonProperty("recording_to_patient_updated_at")
    public void setRecordingToPatientUpdatedAt(Date recordingToPatientUpdatedAt) {
        this.recordingToPatientUpdatedAt = recordingToPatientUpdatedAt;
    }

    /**
     * @return The createdAt
     */
    @JsonProperty("created_at")
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    @JsonProperty("created_at")
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The updatedAt
     */
    @JsonProperty("updated_at")
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt The updated_at
     */
    @JsonProperty("updated_at")
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return The forwardToProviderId
     */
    @JsonProperty("forward_to_provider_id")
    public Integer getForwardToProviderId() {
        return forwardToProviderId;
    }

    /**
     * @param forwardToProviderId The forward_to_provider_id
     */
    @JsonProperty("forward_to_provider_id")
    public void setForwardToProviderId(Integer forwardToProviderId) {
        this.forwardToProviderId = forwardToProviderId;
    }

    /**
     * @return The message
     */
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The voiceMessageFileName
     */
    @JsonProperty("voice_message_file_name")
    public String getVoiceMessageFileName() {
        return voiceMessageFileName;
    }

    /**
     * @param voiceMessageFileName The voice_message_file_name
     */
    @JsonProperty("voice_message_file_name")
    public void setVoiceMessageFileName(String voiceMessageFileName) {
        this.voiceMessageFileName = voiceMessageFileName;
    }

    /**
     * @return The voiceMessageFileSize
     */
    @JsonProperty("voice_message_file_size")
    public Long getVoiceMessageFileSize() {
        return voiceMessageFileSize;
    }

    /**
     * @param voiceMessageFileSize The voice_message_file_size
     */
    @JsonProperty("voice_message_file_size")
    public void setVoiceMessageFileSize(Long voiceMessageFileSize) {
        this.voiceMessageFileSize = voiceMessageFileSize;
    }

    /**
     * @return The voiceMessageContentType
     */
    @JsonProperty("voice_message_content_type")
    public String getVoiceMessageContentType() {
        return voiceMessageContentType;
    }

    /**
     * @param voiceMessageContentType The voice_message_content_type
     */
    @JsonProperty("voice_message_content_type")
    public void setVoiceMessageContentType(String voiceMessageContentType) {
        this.voiceMessageContentType = voiceMessageContentType;
    }

    /**
     * @return The voiceMessageUpdatedAt
     */
    @JsonProperty("voice_message_updated_at")
    public Date getVoiceMessageUpdatedAt() {
        return voiceMessageUpdatedAt;
    }

    /**
     * @param voiceMessageUpdatedAt The voice_message_updated_at
     */
    @JsonProperty("voice_message_updated_at")
    public void setVoiceMessageUpdatedAt(Date voiceMessageUpdatedAt) {
        this.voiceMessageUpdatedAt = voiceMessageUpdatedAt;
    }

    /**
     * @return The forwardedAt
     */
    @JsonProperty("forwarded_at")
    public Date getForwardedAt() {
        return forwardedAt;
    }

    /**
     * @param forwardedAt The forwarded_at
     */
    @JsonProperty("forwarded_at")
    public void setForwardedAt(Date forwardedAt) {
        this.forwardedAt = forwardedAt;
    }

    /**
     * @return The voiceMessageUrl
     */
    @JsonProperty("voice_message_url")
    public String getVoiceMessageUrl() {
        return voiceMessageUrl;
    }

    /**
     * @param voiceMessageUrl The voice_message_url
     */
    @JsonProperty("voice_message_url")
    public void setVoiceMessageUrl(String voiceMessageUrl) {
        this.voiceMessageUrl = voiceMessageUrl;
    }

    @Override
    public int compareTo(Event event) {
        if (getCreatedAt() == null || event.getCreatedAt() == null)
            return 0;
        return getCreatedAt().compareTo(event.getCreatedAt());
    }
}
