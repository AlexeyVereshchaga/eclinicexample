package com.alex.eclinic.utils.audio;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public class Wave implements Serializable {
    private static final long serialVersionUID = 1L;
    private WaveHeader waveHeader;
    private byte[] data;

    public Wave(String filename) {
        try {
            FileInputStream e = new FileInputStream(filename);
            this.initWaveWithInputStream(e);
            e.close();
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }

    private void initWaveWithInputStream(InputStream inputStream) {
        this.waveHeader = new WaveHeader(inputStream);
        if (this.waveHeader.isValid()) {
            try {
                this.data = new byte[inputStream.available()];
                inputStream.read(this.data);
            } catch (IOException var3) {
                var3.printStackTrace();
            }
        } else {
            System.err.println("Invalid Wave Header");
        }
    }

    public short[] getSampleAmplitudes() {
        int bytePerSample = this.waveHeader.getBitsPerSample() / 8;
        int numSamples = this.data.length / bytePerSample;
        short[] amplitudes = new short[numSamples];
        int pointer = 0;

        for (int i = 0; i < numSamples; ++i) {
            short amplitude = 0;

            for (int byteNumber = 0; byteNumber < bytePerSample; ++byteNumber) {
                amplitude |= (short) ((this.data[pointer++] & 255) << byteNumber * 8);
            }

            amplitudes[i] = amplitude;
        }

        return amplitudes;
    }
}
