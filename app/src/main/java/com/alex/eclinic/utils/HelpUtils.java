package com.alex.eclinic.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.alex.eclinic.R;
import com.alex.eclinic.modelv3.RecordType;
import com.alex.eclinic.modelv3.response.Call;
import com.alex.eclinic.modelv3.response.CallEvent;
import com.alex.eclinic.modelv3.response.EventTypeV3;
import com.alex.eclinic.modelv3.response.Provider;
import com.alex.eclinic.modelv3.response.Record;
import com.alex.eclinic.view.activity.StartActivity;
import com.alex.eclinic.view.fragment.Dialog;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Class description
 * 05.10.2015
 *
 * @author Alex.
 */
public class HelpUtils {

    public static final String CALL_SID_KEY = "call_sid";
    public static final String CALL_DETAILS_KEY = "call_details";
    public static final String FORGOT_PIN_KEY = "forgot_pin";

    public static final String CALL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String FOREGROUND_EVENT = "foreground_event";

    private static final String RESPONSE_TEXT_TEMPLATE = "'on' EEEE',' LLLL'\u00A0'd 'at' h:mm a";

    public static final String TYPE_EXTRA_KEY = "type";
    public static final String TYPE_EXTRA_VALUE_NEW_CALL = "new_call";

    public static final String ERROR_DIALOG_TAG = "0";
    public static final String DIALOG_TAG = "1";

    private static final String DOB_TEMPLATE = "MM.dd.yyyy"; //06.02.2000
    private static final String TAG = HelpUtils.class.getSimpleName();

    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        return point.x;
    }

    public static String encodeCredentials(String key) {
        return "Basic " + Base64.encodeToString(key.getBytes(), Base64.NO_WRAP);
    }

    public static <T> T getObjectFromJsonString(String objectStr, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        T obj = null;
        try {
            obj = mapper.readValue(objectStr, clazz);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static boolean isValidEmail(CharSequence email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(CharSequence password) {
        if (TextUtils.isEmpty(password)) {
            return false;
        } else {
            Pattern p = Pattern.compile(".{2,}");
            return p.matcher(password).matches();
        }
    }

    public static String getTimeAgo(Date transcribedDate) {
        String timeAgo = null;
        if (transcribedDate != null) {
            long diffInMs = (new Date()).getTime() - transcribedDate.getTime();
            long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
            timeAgo = convertTimeAgo(diffInSec);
        }
        return timeAgo;
    }

//    private static Date parseDate(String timeStr) {
//        SimpleDateFormat sdf = new SimpleDateFormat(CALL_DATE_FORMAT, Locale.US);     //"2015-10-05 10:12:03"
//        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
//        Date date = null;
//        try {
//            date = sdf.parse(timeStr);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return date;
//    }

    private static String convertTimeAgo(Long deltaSeconds) {
        Long deltaMinutes = deltaSeconds / 60l;
        if (deltaSeconds < 5) {
            return "Just now";
        } else if (deltaSeconds < 60) {
            return deltaSeconds + " seconds ago";
        } else if (deltaSeconds < 120) {
            return "A minute ago";
        } else if (deltaMinutes < 60) {
            return deltaMinutes + " minutes ago";
        } else if (deltaMinutes < 120) {
            return "An hour ago";
        } else if (deltaMinutes < (24 * 60)) {
            long hours = deltaMinutes / 60;
            return hours + " hours ago";
        } else if (deltaMinutes < (24 * 60 * 2)) {
            return "Yesterday";
        } else if (deltaMinutes < (24 * 60 * 7)) {
            long days = deltaMinutes / (60 * 24);
            return days + " days ago";
        } else if (deltaMinutes < (24 * 60 * 14)) {
            return "Last week";
        } else if (deltaMinutes < (24 * 60 * 31)) {
            long weeks = deltaMinutes / (60 * 24 * 7);
            return weeks + " weeks ago";
        } else if (deltaMinutes < (24 * 60 * 61)) {
            return "Last month";
        } else if (deltaMinutes < (24 * 60 * 365.25)) {
            long month = deltaMinutes / (60 * 24 * 30);
            return month + " months ago";
        } else if (deltaMinutes < (24 * 60 * 731)) {
            return "Last year";
        }
        long years = deltaMinutes / (60 * 24 * 365);
        return years + " years ago";
    }

//    public static void setCallStatusImage(Call call, ImageView imageView) {
//        List<Event> events = new ArrayList<>();
//        if (call.getResponses() != null) {
//            events.addAll(call.getResponses());
//        }
//        if (call.getForwards() != null) {
//            events.addAll(call.getForwards());
//        }
//        Collections.sort(events);
//        if (call.getCompletedAt() != null) {
//            imageView.setImageResource(R.drawable.status_complete);
//        } else if (!events.isEmpty()
//                && events.get(events.size() - 1).getForwardedAt() != null) {
//            imageView.setImageResource(R.drawable.status_forwarded);
//        } else if (!events.isEmpty()) {
//            imageView.setImageResource(R.drawable.status_replied);
//        } else {
//            imageView.setImageResource(R.drawable.status_unread);
//        }
//    }

    public static void setCallStatusImage(Call call, ImageView imageView) {
        List<CallEvent> callEvents = call.getCallEvents();
        int imageStatus;
        if (call.getCompletedAt() != null) {
            imageStatus = R.drawable.status_complete;
        } else if (callEvents.isEmpty()) {
            imageStatus = R.drawable.status_unread;
        } else if (isForwarded(callEvents)) {
            imageStatus = R.drawable.status_forwarded;
        } else {
            imageStatus = R.drawable.status_replied;
        }
        imageView.setImageResource(imageStatus);
    }

    private static boolean isForwarded(List<CallEvent> callEvents) {
        for (CallEvent callEvent :
                callEvents) {
            if (EventTypeV3.FORWARD.name().equalsIgnoreCase(callEvent.getType())
                    || EventTypeV3.URGENT_FORWARD.name().equalsIgnoreCase(callEvent.getType())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isCallUnread(Call call) {
        List<CallEvent> callEvents = call.getCallEvents();
        return call.getCompletedAt() == null && callEvents.isEmpty();
    }

//     public static void setEventStatusImage(Event event, ImageView imageView) {
//        if (event != null) {
//            if (event.getForwardedAt() != null) {
//                imageView.setImageResource(R.drawable.status_forwarded);
//            } else {
//                imageView.setImageResource(R.drawable.status_replied);
//            }
//        }
//    }

    public static void setEventStatusImage(CallEvent callEvent, ImageView imageView) {
        int imageStatus;
        if (callEvent != null) {
            if (EventTypeV3.FORWARD.name().equalsIgnoreCase(callEvent.getType())
                    || EventTypeV3.FORWARD.name().equalsIgnoreCase(callEvent.getType())) {
                imageStatus = R.drawable.status_forwarded;
            } else {
                imageStatus = R.drawable.status_replied;
            }
            imageView.setImageResource(imageStatus);
        }
    }

//    public static void setResponseTextToCall(Call call, List<Provider> providers, TextView textView) {
//        List<Event> events = new ArrayList<>();
//        if (call.getResponses() != null) {
//            events.addAll(call.getResponses());
//        }
//        if (call.getForwards() != null) {
//            events.addAll(call.getForwards());
//        }
//        Collections.sort(events);
//
//        Event lastEvent = null;
//        String dateStr = "";
//        if (!events.isEmpty()) {
//            lastEvent = events.get(events.size() - 1);
//            Date date = lastEvent.getCreatedAt();
//            SimpleDateFormat sdf = new SimpleDateFormat(RESPONSE_TEXT_TEMPLATE, Locale.US);
//            dateStr = sdf.format(date);
//        }
//        textView.setText(String.format("%1s %2s", setResponseText(lastEvent, providers), dateStr));
//    }

    public static void setResponseTextToCall(Call call, TextView textView) {
        List<CallEvent> events = call.getCallEvents();
        String lastEventSummary = "No Response";
        if (!events.isEmpty()) {
            String summary = events.get(events.size() - 1).getSummary();
            lastEventSummary =
                    summary != null && !summary.isEmpty() ?
                            summary :
                            lastEventSummary;
        }
        textView.setText(lastEventSummary);
    }

//    public static String setResponseText(Event event, List<Provider> providers) {
//        String person = "";
//        String action = "No Response";
//        if (event != null) {
//            person = getProviderName(event.getProviderId(), providers);
//            if (event.getForwardedAt() != null) {
//                action = "forwarded the call";
//            } else {
//                EventType type = event.getType();
//                if (type != null) {
//                    switch (type) {
//                        case TEXT_MESSAGE:
//                            action = "texted back";
//                            break;
//                        case TEXT_TO_SPEECH:
//                            action = "sent a message";
//                            break;
//                        case VOICE_RECORDING:
//                            action = "sent a voice message";
//                            break;
//                        case CALL_BACK:
//                            action = "called back";
//                            break;
//                        case DISMISS:
//                            action = "dismissed the call";
//                            break;
//                    }
//                }
//            }
//        }
//        return person == null || person.isEmpty() ? action : person + " " + action;
//    }

    public static String getPhoneNumber(String phoneNumber) {
        if (phoneNumber != null) {
            phoneNumber = phoneNumber.replaceAll("^\\+1", "");
            if (phoneNumber.length() == 10) {
                phoneNumber = phoneNumber.substring(0, 3) + "-" + phoneNumber.substring(3, 6) + "-" + phoneNumber.substring(6);
            }
            return phoneNumber;
        }
        return "";
    }

    public static String getNumberOfYears(String dob) {
        SimpleDateFormat sdf = new SimpleDateFormat(DOB_TEMPLATE, Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = sdf.parse(dob);
        } catch (ParseException e) {
            Log.e(TAG, "getNumberOfYears: ", e);
        }
        return date != null ? " (" + getDiffYears(date, new Date()) + " years)" : "";
    }

    private static int getDiffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    public static void clearExternalCache(Context context) {
        File file = context.getExternalCacheDir();
        File[] files = new File[0];
        if (file != null) {
            files = file.listFiles();
        }
        if (files != null) {
            for (File f : files) {
                f.delete();
            }
        }
    }

    public static String getProviderName(Integer providerId, List<Provider> providers) {
        String providerName;
        if (providers != null) {
            for (Provider provider : providers) {
                if (provider.getId().equals(providerId)) {
                    providerName = provider.getUser().getFirstName() + " " + provider.getUser().getLastName();
                    return providerName;
                }
            }
        }
        return null;
    }

    public static int dpToPx(Context context, int dp) {
        if (context != null) {
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
        }
        return 0;
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    public static void logout(Activity activity) {
        HelpUtils.clearExternalCache(activity);
        SharedPreferencesHelper.deleteAccessToken();
        SharedPreferencesHelper.deleteRefreshToken();
        SharedPreferencesHelper.deletePin();
        SharedPreferencesHelper.deleteSpeakerState();
        SharedPreferencesHelper.deleteServer();
        SharedPreferencesHelper.deleteExpiredAt();
        Intent intent = new Intent(activity, StartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    @Nullable
    public static String getTextByRecordType(Call call, RecordType recordType) {
        String text = null;
        List<Record> records = call.getRecords();
        if (records != null && !records.isEmpty()) {
            for (Record record : records) {
                if (record.getType().equalsIgnoreCase(recordType.name()) && record.getTranscription() != null) {
                    text = record.getTranscription().getText();
                }
            }
        }
        return text;
    }

    public static void showOfflineDialog(Activity activity) {
        if (activity != null && !SharedPreferencesHelper.isOfflineDialogShow(activity)) {
            SharedPreferencesHelper.markOfflineDialogShown(activity);
            Dialog dialog = Dialog.newInstance(activity.getString(R.string.common_error),
                    activity.getString(R.string.remote_no_internet_message));
            FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
            ft.add(dialog, Constant.DIALOG_TAG);
            ft.commitAllowingStateLoss();
        }
    }
}
