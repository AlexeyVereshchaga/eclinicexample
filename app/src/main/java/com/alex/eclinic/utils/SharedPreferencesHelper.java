package com.alex.eclinic.utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.alex.eclinic.TheApplication;
import com.alex.eclinic.network.Server;

import java.util.HashSet;
import java.util.Set;

/**
 * 15.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class SharedPreferencesHelper {

    private static SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(TheApplication.getInstance().getApplicationContext());
    }

    @Nullable
    public static String getPin() {
        return getPreferences().getString(Constant.PIN_KEY, null);
    }

    @NonNull
    public static Boolean isSpeakerState() {
        return getPreferences().getBoolean(Constant.SPEAKER_KEY, false);
    }

    @NonNull
    public static Boolean isNeedShowPopUp() {
        return getPreferences().getBoolean(Constant.POP_UP_KEY, true);
    }

    @NonNull
    public static String getServer() {
        return getPreferences().getString(Constant.SERVER_KEY, Server.PROD.getUrl());
    }

    @NonNull
    public static String getAccessToken() {
        return getPreferences().getString(Constant.ACCESS_TOKEN_KEY, "");
    }

    @NonNull
    public static String getRefreshToken() {
        return getPreferences().getString(Constant.REFRESH_TOKEN_KEY, "");
    }

    @NonNull
    public static Long getExpiredAt() {
        return getPreferences().getLong(Constant.EXPIRE_AT_KEY, 0);
    }

    public static void savePin(String pin) {
        getPreferences().edit().putString(Constant.PIN_KEY, pin).apply();
    }

    public static void saveSpeakerState(Boolean state) {
        getPreferences().edit().putBoolean(Constant.SPEAKER_KEY, state).apply();
    }

    public static void saveNeedShowPopUp(Boolean need) {
        getPreferences().edit().putBoolean(Constant.POP_UP_KEY, need).apply();
    }

    public static void saveServer(Server server) {
        getPreferences().edit().putString(Constant.SERVER_KEY, server.getUrl()).apply();
    }

    public static void saveAccessToken(String accessToken) {
        getPreferences().edit().putString(Constant.ACCESS_TOKEN_KEY, accessToken).apply();
    }

    public static void saveRefreshToken(String accessToken) {
        getPreferences().edit().putString(Constant.REFRESH_TOKEN_KEY, accessToken).apply();
    }

    public static void saveExpiredAt(Long expiredAt) {
        getPreferences().edit().putLong(Constant.EXPIRE_AT_KEY, expiredAt).apply();
    }

    public static void deletePin() {
        getPreferences().edit().remove(Constant.PIN_KEY).commit();
    }

    public static void deleteSpeakerState() {
        getPreferences().edit().remove(Constant.SPEAKER_KEY).commit();
    }

    public static void deleteServer() {
        getPreferences().edit().remove(Constant.SERVER_KEY).commit();
    }

    public static void deleteAccessToken() {
        getPreferences().edit().remove(Constant.ACCESS_TOKEN_KEY).commit();
    }

    public static void deleteRefreshToken() {
        getPreferences().edit().remove(Constant.REFRESH_TOKEN_KEY).commit();
    }

    public static void deleteExpiredAt() {
        getPreferences().edit().remove(Constant.EXPIRE_AT_KEY).commit();
    }

    public static void markOfflineDialogShown(Activity activity) {
        Set<String> stringSet = PreferenceManager.getDefaultSharedPreferences(activity).getStringSet(Constant.OFFLINE_DIALOG_KEY, null);
        if (stringSet == null) {
            stringSet = new HashSet<>();
        }
        stringSet.add(activity.getClass().getSimpleName());
        PreferenceManager.getDefaultSharedPreferences(activity).edit()
                .putStringSet(Constant.OFFLINE_DIALOG_KEY, stringSet).apply();
    }

    public static void markOfflineDialogNotShown(Activity activity) {
        Set<String> stringSet = PreferenceManager.getDefaultSharedPreferences(activity).getStringSet(Constant.OFFLINE_DIALOG_KEY, null);
        if (stringSet != null) {
            stringSet.remove(activity.getClass().getSimpleName());
            PreferenceManager.getDefaultSharedPreferences(activity).edit()
                    .putStringSet(Constant.OFFLINE_DIALOG_KEY, stringSet).apply();
        }
    }

    public static boolean isOfflineDialogShow(Activity activity) {
        Set<String> stringSet = PreferenceManager.getDefaultSharedPreferences(activity).getStringSet(Constant.OFFLINE_DIALOG_KEY, null);
        if (stringSet != null) {
            return stringSet.contains(activity.getClass().getSimpleName());
        }
        return false;
    }
}
