package com.alex.eclinic.utils.audio;

import java.io.IOException;
import java.io.InputStream;


public class WaveHeader {
    private boolean valid;
    private String chunkId;
    private long chunkSize;
    private String format;
    private String subChunk1Id;
    private long subChunk1Size;
    private int audioFormat;
    private int channels;
    private long sampleRate;
    private long byteRate;
    private int blockAlign;
    private int bitsPerSample;
    private String subChunk2Id;
    private long subChunk2Size;

    public WaveHeader(InputStream inputStream) {
        this.valid = this.loadHeader(inputStream);
    }

    private boolean loadHeader(InputStream inputStream) {
        byte[] headerBuffer = new byte[44];

        try {
            inputStream.read(headerBuffer);
            byte e = 0;
            byte[] var10003 = new byte[4];
            int var5 = e + 1;
            var10003[0] = headerBuffer[e];
            var10003[1] = headerBuffer[var5++];
            var10003[2] = headerBuffer[var5++];
            var10003[3] = headerBuffer[var5++];
            this.chunkId = new String(var10003);
            this.chunkSize = (long) (headerBuffer[var5++] & 255) | (long) (headerBuffer[var5++] & 255) << 8 | (long) (headerBuffer[var5++] & 255) << 16 | (long) (headerBuffer[var5++] & -16777216);
            this.format = new String(new byte[]{headerBuffer[var5++], headerBuffer[var5++], headerBuffer[var5++], headerBuffer[var5++]});
            this.subChunk1Id = new String(new byte[]{headerBuffer[var5++], headerBuffer[var5++], headerBuffer[var5++], headerBuffer[var5++]});
            this.subChunk1Size = (long) (headerBuffer[var5++] & 255) | (long) (headerBuffer[var5++] & 255) << 8 | (long) (headerBuffer[var5++] & 255) << 16 | (long) (headerBuffer[var5++] & 255) << 24;
            this.audioFormat = headerBuffer[var5++] & 255 | (headerBuffer[var5++] & 255) << 8;
            this.channels = headerBuffer[var5++] & 255 | (headerBuffer[var5++] & 255) << 8;
            this.sampleRate = (long) (headerBuffer[var5++] & 255) | (long) (headerBuffer[var5++] & 255) << 8 | (long) (headerBuffer[var5++] & 255) << 16 | (long) (headerBuffer[var5++] & 255) << 24;
            this.byteRate = (long) (headerBuffer[var5++] & 255) | (long) (headerBuffer[var5++] & 255) << 8 | (long) (headerBuffer[var5++] & 255) << 16 | (long) (headerBuffer[var5++] & 255) << 24;
            this.blockAlign = headerBuffer[var5++] & 255 | (headerBuffer[var5++] & 255) << 8;
            this.bitsPerSample = headerBuffer[var5++] & 255 | (headerBuffer[var5++] & 255) << 8;
            this.subChunk2Id = new String(new byte[]{headerBuffer[var5++], headerBuffer[var5++], headerBuffer[var5++], headerBuffer[var5++]});
            this.subChunk2Size = (long) (headerBuffer[var5++] & 255) | (long) (headerBuffer[var5++] & 255) << 8 | (long) (headerBuffer[var5++] & 255) << 16 | (long) (headerBuffer[var5++] & 255) << 24;
        } catch (IOException var4) {
            var4.printStackTrace();
            return false;
        }

        if (this.bitsPerSample != 8 && this.bitsPerSample != 16) {
            System.err.println("WaveHeader: only supports bitsPerSample 8 or 16");
            return false;
        } else if (this.chunkId.toUpperCase().equals("RIFF") && this.format.toUpperCase().equals("WAVE") && this.audioFormat == 1) {
            return true;
        } else {
            System.err.println("WaveHeader: Unsupported header format");
            return false;
        }
    }

    public boolean isValid() {
        return this.valid;
    }

    public String getFormat() {
        return this.format;
    }

    public int getBitsPerSample() {
        return this.bitsPerSample;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}

