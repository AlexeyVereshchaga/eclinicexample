package com.alex.eclinic.utils;

/**
 * 11.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class Constant {

    public static final String ACCESS_TOKEN = "access_token";

    public static final String PIN_KEY = "pin_key";
    public static final String SPEAKER_KEY = "speaker";
    public static final String SERVER_KEY = "server";
    public static final String POP_UP_KEY = "pop_up";
    public static final String ACCESS_TOKEN_KEY = "access_token";
    public static final String REFRESH_TOKEN_KEY = "refresh_token";
    public static final String EXPIRE_AT_KEY = "expire_at";

    public static final String OFFLINE_DIALOG_KEY = "Offline";

    public static final String DIALOG_TAG = "0";
}
