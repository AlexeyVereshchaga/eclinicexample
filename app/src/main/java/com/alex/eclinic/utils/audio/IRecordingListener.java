package com.alex.eclinic.utils.audio;

/**
 * IStopRecordindListener.java 22.10.2015
 * Class description
 *
 * @author Alex.
 */
public interface IRecordingListener {
    void onStop();

    void receiveAmplitude(Double amplitude);
}
