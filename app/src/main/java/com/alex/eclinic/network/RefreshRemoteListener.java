package com.alex.eclinic.network;

import android.app.Activity;
import android.widget.Toast;

import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.response.Authentication;
import com.alex.eclinic.utils.HelpUtils;
import com.alex.eclinic.utils.SharedPreferencesHelper;

import java.util.Date;

import retrofit2.Call;

/**
 * 18.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class RefreshRemoteListener extends BaseRemoteListener<Authentication> {

    public RefreshRemoteListener(Activity activity) {
        super(activity);
    }

    @Override
    public void onStartTask() {
        DataManager.isRefreshed = true;
    }

    @Override
    public void onSuccess(Authentication result) {
        if (result != null) {
            SharedPreferencesHelper.saveExpiredAt(new Date().getTime() + result.getExpiresIn() * 1000);
            SharedPreferencesHelper.saveAccessToken(result.getAccessToken());
            SharedPreferencesHelper.saveRefreshToken(result.getRefreshToken());
            DataManager.isRefreshed = false;
            DataManager.repeatRequests();
        }
    }

    @Override
    public void onFailure(Integer failureCode, Call<Authentication> call, BaseHandler<Authentication> handler) {
        if (activity != null) { //failureCode.equals(HttpURLConnection.HTTP_BAD_REQUEST) &&
            HelpUtils.logout(activity);
        }
    }

    @Override
    public void onFailure(Integer errorCode, Authentication error) {
        if (activity != null && error != null) {
            Toast.makeText(activity, error.getErrorDescription(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Throwable throwable) {
        super.onFailure(throwable);
        DataManager.getCallsUnauthorized().clear();
        DataManager.isRefreshed = false;
    }
}
