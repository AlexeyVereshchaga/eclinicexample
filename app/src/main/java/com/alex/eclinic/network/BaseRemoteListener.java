package com.alex.eclinic.network;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.alex.eclinic.controller.DataManager;
import com.alex.eclinic.modelv3.ErrorModel;
import com.alex.eclinic.utils.Constant;
import com.alex.eclinic.utils.HelpUtils;

import retrofit2.Call;

import static com.alex.eclinic.network.ErrorCode.UNAUTHORIZED;

/**
 * 08.10.15.
 *
 * @author Alexey Vereshchaga
 */
public abstract class BaseRemoteListener<T> implements IRemoteListener<T> {

    private final static String TAG = BaseRemoteListener.class.getSimpleName();
    protected static final String TAG_NETWORK_NOT_AVAILABLE = "network";
    private static final String UNABLE_TO_RESOLVE_HOST = "Unable to resolve host";
    protected Activity activity;

    protected BaseRemoteListener(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onStartTask() {
    }

    @Override
    public void onSuccess(T result) {
    }

    @Override
    public void onFailure(Integer failureCode, Call<T> call, BaseHandler<T> handler) {
        if (UNAUTHORIZED.getCode().equals(failureCode)
                && call != null
                && handler != null) {
            WrapperUnauthorized wrapperUnauthorized = new WrapperUnauthorized(call, handler);
            DataManager.addCallUnauthorized(wrapperUnauthorized);
            DataManager.refreshToken(activity);
        }
    }

    @Override
    public void onFailure(Integer errorCode, T error) {
        Log.e(TAG, "Response error body: " + error);
        if (!UNAUTHORIZED.getCode().equals(errorCode)) {
            if (activity != null && error != null) {
                String errorMessage = null;
                try {
                    errorMessage = ((ErrorModel) error).getErrorDescription();
                } catch (Exception e) {
                    Log.e(TAG, "Error output", e);
                }
                if (errorMessage == null || errorMessage.isEmpty()) {
                    errorMessage = "Error";
                }
                Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onFinishTask() {
    }

    @Override
    public void onFailure(Throwable throwable) {
        if (throwable != null) {
            Log.e(TAG, "", throwable);
            if (throwable.getMessage() != null && throwable.getMessage().contains(UNABLE_TO_RESOLVE_HOST)) {
                if (activity != null && activity.getFragmentManager().findFragmentByTag(Constant.DIALOG_TAG) == null) {
                    HelpUtils.showOfflineDialog(activity);
                }
            }
        }
    }
}
