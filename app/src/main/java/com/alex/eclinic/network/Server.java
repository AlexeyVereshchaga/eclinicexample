package com.alex.eclinic.network;

/**
 * Class description
 * 03.12.15.
 *
 * @author Alexey Vereshchaga
 */
public enum Server {
    PROD("http://api.eclinichealthcare.com"),
    DEMO("http://demo-api.eclinichealthcare.com"),
    REVIEW("http://review-api.eclinichealthcare.com");

    private String url;

    Server(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
