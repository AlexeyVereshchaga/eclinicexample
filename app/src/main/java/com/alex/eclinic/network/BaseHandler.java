package com.alex.eclinic.network;

import android.util.Log;

import com.alex.eclinic.modelv3.ErrorModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Class description
 * 08.10.15.
 *
 * @author Alexey Vereshchaga
 */
public class BaseHandler<T> implements Callback<T> {

    private static final String TAG = BaseHandler.class.getSimpleName();

    protected IRemoteListener remoteListener;
    private final TypeReference<T> typeReference;

    public BaseHandler(IRemoteListener remoteListener, TypeReference<T> typeReference) {
        this.remoteListener = remoteListener;
        this.typeReference = typeReference;
    }

    public void onStart() {
        remoteListener.onStartTask();
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response != null) {
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    remoteListener.onSuccess(response.body());
                }
            } else {
                Integer errorCode = response.code();
                if (response.errorBody() != null) {
                    T object = null;
                    try {
                        String errorString = response.errorBody().string();
                        object = new ObjectMapper().readValue(errorString, typeReference);
                    } catch (Exception e) {
                        Log.e(TAG, "Error", e);
                    }
                    remoteListener.onFailure(errorCode, object);
                }

                remoteListener.onFailure(errorCode, call, this);
                remoteListener.onFailure((Throwable) null);
            }
        }

        remoteListener.onFinishTask();
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        remoteListener.onFinishTask();
        remoteListener.onFailure(t);
    }
}
