package com.alex.eclinic.network;

import retrofit2.Call;

/**
 * Class description
 * 08.10.15.
 *
 * @author Alexey Vereshchaga
 */
public interface IRemoteListener<T> {

    void onStartTask();

    void onSuccess(T result);

    void onFailure(Integer errorCode, T errorBody);

    void onFailure(Integer failureCode, Call<T> call, BaseHandler<T> handler);

    void onFailure(Throwable throwable);

    /**
     * End remote call process, call in any case after onSuccess,onFailure
     */
    void onFinishTask();
}
