package com.alex.eclinic.network;

import java.util.HashMap;
import java.util.Map;

/**
 * Class description
 * 08.10.15.
 *
 * @author Alexey Vereshchaga
 */
public enum ErrorCode {
    NETWORK_NOT_AVAILABLE(-1, "Network is not available"),
    UNAUTHORIZED(401, "Unauthorized"),
    CONFLICT(409, "");

    private Integer code;
    private String message;
    private static Map<String, Integer> mapCodeByMessage;


    ErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static Integer getCodeByMessage(String message) {
        if (mapCodeByMessage == null) {
            initializeMappingNameById();
        }
        if (mapCodeByMessage.containsKey(message)) {
            return mapCodeByMessage.get(message);
        }
        return null;
    }

    private static void initializeMappingNameById() {
        mapCodeByMessage = new HashMap<>();
        for (ErrorCode errorCode : ErrorCode.values()) {
            mapCodeByMessage.put(errorCode.message, errorCode.code);
        }
    }
}