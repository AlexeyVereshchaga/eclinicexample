package com.alex.eclinic.network;

import com.alex.eclinic.modelv3.request.Credentials;
import com.alex.eclinic.modelv3.request.ProviderAvailability;
import com.alex.eclinic.modelv3.request.ProviderRemote;
import com.alex.eclinic.modelv3.request.Remote;
import com.alex.eclinic.modelv3.request.Reset;
import com.alex.eclinic.modelv3.request.Token;
import com.alex.eclinic.modelv3.response.Access;
import com.alex.eclinic.modelv3.response.CallWrapper;
import com.alex.eclinic.modelv3.response.CommonInfo;
import com.alex.eclinic.modelv3.response.CountInfo;
import com.alex.eclinic.modelv3.response.ProfileProvider;
import com.alex.eclinic.modelv3.response.Provider;
import com.alex.eclinic.modelv3.response.Providers;
import com.alex.eclinic.modelv3.response.Response;
import com.alex.eclinic.modelv3.response.Status;
import com.alex.eclinic.modelv3.response.UserProfile;
import com.alex.eclinic.utils.audio.AudioRecorder;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;


/**
 * 07.10.15.
 *
 * @author Alexey Vereshchaga
 */
public interface ClinicApi {

    String VERSION_PREFIX = "/api/v3";

    //v3
    @POST(VERSION_PREFIX + "/users/gcm-token")
    Call<Status> sendToken(@Body Token token);

    @POST(VERSION_PREFIX + "/oauth/access_token")
    Call<Access> getToken(@Body Credentials credentials);

    @POST(VERSION_PREFIX + "/password/reset")
    Call<Response> resetPassword(@Body Reset reset);

    @GET(VERSION_PREFIX + "/users/profile")
    Call<UserProfile> getUserProfile();

    @Headers("Content-Type: application/json")
    @PUT(VERSION_PREFIX + "/providers/{providerId}")
    Call<Provider> updateProfileProvider(@Path("providerId") Integer providerId, @Body ProviderRemote profileProvider);

    @PUT(VERSION_PREFIX + "/providers/{providerId}/provider-status ")
    Call<ProfileProvider> updateProfileAvailability(@Path("providerId") Integer providerId, @Body ProviderAvailability providerAvailability);

    @GET(VERSION_PREFIX + "/providers/{providerId}/calls/find/{calls_type}")
    Call<CommonInfo> getCalls(@Path("providerId") Integer providerId, @Path("calls_type") String callsType);

    @Headers("Content-Type: application/json")
    @POST(VERSION_PREFIX + "/calls/{sid}/response")
    Call<Status> sendAction(@Path("sid") String sid, @Body Remote remote);

    @Multipart
    @POST(VERSION_PREFIX + "/calls/{sid}/response")
    Call<Status> upload(@Path("sid") String sid,
                        @Part("action") RequestBody action,
                        @Part("data\"; filename=\"" + AudioRecorder.AUDIO_RECORDER_FILE_EXT_WAV + "\" ") RequestBody file);

    @Headers("Content-Type: application/json")
    @POST(VERSION_PREFIX + "/calls/{sid}/complete")
    Call<com.alex.eclinic.modelv3.response.Call> sendCompletedCall(@Path("sid") String sid);

    @GET(VERSION_PREFIX + "/providers")
    Call<Providers> getProviders();

    @GET(VERSION_PREFIX + "/providers/calls/find/new/count")
    Call<CountInfo> getIncompleteCalls();

    @GET(VERSION_PREFIX + "/success")
    Call<Response> getSuccess();

    @GET(VERSION_PREFIX + "/calls/{callId}")
    Call<CallWrapper> getCall(@Path("callId") String callId);
}
